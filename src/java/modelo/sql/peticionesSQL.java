/*
 * Esta clase contiene todas las instrucciones sql que se van a ejecutar
 * @autho: Enrique Cervantes
 */
package modelo.sql;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modelo.Comentario;
import modelo.EntradasForo;
import modelo.Incidencia;
import modelo.Miembro;

public class peticionesSQL {

		/**
		* Este metodo sirve para crear un nuevo miembro en la base de datos
		* @param nombre Nombre del miembro
		* @param correo Direccion de correo del usuario
		* @param password Contraseña de la nueva cuenta
		* @param linea Linea de metro que sea de su preferencia
		* @param admin Tipo de cuenta que se crea true-admin | false-normal
		*/
		public void crearMiembro(String nombre, String correo, String password, String linea, boolean admin) {
				try {
						System.out.println("Entramos al miembro");
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("INSERT INTO miembros(nombre_m, correo_m, contrasena_m, linea_m, administrador) VALUES (?,?,?,?,?)");
						System.out.println("statemet creado");
						ps.setString(1, nombre);
						ps.setString(2, correo);
						ps.setString(3, password);
						ps.setString(4, linea);
						ps.setBoolean(5, admin);
						ps.executeUpdate();
						System.out.println("execucion hecha");
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("uis miembro");
				}
		}
		
		/**
		 *	Con este metodo se pueden crear registros de nuevas incidencias y almacenarlos en la base de datos
		 * @param reporte Objeto Incidencia que se va a insertar en la base de datos
		 */
		public void crearIncidencia(Incidencia reporte) {
				System.out.println("El coso es: " + reporte.toString());
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("INSERT INTO incidencias(hora_i, fecha_i, tipo_i, comentario, id_es, nombre_m) VALUES (?,?,?,?,?,?)");
						ps.setString(1, reporte.getHora());
						ps.setString(2, reporte.getFecha());
						ps.setString(3, reporte.getTipo());
						ps.setString(4, reporte.getComent());
						ps.setInt(5, reporte.getId_es());
						ps.setString(6, reporte.getNombre_m());
						ps.executeUpdate();
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("uis al crear" + e);
				}
		}
		
		/**
		 * Con este metodo se pueden crear nuevas entradas de la seccion de Foro
		 * @param nombre_e Es la id de la entrada, aunque es un nombre, la verdad, no se que sea
		 * @param descripcion Es el contenido de la entrada
		 * @param nombre Es el nombre del usuario que creo la entrada
		 */
		public void crearEntrada(String nombre_e, String descripcion, String nombre) {
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("INSERT INTO entradaForo(nombre_e, descripcion, nombre_m) VALUES (?,?,?)");
						ps.setString(1, nombre_e);
						ps.setString(2, descripcion);
						ps.setString(3, nombre);
						ps.executeUpdate();
						
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("uis entrada crear" + e);
				}
		}

		/**
		 * Con este metodo se crean comentarios para las entradas del foro
		 * @param desc Es el contenido del comentario
		 * @param nombre_m Es el nombre del usuario que creo el comentario
		 * @param id_e Es la id de la entrada del foro en la que se agrega dicho comentario
		 */
		public void crearComentario(String desc, String nombre_m, int id_e) {
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("INSERT INTO comentarios (descripcion_c, nombre_m, id_e) VALUES(?,?,?)");
						ps.setString(1, desc);
						ps.setString(2, nombre_m);
						ps.setInt(3, id_e);
						ps.executeUpdate();
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("Error al crear comentario " + e);
				}
		}
		
		/**
		 * Con este metodo se pueden editar los datos de un miembro
		 * @param miembro Objeto del tipo miembro que contiene los datos del miembro
		 */
		public void editarMiembro(Miembro miembro){
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("UPDATE miembros SET correo_m=?, contrasena_m=?, linea_m=? WHERE nombre_m=?");
						ps.setString(1, miembro.getCorreo());
						ps.setString(2, miembro.getPass());
						ps.setString(3, miembro.getLinea());
						ps.setString(4, miembro.getNombre());
						
						ps.executeUpdate();
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("uis al editar miembro" + e);
				}
		}
		
		/**
		 * Edita una entrada que ya se tenga registrada
		 *@param reporte Objeto Incidencia que contiene los datos de la incidencia
		 */
		public void editarIncidencia(Incidencia reporte) {
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("UPDATE incidencias SET hora_i=?, fecha_i=?, tipo_i=?, comentario=?, id_es=? WHERE id_i =?");
						ps.setString(1, reporte.getHora());
						ps.setString(2, reporte.getFecha());
						ps.setString(3, reporte.getTipo());
						ps.setString(4, reporte.getComent());
						ps.setInt(5, reporte.getId_es());
						ps.setInt(6, reporte.getId_i());
						ps.executeUpdate();
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("uis editar incidencai");
				}
		}

		/**
		 * Edita una entrada del foro de un usuario.
		 * @param entrada Objeto tipo EntradasForo que contiene los datos de la entrada a editar
		 */
		public void editarEntrada( EntradasForo entrada ) {
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("UPDATE entradaForo SET nombre_e=?, descripcion=? WHERE id_e=?");
						ps.setString(1, entrada.getNombre_e());
						ps.setString(2, entrada.getDesc());
						ps.setInt(3, entrada.getId_e());
						ps.executeUpdate();
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("uis editar entrada "  + e);
				}
		}
		
		public void editarComentario(String desc, int id_c){
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("UPDATE comentarios SET descripcion_c=? WHERE id_c=?");
						
						ps.setString(1, desc);
						ps.setInt(2, id_c);
						
						ps.executeUpdate();
						conexionSQL.closeConnection();
						
				} catch (Exception e) {
				}
		}

		/**
		 * Obtiene todas las entradas
		 * @return Devuelve una lista con obtejos EntradasForo que contienen todas las entradas al foro
		 */
		public List<EntradasForo> getEntradas() {
				List<EntradasForo> entradas = new ArrayList<EntradasForo>();
				try {
						Statement st = conexionSQL.getConnection().createStatement();
						ResultSet rs = st.executeQuery("SELECT * FROM entradaForo ORDER BY id_e DESC");
						while (rs.next()) {
								int id_e = rs.getInt(1);
								String nombre_e = rs.getString(2);
								String desc = rs.getString(3);
								String nombre_m = rs.getString(4);
								entradas.add(new EntradasForo(id_e, nombre_e, desc, nombre_m));
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("uis get entradas" + e);
				}
				return entradas;
		}
		
		/**
		 * Consigue todas las entradas que estan registradas por un miembro
		 * @param nombre_m El nombre del usuario del que se buscan las entradas
		 * @return Devuelve una lista con todas las entradas del usuario
		 */
		public List<EntradasForo> getEntradas(String nombre_m) {
				List<EntradasForo> entradas = new ArrayList<EntradasForo>();
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("SELECT * FROM entradaForo WHERE nombre_m=?");
						ps.setString(1, nombre_m);
						ResultSet rs = ps.executeQuery();
						
						while (rs.next()) {
								int id_e = rs.getInt(1);
								String nombre_e = rs.getString(2);
								String desc = rs.getString(3);
								entradas.add(new EntradasForo(id_e, nombre_e, desc, nombre_m));
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
				}
				return entradas;
		}
		
		/**
		 * Obtiene una entrada dependiendo su id.
		 * @param id Id de la entrada que se busca
		 * @return Objeto tipo EntradaForo que contiene los datos de la entrada
		 */
		public EntradasForo getEntrada(int id){
				EntradasForo entrada = null;
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("SELECT nombre_e, descripcion, nombre_m FROM entradaForo WHERE id_e=?");
						ps.setInt(1, id);
						ResultSet rs = ps.executeQuery();
						//entrada = new EntradasForo(id, rs.getString(1), rs.getString(2), rs.getString(3));
						while(rs.next()){
								String nombre_e = rs.getString(1);
								String desc = rs.getString(2);
								String miembro = rs.getString(3);
								
								entrada = new EntradasForo(id, nombre_e, desc, miembro);
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("uis get entrada " + e);
				}
				return entrada;
		}

		/**
		 * Obtiene todos los comentarios de una entrada
		 * @param id_e La id de la entrada de los comentarios
		 * @return Devuelve una lista con todos los comentarios
		 */
		public List<Comentario> getComentarios(int id_e) {
				List<Comentario> comentarios = new ArrayList<Comentario>();
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("SELECT * FROM comentarios WHERE id_e=?");
						ps.setInt(1, id_e);
						ResultSet rs = ps.executeQuery();
						while (rs.next()) {
								int id_c = rs.getInt(1);
								String desc = rs.getString(2);
								String nombre_m = rs.getString(3);
								int id_es = rs.getInt(4);
								
								comentarios.add(new Comentario(id_c, desc, nombre_m, id_es));
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("uis al obtener esto" + e);
				}
				
				return comentarios;
		}
		
		/**
		 * Obtine todos los comentarios de un usuario
		 * @param user Nombre de usuario
		 * @return Lista con todos los comentarios del usuario
		 */
		public List<Comentario> getComentarios(String user){
				List<Comentario> comentarios = new ArrayList<>();
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("SELECT id_c, descripcion_c, id_e FROM comentarios WHERE nombre_m=?");
						ps.setString(1, user);
						ResultSet rs = ps.executeQuery();
						while (rs.next()) {
								int id_c = rs.getInt(1);
								String desc = rs.getString(2);
								int id_es = rs.getInt(3);
								
								comentarios.add(new Comentario(id_c, desc, user, id_es));
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
				}
				
				return comentarios;
		}
 
		/**
		 * Obtiene todas las incidencias registradas.
		 * @return Devuelve todas las incidencias almacenadas
		 */
		public List<Incidencia> getIncidencias() {
				List<Incidencia> incidencias = new ArrayList<Incidencia>();
				try {
						Statement st = conexionSQL.getConnection().createStatement();
						ResultSet rs = st.executeQuery("select id_i, hora_i, fecha_i, tipo_i, comentario, linea_es, nombre_es, nombre_m from incidencias  inner join estaciones where estaciones.id_es = incidencias.id_es");
						while (rs.next()) {
								int id_i = rs.getInt(1);
								String hora = rs.getString(2);
								String fecha = rs.getString(3);
								String tipo = rs.getString(4);
								String coment = rs.getString(5);
								String linea = rs.getString(6);
								String nombre_es = rs.getString(7);
								String nombre_m = rs.getString(8);
								incidencias.add(new Incidencia(id_i, hora, fecha, tipo, coment, nombre_m, nombre_es, linea));
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
				}
				return incidencias;
		}
		
		/**
		 * Obtiene una Incidencia segun su id
		 * @param id Id de la incidencia que se busca
		 * @return Devuelve una incidencia
		 */
		public Incidencia getIncidencias(int id) {
				Incidencia incidencia = null;
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("select nombre_es, linea_es, hora_i, fecha_i, tipo_i, comentario, nombre_m from incidencias as i inner join estaciones as  e on e.id_es=i.id_es where i.id_i = ?");
						ps.setInt(1, id);
						ResultSet rs = ps.executeQuery();
						while (rs.next()) {
								String estacion = rs.getString(1);
								String linea = rs.getString(2);
								String hora = rs.getString(3);
								String fecha = rs.getString(4);
								String tipo = rs.getString(5);
								String coment = rs.getString(6);
								String miembro = rs.getString(7);
								
								incidencia = new Incidencia(id, hora, fecha, tipo, coment, miembro, estacion, linea);
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
				}
				return incidencia;
		}
		
		/**
		* Este metodo consigue todas los reportes que fueron creados por un miembro
		* @param idMiembro Es la id del miembro de quien se buscan las incidencias
		* @return Devuelve una lista con otdas las incidencias
		*/
		public List<Incidencia> getIncidencias(String idMiembro) {
				List<Incidencia> incidencias = new ArrayList<Incidencia>();
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("select nombre_es, linea_es, id_i, hora_i, fecha_i, tipo_i, comentario from incidencias as i inner join estaciones as  e on e.id_es=i.id_es where i.nombre_m = ?");
						ps.setString(1, idMiembro);
						ResultSet rs = ps.executeQuery();
						while (rs.next()) {
								String estacion = rs.getString(1);
								String linea = rs.getString(2);
								int id_i = rs.getInt(3);
								String hora = rs.getString(4);
								String fecha = rs.getString(5);
								String tipo = rs.getString(6);
								String coment = rs.getString(7);
								
								incidencias.add(new Incidencia(id_i, hora, fecha, tipo, coment, idMiembro, estacion, linea));
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("uis" + e);
				}
				return incidencias;
		}
		
		/**
		 * Obtiene todas las incidenicas de una linea del metro
		 * @param linea linea del metro que se busca
		 * @return Devuelve una ista con todas las incidencias de esa linea
		 */
		public List<Incidencia> getIncidenciasLinea(String linea){
				List<Incidencia> incidencias = new ArrayList<>();
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("SELECT id_i, hora_i, fecha_i, tipo_i, comentario, nombre_es FROM incidencias INNER JOIN estaciones WHERE incidencias.id_es = estaciones.id_es and estaciones.linea_es = ?");
						ps.setString(1, linea);
						ResultSet rs = ps.executeQuery();
						
						while(rs.next()){
								int id_i = rs.getInt(1);
								String hora = rs.getString(2);
								String fecha = rs.getString(3);
								String tipo = rs.getString(4);
								String coment = rs.getString(5);
								String nombre_es = rs.getString(6);
								incidencias.add(new Incidencia(id_i, hora, fecha, tipo, coment, nombre_es, linea));
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("Error al obtener las incidencias de una linea");
				}
				return incidencias;
		}
		
		/**
		 * Obtiene todas las incidencias a partir de una peticion que tiene los filtros incluidos
		 * @param query Peticion que se va a ejecutar en la base de datos
		 * @return Lista con todas las incidencias que cumplan tales filtros
		 */
		public List<Incidencia> getIncidenciasF(String query){
				List<Incidencia> incidencias = new ArrayList<Incidencia>();
				System.out.println("La peticion a hacer es: " + query);
				try {
						Statement st = conexionSQL.getConnection().createStatement();
						
						ResultSet rs = st.executeQuery(query);
						
						while (rs.next()) {
								String nombre_es = rs.getString(1);
								String linea = rs.getString(2);
								String hora = rs.getString(3);
								String fecha = rs.getString(4);
								String tipo = rs.getString(5);
								String coment = rs.getString(6);
								int id_i = rs.getInt(7);
								
								incidencias.add(new Incidencia(id_i, hora, fecha, tipo, coment, nombre_es, linea));
						}
						conexionSQL.closeConnection();
						
				} catch (Exception e) {
						System.out.println("uis getincidenciasF" + e);
				}
				return incidencias;
		}
		
		/**
		 * Busca en la base de datos un miembro en especifico
		 * @param id Es el nombre de usuario del miembro que se busca
		 * @return Devuelve un objeto de tipo Miembro el cual contiene los datos del miembro
		 */
		public Miembro getMiembro(String id) {
				Miembro miembro = null;
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("SELECT nombre_m, correo_m, contrasena_m, linea_m,administrador  FROM miembros WHERE nombre_m=?");
						ps.setString(1, id);
						ResultSet rs = ps.executeQuery();
						while (rs.next()) {
								String nombre = rs.getString(1);
								String correo = rs.getString(2);
								String pass = rs.getString(3);
								String linea = rs.getString(4);
								Boolean admin = rs.getBoolean(5);
								miembro = new Miembro(nombre, correo, pass, linea, admin);
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
				}
				return miembro;
		}
		
		/**
		 * Obtiene todos los miembros que estan registrados y no son admin
		 * @return Devuelve una lista con todos los miembros
		 */
		public List<Miembro> getMiembros(){
				List<Miembro> miembros = new ArrayList<>();
				try {
						Statement st = conexionSQL.getConnection().createStatement();
						ResultSet rs = st.executeQuery("SELECT nombre_m, correo_m, linea_m FROM miembros WHERE administrador = false");
						while(rs.next()){
								String nombre_m = rs.getString(1);
								String correo = rs.getString(2);
								String linea = rs.getString(3);
								
								miembros.add(new Miembro(nombre_m, correo, linea));
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("Error al conseguir los miembros");
				}
				return miembros;
		}
		
		/**
		 * Elimina una entrada del foro
		 * @param id_e La id de la entrada que se quiere eliminar
		 */
		public void borrarEntrada(int id_e) {
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("DELETE FROM entradaForo WHERE id_e=?");
						ps.setInt(1, id_e);
						ps.executeUpdate();
						conexionSQL.closeConnection();
				} catch (Exception e) {
				}
		}
		
		/**
		 * Elimina una incidencia
		 * @param id_i Clave de identificacion de la incidencia
		 */
		public void borrarIncidencia(int id_i) {
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("DELETE FROM incidencias WHERE id_i=?");
						ps.setInt(1, id_i);
						ps.executeUpdate();
						conexionSQL.closeConnection();
				} catch (Exception e) {
				}
		}
		
		/**
		 * Este metodo elimina un miembro
		 * @param nombre_m El nombre del usuario que se quiere eliminar
		 */
		public void borrarMiembro(String nombre_m) {
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("DELETE FROM miembros WHERE nombre_m=?");
						ps.setString(1, nombre_m);
						ps.executeUpdate();
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("uis borrar miembro" + e);
				}
		}

		/**
		 * Elimina un comentario segun su id
		 * @param id_c Id del comentario a eliminar
		 */
		public void borrarComentario(int id_c) {
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("DELETE FROM comentarios WHERE id_c=?");
						ps.setInt(1, id_c);
						ps.executeUpdate();
						conexionSQL.closeConnection();
				} catch (Exception e) {
				}
		}
		
		/**
		 * Metodos para obtener la id de una estacion
		 * @param est Nombre de la estacion que se busca
		 * @param linea Linea del metro de la estacion
		 * @return Devuelve la id de la estacion
		 */
		public int getEstacion(String est, String linea){
				int id = 0;
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("SELECT id_es FROM estaciones WHERE nombre_es=? and linea_es=?");
						ps.setString(1, est);
						ps.setString(2, linea);
						
						ResultSet rs = ps.executeQuery();
						
						while(rs.next())
								id = rs.getInt("id_es");
						conexionSQL.closeConnection();
						
				} catch (Exception e) {
						System.out.println("uis en la est");
				}
				return id;
		}
		
		/**
		 * Obtiene la id de la estacion de un reporte
		 * @param idReporte Id del reporte del que se quiere la estacion
		 * @return Devuelve la id de la estacion en que ocurrio el reporte
		 */
		public int getEstacion(int idReporte){
				int idEst = 0;
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("SELECT id_es FROM incidencias WHERE id_i=?");
						ps.setInt(1, idReporte);
						ResultSet rs = ps.executeQuery();
						while(rs.next()){
								idEst = rs.getInt(1);
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("uis get estacion" + e);
				}
				return idEst;
		}
		
		/**
		 * Devuelve tanto la estacion de metro, como la linea a la que pertenece a partir de su id
		 * @param id Id de la estacion que se busca
		 * @return Devuelve un array String que contiene ambos datos[estacion, linea]
		 */
		public String[] getMetro(int id){
				String estacion = "", linea = "";
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("SELECT nombre_es, linea_es FROM estaciones WHERE id_es = ?");
						ps.setInt(1, id);
						
						ResultSet rs = ps.executeQuery();
						estacion = rs.getString(1);
						linea = rs.getString(2);
						
						conexionSQL.closeConnection();
						
				} catch (Exception e) {
				}
				
				String[] resultado = new String[2];
				resultado[0] = estacion;
				resultado[1] = linea;
				
				return resultado;
				
		}
		
		/**
		 *	Obtiene la id de un comentario
		 * @param idEntrada Id de la entrada del comentario
		 * @param comentario Comentario que se busca
		 * @return Devuelve la id del comentario
		 */
		public int getIDComentario(int idEntrada, String comentario){
				int id = 0;
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("SELECT  id_c FROM comentarios WHERE id_e=? and nombre_m?");
						ps.setInt(1, idEntrada);
						ps.setString(1, comentario);
						
						ResultSet rs = ps.executeQuery();
						
						while(rs.next()){
								id = rs.getInt(1);
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("uis id Comentario " + e);
				}
				return id;
		}
		
		/**
		 * Verifica que un miembro exista en la base de datos
		 * @param nombre El nombre del usuario del que se quiere comprobar la existencia
		 * 
		 * @return Devuelve true en caso de que el usuario exista
		 */
		public boolean existeMiembro(String nombre){
				boolean existe = true;
				try{
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("SELECT * FROM miembros WHERE nombre_m =?");
						ps.setString(1, nombre);
								;
						ResultSet rs = ps.executeQuery();
						
						//	Este if es true en caso de que el resultset este vacio
						if(!rs.isBeforeFirst() && rs.getRow() == 0){
								existe = false;
						}
						conexionSQL.closeConnection();
				}catch(Exception e){
						System.out.println("uis");
				}
				return existe;
		}
		
		/**
		 *  Con este metodo se comprueba que exista la id de un reporte
		 * @param idReporte Id del reporte que se quiere saber si existe
		 * @param idMiembro Id del miembro que creo el reporte
		 * @return Devuelve true en caso de que un reporte exista
		 */
		public boolean existeReporte(int idReporte, String idMiembro){
				boolean existe = true;
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("SELECT * FROM incidencias WHERE id_i=? and nombre_m=?");
						ps.setInt(1, idReporte);
						ps.setString(2, idMiembro);
						
						ResultSet rs = ps.executeQuery();
						
						// Devuelve true en caso de que el resultset este vacio
						if(!rs.isBeforeFirst() && rs.getRow() == 0){
								existe = false;
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("uis aqui");
				}
				return existe;
		}
		
		/**
		 * Con este metodo se comprueba que un usuario sea el creador de una entrada al foro.
		 * @param id Id de la entrada
		 * @param miembro Nombre del miembro
		 * @return Devuelve true en caso de que el miembro sea quien creo dicha entrada
		 */
		public boolean existeEntrada(int id, String miembro){
				boolean existe = true;
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("SELECT * FROM entradaForo WHERE id_e=? and nombre_m =?");
						ps.setInt(1, id);
						ps.setString(2, miembro);
						
						ResultSet rs = ps.executeQuery();
						
						
						if(!rs.isBeforeFirst() && rs.getRow() == 0){
								existe = false;
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("Existe entrada" + e);
				}
				return existe;
		}
		
		/**
		 * Verifica que un miembro sea el creador de un comentario
		 * @param id_c Id del comentario
		 * @param user Nombre de usuario
		 * @return Devuelve true en caso de que exista el comentario
		 */
		public boolean existeComentario(int id_c, String user){
				boolean existe = true;
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("SELECT * FROM comentarios WHERE id_c = ? and nombre_m = ?");
						ps.setInt(1, id_c);
						ps.setString(2, user);
						
						ResultSet rs = ps.executeQuery();
						if(!rs.isBeforeFirst() && rs.getRow() == 0){
								existe = false;
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("Error existeComentario: " + e);
				}
				return existe;
		}
		
		/**
		 * Con este metodo se comprueba que una entrada del foro exista.
		 * @param id Id de la entrada
		 * @return Devuelve true en caso de que el miembro sea quien creo dicha entrada
		 */
		public boolean existeEntrada(int id){
				boolean existe = true;
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("SELECT * FROM entradaForo WHERE id_e=?");
						ps.setInt(1, id);
						
						ResultSet rs = ps.executeQuery();
						
						
						if(!rs.isBeforeFirst() && rs.getRow() == 0){
								existe = false;
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("Existe entrada" + e);
				}
				return existe;
		}
		
		/**
		 * Aumenta en 1 el numero de incidencias qu ese tienen registradas en una estacion.
		 * @param idEst id de la estacion que aumenta 1 incidencia
		 */
		public void incrementarEst(int idEst){
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("UPDATE estaciones SET incidencias_es = incidencias_es + 1 WHERE id_es = ? ");
						ps.setInt(1, idEst);
						ps.executeUpdate();
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println( "uis incrementar Est" + e );
				}
		}
		
		/**
		 * Disminuue en 1 el numero de incidencias que se tienen registradas en una estacion
		 * @param idEst id de la estacion que disminuyo 1 incidencia
		 */
		public void disminuirEst(int idEst){
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("UPDATE estaciones SET incidencias_es = incidencias_es - 1 WHERE id_es = ?");
						ps.setInt(1, idEst);
						ps.executeUpdate();
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("uis al disminuir" + e);
				}
		}
		
		/**
		 *Devuelve el numero de todos los reportes por estacion
		 * @return Devuelve un objeto Json convertido a string con todos los datos
		 */
		public String obtenerNumEst(){
				JsonArray estacionesArray = new JsonArray();
				
				try {
						Statement st = conexionSQL.getConnection().createStatement();
						
						ResultSet rs = st.executeQuery("SELECT  nombre_es,  incidencias_es, linea_es FROM estaciones");
						while(rs.next()){
								
								JsonObject jsonEst = new JsonObject();
								jsonEst.addProperty("estacion", rs.getString(1));
								jsonEst.addProperty("incidencias", rs.getInt(2));
								jsonEst.addProperty("linea", rs.getString(3));
								
								estacionesArray.add(jsonEst);
								
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("Error al obtener este coso " + e );
				}
				String json = new Gson().toJson(estacionesArray);
				
				return json;
		}
		
		/**
		 * Obtiene todas las id de los reportes de un usuario
		 * @param user Nombre del usuario del que se buscaran los comentarios.
		 * @return Devuelve la lista de las id's 
		 */
		public List<Integer> getIdEstUser(String user){
				List<Integer> ides = new ArrayList<>();
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("SELECT id_es FROM incidencias WHERE nombre_m = ?");
						ps.setString(1, user);
						
						ResultSet rs = ps.executeQuery();
						
						while(rs.next()){
								ides.add( rs.getInt(1) );
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("Error getIdEstUser: " + e);
				}
				return ides;
		}
		
		public boolean userIsAdmin(String user){
				boolean admin = false;
				try {
						PreparedStatement ps = conexionSQL.getConnection().prepareStatement("SELECT administrador FROM miembros WHERE nombre_m = ?");
						ps.setString(1, user);
						ResultSet rs = ps.executeQuery();
						while(rs.next()){
								admin = rs.getBoolean(1);
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("Error userIsAdmin " + e );
				}
				return admin;
		}
		
		/**
		 * Obtiene las ultimas 10 entradas almacenadas
		 * @return devuelve una lista con objetos tipo entradaForo
		 */
		public List<EntradasForo> obtenerUltimasEntradas(){
				List<EntradasForo> entradas = new ArrayList<>();
				try {
						Statement st = conexionSQL.getConnection().createStatement();
						String query = "SELECT nombre_e, descripcion, nombre_m FROM entradaForo ORDER BY id_e DESC LIMIT 10";
						ResultSet rs = st.executeQuery(query);
						
						while(rs.next()){
								String ne = rs.getString(1);
								String desc = rs.getString(2);
								String nm = rs.getString(3);
								
								entradas.add(new EntradasForo(ne, desc, nm));
						}
						conexionSQL.closeConnection();
				} catch (Exception e) {
						System.out.println("Error obtener ultimas entradas " + e );
				}
				return entradas;
		}
		 
}
