package modelo.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class conexionSQL {

		private static Connection conexion;

		public conexionSQL() {
				conexion = null;
		}

		public static Connection getConnection() throws SQLException, ClassNotFoundException {
				String url = "jdbc:mysql://localhost:3306/stcontrol?useSSL=false";
				String user = "root";
				String password = "password";
				try {
						Class.forName("com.mysql.jdbc.Driver");
						conexion = DriverManager.getConnection(url, user, password);
				} catch (SQLException ex) {
						System.out.println("Problema al crear la conexion");
				}
				return conexion;
		}
		
		public static void closeConnection() throws SQLException{
				if(conexion != null){
						conexion.close();
				}
		}
}
