package modelo;

public class Incidencia {
		private int id_i;
		private String hora;
		private String fecha;
		private String tipo;
		private String coment;
		private int id_es;
		private String nombre_m;
		private String estacion;
		private String linea;

		public Incidencia(int id_i, String hora, String fecha, String tipo, String coment, int id_es, String nombre_m) {
				this.id_i = id_i;
				this.hora = hora;
				this.fecha = fecha;
				this.tipo = tipo;
				this.coment = coment;
				this.id_es = id_es;
				this.nombre_m = nombre_m;
		}

		public Incidencia(int id_i, String hora, String fecha, String tipo, String coment, int id_es, String nombre_m, String estacion, String linea) {
				this.id_i = id_i;
				this.hora = hora;
				this.fecha = fecha;
				this.tipo = tipo;
				this.coment = coment;
				this.id_es = id_es;
				this.nombre_m = nombre_m;
				this.estacion = estacion;
				this.linea = linea;
		}

		public Incidencia(int id_i, String hora, String fecha, String tipo, String coment, String estacion, String linea) {
			this.id_i = id_i;
			this.hora = hora;
			this.fecha = fecha;
			this.tipo = tipo;
			this.coment = coment;
			this.estacion = estacion;
			this.linea = linea;
	}

		public Incidencia(int id_i, String hora, String fecha, String tipo, String coment, String nombre_m, String estacion, String linea) {
				this.id_i = id_i;
				this.hora = hora;
				this.fecha = fecha;
				this.tipo = tipo;
				this.coment = coment;
				this.nombre_m = nombre_m;
				this.estacion = estacion;
				this.linea = linea;
		}
		
		

		public Incidencia(String hora, String fecha, String tipo, String coment, int id_es, String nombre_m) {
				this.hora = hora;
				this.fecha = fecha;
				this.tipo = tipo;
				this.coment = coment;
				this.id_es = id_es;
				this.nombre_m = nombre_m;
		}

		public Incidencia(String hora, String fecha, String tipo, String coment, String estacion, String linea) {
				this.hora = hora;
				this.fecha = fecha;
				this.tipo = tipo;
				this.coment = coment;
				this.estacion = estacion;
				this.linea = linea;
		}
		
		

		public int getId_i() {
				return id_i;
		}

		public String getHora() {
				return hora;
		}

		public void setHora(String hora) {
				this.hora = hora;
		}

		public String getFecha() {
				return fecha;
		}

		public void setFecha(String fecha) {
				this.fecha = fecha;
		}

		public String getTipo() {
				return tipo;
		}

		public void setTipo(String tipo) {
				this.tipo = tipo;
		}

		public String getComent() {
				return coment;
		}

		public void setComent(String coment) {
				this.coment = coment;
		}

		public int getId_es() {
				return id_es;
		}

		public void setId_es(int id_es) {
				this.id_es = id_es;
		}

		public String getNombre_m() {
				return nombre_m;
		}

		public void setNombre_m(String nombre_m) {
				this.nombre_m = nombre_m;
		}

		public String getEstacion() {
				return estacion;
		}

		public void setEstacion(String estacion) {
				this.estacion = estacion;
		}

		public String getLinea() {
				return linea;
		}

		public void setLinea(String linea) {
				this.linea = linea;
		}

		@Override
		public String toString() {
				return "Incidencia{" + "id_i=" + id_i + ", hora=" + hora + ", fecha=" + fecha + ", tipo=" + tipo + ", coment=" + coment + ", id_es=" + id_es + ", nombre_m=" + nombre_m + ", estacion=" + estacion + ", linea=" + linea + '}';
		}
		
		
		
}
