package modelo;

public class Miembro {
		private String nombre;
		private String correo;
		private String pass;
		private String linea;
		private boolean admin;

		public Miembro(String nombre, String correo, String pass, String linea, boolean admin) {
				this.nombre = nombre;
				this.correo = correo;
				this.pass = pass;
				this.linea = linea;
				this.admin = admin;
		}

		public Miembro(String nombre, String correo, String pass, String linea) {
				this.nombre = nombre;
				this.correo = correo;
				this.pass = pass;
				this.linea = linea;
		}
		public Miembro(String nombre, String correo, String linea){
				this.nombre = nombre;
				this.correo = correo;
				this.linea = linea;
		}

		public String getNombre() {
				return nombre;
		}

		public void setNombre(String nombre) {
				this.nombre = nombre;
		}

		public String getCorreo() {
				return correo;
		}

		public void setCorreo(String correo) {
				this.correo = correo;
		}

		public String getPass() {
				return pass;
		}

		public void setPass(String pass) {
				this.pass = pass;
		}

		public String getLinea() {
				return linea;
		}

		public void setLinea(String linea) {
				this.linea = linea;
		}

		public boolean isAdmin() {
				return admin;
		}

		@Override
		public String toString() {
				return "Miembro{" + "nombre=" + nombre + ", correo=" + correo + ", pass=" + pass + ", linea=" + linea + ", admin=" + admin + '}';
		}
		
		
		
		
}
