package modelo;

public class Comentario {
		private int id_c;
		private String desc;
		private String nombre_m;
		private int id_e;

		public Comentario(int id_c, String desc, String nombre_m, int id_e) {
				this.id_c = id_c;
				this.desc = desc;
				this.nombre_m = nombre_m;
				this.id_e = id_e;
		}

		public int getId_c() {
				return id_c;
		}

		public void setId_c(int id_c) {
				this.id_c = id_c;
		}

		public String getDesc() {
				return desc;
		}

		public void setDesc(String desc) {
				this.desc = desc;
		}

		public String getNombre_m() {
				return nombre_m;
		}

		public void setNombre_m(String nombre_m) {
				this.nombre_m = nombre_m;
		}

		public int getId_e() {
				return id_e;
		}

		public void setId_e(int id_e) {
				this.id_e = id_e;
		}
		
		
}
