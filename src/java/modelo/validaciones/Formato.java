package modelo.validaciones;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.sql.peticionesSQL;

public class Formato {
		
		/**
		 * Este metodo va a dar formato a una cadena que contenga la linea del metro que se envia del formulario
		 * @param linea Nombre de la linea de metro a la que se le dara formato
		 */
		public String formatoLinea(String linea){
				// Aque aun hay que validar que la linea qe se ingresa tenga el formato correcto
				String[] aux = linea.split(" ");
				
				return aux[1];
		}
		/**
		 * Este metodo convierte un string a una fecha cuyo formato es yyyy-MM-dd
		 * @param fecha El string de la fecha que se desee convertir
		 * @return Devuelve un objeto del tipo java.util.Date
		 */
		public Date convertirFecha(String fecha){
				Date date = null;
				SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
				try {
						date = formato.parse(fecha);
				} catch (ParseException ex) {
						System.out.println("error" + ex);
				}
				return date;
				
		}
}
