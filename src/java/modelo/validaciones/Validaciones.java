/**
 *	Esta clase va a gestionar todas las validaciones que sean necesarias hacer en la web
 */
package modelo.validaciones;

public class Validaciones {
		
		/**
		 * Valida una direccion de correo electronico.
		 * @param correo Direccion de correo que se quiere validar
		 * @return Devuelve si la cadena es valida o no
		 */
		public boolean validarCorreo(String correo){
				String pattern = "(?i)^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
				return (correo.matches(pattern) && correo.length() <= 30);
		}
		
		/**
		 * Valida una cadena sencilla
		 * @param cadena Cadena que se va  a validar
		 * @param size Tamaño de la cadena
		 * @return Devuelve si es valida o no la cadena
		 */
		public boolean validacionCadenaBasica(String cadena, int size){
				String pattern = "(?i)[-a-z0-9~!$%^&*_=+,ñ. }{\\?]{0," + size + "}" ;
				return cadena.matches(pattern);
		}
		
		/**
		 * Valida una contraseña
		 * @param p1 Contraseña
		 * @param p2 Verificacion de p1
		 * @return Devuelve si es valida o no la contraseña
		 */
		public boolean validarPassword(String p1, String p2){
				return (p1.matches( "(?i)[-a-z0-9~!$%^&*_+ñ.}{\\?]{0,20}" ) && p1.equals(p2));
		}
		
		/**
		 * Valida una fecha
		 * @param fecha Fecha a validar
		 * @return Devuelve si la fecha es valida o no
		 */
		public boolean validarFecha(String fecha){
				return fecha.matches("(?i)^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$");
		}
		
		/**
		 * Valida una hora
		 * @param hora Hora que se quiere validar
		 * @return Devuelve si la hora es valida o no
		 */
		public boolean validarHora(String hora){
				return hora.matches("(?i)^(20|21|22|23|[01]\\d|\\d)((:[0-5]\\d){1,2})$");
		}
		
		// https://www.freeformatter.com/java-regex-tester.html
}
