package modelo;

public class EntradasForo {
		private int id_e;
		private String nombre_e; 
		private String desc;
		private String nombre_m;

		public EntradasForo(int id_e, String nombre_e, String desc, String nombre_m) {
				this.id_e = id_e;
				this.nombre_e = nombre_e;
				this.desc = desc;
				this.nombre_m = nombre_m;
		}

		public EntradasForo(String nombre_e, String desc, String nombre_m) {
				this.nombre_e = nombre_e;
				this.desc = desc;
				this.nombre_m = nombre_m;
		}
		

		public int getId_e() {
				return id_e;
		}

		public void setId_e(int id_e) {
				this.id_e = id_e;
		}

		public String getNombre_e() {
				return nombre_e;
		}

		public void setNombre_e(String nombre_e) {
				this.nombre_e = nombre_e;
		}

		public String getDesc() {
				return desc;
		}

		public void setDesc(String desc) {
				this.desc = desc;
		}

		public String getNombre_m() {
				return nombre_m;
		}

		public void setNombre_m(String nombre_m) {
				this.nombre_m = nombre_m;
		}
		
		
}
