package Controladores;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.Comentario;
import modelo.EntradasForo;
import modelo.Incidencia;
import modelo.Miembro;
import modelo.sql.peticionesSQL;

public class ObtenerDatosMiembro extends HttpServlet {

		protected void processRequest(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				response.setContentType("text/html;charset=UTF-8");
				try (PrintWriter out = response.getWriter()) {
						/*
						 * TODO output your page here. You may use following sample code.
						 */
						out.println("<!DOCTYPE html>");
						out.println("<html>");
						out.println("<head>");
						out.println("<title>Servlet ObtederDatosUser</title>");						
						out.println("</head>");
						out.println("<body>");
						out.println("<h1>Servlet ObtederDatosUser at " + request.getContextPath() + "</h1>");
						out.println("</body>");
						out.println("</html>");
				}
		}

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				doPost(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				HttpSession sesion = request.getSession();
				
				peticionesSQL peticion = new peticionesSQL();
				// Comprueba que el usuario ya tenga una sesion iniciada, en caso de que no la tenga, lleva al usuario al login
				if(sesion != null){
						String user = (String) sesion.getAttribute("username");
						// String user = "Enrique";
						// Aqui deben ir las validacinoes para el nombre de usuario
						
						// Verifica que el usuario exista
						if(peticion.existeMiembro(user)){
								// Aqui hay que obtener los datos del usuario
								Miembro miembro = peticion.getMiembro(user);
								List<Incidencia> reportes = peticion.getIncidencias(user);
								List<EntradasForo> entradas = peticion.getEntradas(user);
								List<Comentario> comentarios = peticion.getComentarios(user);
								List<Incidencia> reportesLinea = peticion.getIncidenciasLinea(miembro.getLinea());
								
								// Se crean objetos de las clases Gson y JsonObject
								Gson gson = new Gson();
								JsonObject json = new JsonObject();
								
								// Se agregan los valores necesarios en la respuesta al objeto json 
								json.addProperty("datosUser", gson.toJson(miembro));
								json.addProperty("reportesUser", gson.toJson(reportes));
								json.addProperty("reportesLinea", gson.toJson(reportesLinea));
								json.addProperty("entradasUser", gson.toJson(entradas));
								json.addProperty("comentarios", gson.toJson(comentarios));
								
								// Se convierte el objeto json en un objeto string para que pueda ser la respuesta
								String respuesta = gson.toJson(json);
								
								// Se configura la respuesta especificando que es un objeto del tipo json, y se escribe el objeto en la respuesta
								response.setContentType("application/json");
								response.setCharacterEncoding("UTF-8");
								response.getWriter().write(respuesta);
						}
						else{
								response.sendRedirect("pages/pageLogin/login.jsp");
						}
						
				}
				else{
						response.sendRedirect("pages/pageLogin/login.jsp");
				}
				
		}

		@Override
		public String getServletInfo() {
				return "Short description";
		}// </editor-fold>

}
