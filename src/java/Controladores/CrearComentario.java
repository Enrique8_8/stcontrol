package Controladores;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.sql.peticionesSQL;
import modelo.validaciones.Validaciones;

public class CrearComentario extends HttpServlet {


		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				doPost(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				peticionesSQL peticion = new peticionesSQL();
				Validaciones v = new Validaciones();
				JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);
				
				int idEntrada = data.get("idEntrada").getAsInt();
				String comentario = data.get("comentario").getAsString();
				
				HttpSession  sesion = request.getSession();
				String username = (String) sesion.getAttribute("username");
				
				String json;
				if(username != null && peticion.existeMiembro(username)){
						if(peticion.existeEntrada(idEntrada)){
								if(v.validacionCadenaBasica(comentario, 120)){
										peticion.crearComentario(comentario, username, idEntrada);
										int idComentario = peticion.getIDComentario(idEntrada, comentario);
										JsonObject jsonPrev = new JsonObject();
										jsonPrev.addProperty("error", Boolean.FALSE);
										jsonPrev.addProperty("idComentario", idComentario);
										jsonPrev.addProperty("usuario", username);
										json = new Gson().toJson(jsonPrev);
								} else{
										JsonObject jsonPrev = new JsonObject();
										jsonPrev.addProperty("error", Boolean.TRUE);
										jsonPrev.addProperty("tipo", "datos");
										json = new Gson().toJson(jsonPrev);
								}
						} else{
								JsonObject jsonPrev = new JsonObject();
								jsonPrev.addProperty("error", Boolean.TRUE);
								jsonPrev.addProperty("tipo", "entrada");
								json = new Gson().toJson(jsonPrev);
						}		
				}else{
						JsonObject jsonPrev = new JsonObject();
						jsonPrev.addProperty("error", Boolean.TRUE);
						jsonPrev.addProperty("tipo", "usuario");
						json = new Gson().toJson(jsonPrev);
				}
				
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write(json);
				
		}
}
