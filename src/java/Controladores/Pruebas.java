/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import modelo.validaciones.Validaciones;

/**
 *
 * @author enrique
 */
public class Pruebas {
		public static void main(String[] args) {
				Validaciones v = new Validaciones();
				
				String cadena1 = "abecedari@bar.com",
						cadena2 = "2020-10-21",
						cadena3 = "99:20:00",
						cadena4 = "", 
						cadena5 = "-- Coso";
				if(v.validarHora(cadena3)){
						System.out.println("La cadena esta bien: " + cadena3);
				}else
						System.out.println("cadena invalida: " + cadena3);
		}
		
}
