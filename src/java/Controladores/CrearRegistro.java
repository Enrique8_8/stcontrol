package Controladores;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.Incidencia;
import modelo.sql.peticionesSQL;
import modelo.validaciones.Formato;
import modelo.validaciones.Validaciones;

public class CrearRegistro extends HttpServlet {
		@Override
		protected  void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				doPost(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				Validaciones v = new Validaciones();
				Formato f = new Formato();
				peticionesSQL peticion = new peticionesSQL();
			
				// Obtencion de los datos
				String hora = request.getParameter("hora");
				String tipo = request.getParameter("tipo"); 
				String fecha = request.getParameter("dia");
				String comentario = request.getParameter("desc");
				String est = request.getParameter("estaciones");
				String linea = request.getParameter("linea");
				
				linea = linea.substring(6);
				HttpSession sesion = request.getSession();
				String nombre = (String) sesion.getAttribute("username");
				
				if(tipo.equals("otros")){
						tipo = request.getParameter("otro");
				}
				// hora += ":00";
				
				//	Validacion y formato de los datos y la sesion
				int id_es = peticion.getEstacion(est, linea);
				
				if(nombre != null){
						if( v.validarHora(hora) && v.validarFecha(fecha) && v.validacionCadenaBasica(tipo, 20) && v.validacionCadenaBasica(comentario, 120) ){
								// Creacion del objeto Incidencia
								Incidencia reporte = new Incidencia(hora, fecha, tipo, comentario, id_es, nombre);

								// Creacion del registro
								peticion.crearIncidencia(reporte);
								peticion.incrementarEst(id_es);
 
								response.sendRedirect("pages/pageRedireccionamiento/redireccionamiento.jsp?creado=true");
						}else
								response.sendRedirect("pages/pageCrearRegistro/crearRegistro.jsp?errorDatos=true");
						
				}else
						// Redireccionamiento en caso de que no haya una sesion iniciada
						response.sendRedirect("pages/pageLogin/login.jsp?redireccionado=true");
				
				 
				
		}

		@Override
		public String getServletInfo() {
				return "Short description";
		}// </editor-fold>

}