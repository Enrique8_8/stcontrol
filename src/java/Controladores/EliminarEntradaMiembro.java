package Controladores;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.sql.peticionesSQL;
import modelo.validaciones.Validaciones;

public class EliminarEntradaMiembro extends HttpServlet {

		protected void processRequest(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				response.setContentType("text/html;charset=UTF-8");
				try (PrintWriter out = response.getWriter()) {

						out.println("<!DOCTYPE html>");
						out.println("<html>");
						out.println("<head>");
						out.println("<title>Servlet EliminarEntradaMiembro</title>");						
						out.println("</head>");
						out.println("<body>");
						out.println("<h1>Servlet EliminarEntradaMiembro at " + request.getContextPath() + "</h1>");
						out.println("</body>");
						out.println("</html>");
				}
		}

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				processRequest(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				
				HttpSession sesion = request.getSession();
				String user = (String)sesion.getAttribute("username");
				String json;
				if(user != null){
						peticionesSQL peticion = new peticionesSQL();
						JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);
						
						int idReporte = data.get("id").getAsInt();
						if(peticion.existeEntrada(idReporte, user)){
								peticion.borrarEntrada(idReporte);
								JsonObject jsonPrev = new JsonObject();
								jsonPrev.addProperty("eliminado", Boolean.TRUE);
								json = new Gson().toJson(jsonPrev);
						}
						else{
								JsonObject jsonPrev = new JsonObject();
								jsonPrev.addProperty("eliminado", Boolean.FALSE);
								json = new Gson().toJson(jsonPrev);
						}
				}else{
						JsonObject jsonPrev = new JsonObject();
						jsonPrev.addProperty("cuenta", "error");
						json = new Gson().toJson(jsonPrev);
				}
				
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write(json);
				
		}

		@Override
		public String getServletInfo() {
				return "Short description";
		}// </editor-fold>

}
