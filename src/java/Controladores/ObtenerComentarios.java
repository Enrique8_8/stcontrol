package Controladores;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Comentario;
import modelo.sql.peticionesSQL;
import modelo.validaciones.Validaciones;

public class ObtenerComentarios extends HttpServlet {

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				doPost(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				peticionesSQL peticion = new peticionesSQL();
				JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);
				
				int idEntrada = data.get("idEntrada").getAsInt();
				List<Comentario> comentarios = peticion.getComentarios(idEntrada);
				
				String json = new Gson().toJson(comentarios);
				
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write(json);
		}


}
