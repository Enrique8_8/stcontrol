package Controladores;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.sql.peticionesSQL;

public class EliminarCuentaMiembro extends HttpServlet {

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				doPost(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				HttpSession sesion = request.getSession();
				peticionesSQL peticion = new peticionesSQL();
				
				String user = (String) sesion.getAttribute("username");

				if(user != null){
					if(peticion.existeMiembro(user)){
						// Se borran las cookies que hay
						Cookie[] coockies = request.getCookies();
						
						for(Cookie cock  : coockies){
								cock.setMaxAge(0);
								cock.setPath("/");
								response.addCookie(cock);
						}
						List<Integer> ids = peticion.getIdEstUser(user);
						for(int id : ids){
								peticion.disminuirEst(id);
						}
						
						// Se borra el miembro de la base de datos
						peticion.borrarMiembro(user);
						
						// Se elimina la sesion
						sesion.invalidate();
						
						response.sendRedirect("pages/pageIndex/index.jsp");
					}else{
						response.sendRedirect("pages/pageLogin/login.jsp?redireccionado=true");
					}
				}else{
					response.sendRedirect("pages/pageLogin/login.jsp?redireccionado=true");
				}

		}

}
