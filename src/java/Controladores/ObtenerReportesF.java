package Controladores;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Incidencia;
import modelo.sql.peticionesSQL;

public class ObtenerReportesF extends HttpServlet {

		protected void processRequest(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				response.setContentType("text/html;charset=UTF-8");
				try (PrintWriter out = response.getWriter()) {
						/*
						 * TODO output your page here. You may use following sample code.
						 */
						out.println("<!DOCTYPE html>");
						out.println("<html>");
						out.println("<head>");
						out.println("<title>Servlet ObtenerReportesF</title>");						
						out.println("</head>");
						out.println("<body>");
						out.println("<h1>Servlet ObtenerReportesF at " + request.getContextPath() + "</h1>");
						out.println("</body>");
						out.println("</html>");
				}
		}

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				doPost(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				
				JsonObject resJsonObject = new Gson().fromJson(request.getReader(), JsonObject.class);
				
				JsonArray lineas = resJsonObject.get("lineas").getAsJsonArray();
				JsonArray estaciones = resJsonObject.get("estaciones").getAsJsonArray();
				JsonArray fechas = resJsonObject.get("fechas").getAsJsonArray();
				
				String peticion = "select nombre_es, linea_es, hora_i, fecha_i, tipo_i, comentario, id_i from incidencias as i inner join estaciones as  e on e.id_es=i.id_es where ";
				
				if(lineas.size() == 0 && estaciones.size() == 0 && fechas.size() == 0){
						System.out.println("No hay filtros");
				}
				else{
						if(lineas.size() != 0){
								for(JsonElement linea : lineas){
										peticion += "linea_es = "  + linea +" or ";
								}
						}

						if(estaciones.size() != 0){
								for(JsonElement est : estaciones){
										JsonObject dataEst = new Gson().fromJson(est, JsonObject.class);
										String linea = dataEst.get("linea").getAsString();
										String estacion = dataEst.get("estacion").getAsString();

										peticion += "(linea_es = '" + linea + "' and nombre_es = '" + estacion + "') or ";
								}
						}

						if(fechas.size() != 0){
								if(fechas.size() == 1){
										peticion += "fecha_i = " + fechas.get(0);
								}else if(fechas.size() == 2){
										peticion += "fecha_i between " + fechas.get(0) + " and " + fechas.get(1);
								}
						}else{
								peticion = peticion.substring(0, peticion.length() - 3);
						}
						
						peticionesSQL sql = new peticionesSQL();
						List<Incidencia> incidencias = sql.getIncidenciasF(peticion);
						
						String json = new Gson().toJson(incidencias);
						
						response.setContentType("application/json");
						response.setCharacterEncoding("UTF-8");
						response.getWriter().write(json);
				}
				
				
				
		}

		@Override
		public String getServletInfo() {
				return "Short description";
		}// </editor-fold>

}
