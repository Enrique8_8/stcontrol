package Controladores;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Incidencia;
import modelo.sql.peticionesSQL;

public class ObtenerReportes extends HttpServlet {

		protected void processRequest(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				response.setContentType("text/html;charset=UTF-8");
				try (PrintWriter out = response.getWriter()) {
						/*
						 * TODO output your page here. You may use following sample code.
						 */
						out.println("<!DOCTYPE html>");
						out.println("<html>");
						out.println("<head>");
						out.println("<title>Servlet ObtenerReportes</title>");						
						out.println("</head>");
						out.println("<body>");
						out.println("<h1>Servlet ObtenerReportes at " + request.getContextPath() + "</h1>");
						out.println("</body>");
						out.println("</html>");
				}
		}

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				doPost(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				peticionesSQL peticion = new peticionesSQL();
				
				List<Incidencia> reportes =  peticion.getIncidencias();
				
				String json = new Gson().toJson(reportes);
				
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write(json);
				
		}

		@Override
		public String getServletInfo() {
				return "Short description";
		}// </editor-fold>

}
