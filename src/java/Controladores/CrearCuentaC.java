/*
 *	Este servlet va a gestionar el crear una cuenta en la pagina
 */
package Controladores;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.sql.peticionesSQL;
import modelo.validaciones.Formato;
import modelo.validaciones.Validaciones;

public class CrearCuentaC extends HttpServlet {

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				
				/* Crecion de los objetos necesarios para ejecutar todo esto */
				Validaciones v = new Validaciones();
				Formato f = new Formato();
				peticionesSQL peticion = new peticionesSQL();
				
				// Obtencion de los datos del formulario
				String username = request.getParameter("username");
				String email = request.getParameter("email");
				String password = request.getParameter("user-password");
				String vpassword = request.getParameter("vpassword");
				String lineaInv = request.getParameter("linea");
				
				// Aqui se van a validar los datos, y se les da formato	
				String linea = f.formatoLinea(lineaInv);
				
				if (v.validacionCadenaBasica(username, 20) && !peticion.existeMiembro(username) && v.validarCorreo(email) && v.validarPassword(password, vpassword) && v.validacionCadenaBasica(linea, 2) ) {
						// En caso de que todos los datos sean validos
						
						// Se crea la sesion del usuario
						HttpSession sesion = request.getSession();
						sesion.setAttribute("username", username);
						
						// Se crea la cuenta en la base de datos
						peticion.crearMiembro(username, email, password, linea, false);
						
						// Se redirecciona al index, donde ya se podra navegar por toda la web
						response.sendRedirect("pages/pageCuentaUser/cuentaUsuario.jsp");
				} else {
						// Redirecciona a la pagina de login en caso de que no haya sido validos los datos ingresados
						response.sendRedirect("pages/pageLogin/login.jsp?caracteresInv=true");
				}
		}

		@Override
		public String getServletInfo() {
				return "Short description";
		}// </editor-fold>
}
