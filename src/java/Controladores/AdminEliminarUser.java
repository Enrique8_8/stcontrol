package Controladores;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.sql.peticionesSQL;

public class AdminEliminarUser extends HttpServlet {

		protected void processRequest(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				response.setContentType("text/html;charset=UTF-8");
				try (PrintWriter out = response.getWriter()) {
						/*
						 * TODO output your page here. You may use following sample code.
						 */
						out.println("<!DOCTYPE html>");
						out.println("<html>");
						out.println("<head>");
						out.println("<title>Servlet AdminEliminarUser</title>");						
						out.println("</head>");
						out.println("<body>");
						out.println("<h1>Servlet AdminEliminarUser at " + request.getContextPath() + "</h1>");
						out.println("</body>");
						out.println("</html>");
				}
		}

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				doPost(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				
				HttpSession sesion = request.getSession();
				String user = (String) sesion.getAttribute("username");
				
				peticionesSQL peticiones = new peticionesSQL();
				
				String json;
				if(peticiones.userIsAdmin(user)){
						JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);
						String userEliminar = data.get("userDelete").getAsString();
						
						peticiones.borrarMiembro(userEliminar);
						
						JsonObject jsonPrev = new JsonObject();
						jsonPrev.addProperty("eliminado", Boolean.TRUE);
						json = new Gson().toJson(jsonPrev);
						
				}else{
						JsonObject jsonPrev = new JsonObject();
						jsonPrev.addProperty("eliminado", Boolean.FALSE);
						json = new Gson().toJson(jsonPrev);
				}
				
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write(json);
				
		}

		/**
		 * Returns a short description of the servlet.
		 *
		 * @return a String containing servlet description
		 */
		@Override
		public String getServletInfo() {
				return "Short description";
		}// </editor-fold>

}
