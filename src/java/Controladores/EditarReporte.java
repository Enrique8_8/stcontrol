package Controladores;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.Incidencia;
import modelo.sql.peticionesSQL;
import modelo.validaciones.Formato;
import modelo.validaciones.Validaciones;

/**
 *
 * @author enrique
 */
public class EditarReporte extends HttpServlet {

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request  servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException      if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		try (PrintWriter out = response.getWriter()) {
			/*
			 * TODO output your page here. You may use following sample code.
			 */
			out.println("<!DOCTYPE html>");
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Servlet EditarReporte</title>");
			out.println("</head>");
			out.println("<body>");
			out.println("<h1>Servlet EditarReporte at " + request.getContextPath() + "</h1>");
			out.println("</body>");
			out.println("</html>");
		}
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
	// + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request  servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException      if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
		 * Handles the HTTP <code>POST</code> method.
		 *
		 * @param request servlet request
		 * @param response servlet response
		 * @throws ServletException if a servlet-specific error occurs
		 * @throws IOException if an I/O error occurs
		 */
		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				Validaciones v = new Validaciones();
				Formato f = new Formato();
				peticionesSQL peticion = new peticionesSQL();
			
				// Obtencion de los datos
				int idReporte = Integer.parseInt(request.getParameter("idReporte"));
				String hora = request.getParameter("hora");
				String tipo = request.getParameter("tipo"); 
				String fecha = request.getParameter("dia");
				String comentario = request.getParameter("desc");
				String est = request.getParameter("estaciones");
				String linea = request.getParameter("linea");
				
				// Obtencion del nombre de usuario
				HttpSession sesion = request.getSession();
				String nombre = (String) sesion.getAttribute("username");
				
				if(tipo.equals("otros")){
						tipo = request.getParameter("otro");
				}
				linea = linea.substring(6);
				//	Validacion y formato de los datos y la sesion
				int id_es = peticion.getEstacion(est, linea);
				
				Incidencia reporte = new Incidencia(idReporte, hora, fecha, tipo, comentario, id_es, nombre);
				
				if(peticion.existeReporte(idReporte, nombre)){
						if(nombre != null){
								if( v.validarFecha(fecha) && v.validarHora(hora) && v.validacionCadenaBasica(tipo, 20) && v.validacionCadenaBasica(comentario, 50) ){
										// Verifica si se cambio la estacion, para saber si modificar el registro o no
										int id_esP = peticion.getEstacion(idReporte);
										if(id_esP != id_es){
												peticion.disminuirEst(id_esP);
												peticion.incrementarEst(id_es);
										}
										peticion.editarIncidencia(reporte);
										response.sendRedirect("pages/pageCuentaUser/cuentaUsuario.jsp?edicion=true");
								}else{
								// AQUI HAY QUE REVISAR EL REDIRECCIONAMIENTO, PUES CREO QUE VA CON UNA ID
										String url = "pages/pageEditarReporte/editarReporte.jsp?errorDatos=true&id=" + Integer.toString(idReporte);
										response.sendRedirect(url);
								}
						}else
							// Redireccionamiento en caso de que no haya una sesion iniciada
							response.sendRedirect("pages/pageLogin/login.jsp?redireccionado=true");
				}else{
						// En este caso, el reporte no existe
						response.sendRedirect("pages/pageRedireccionamiento/redireccionamiento.jsp?editado=false");
				}
		}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

}
