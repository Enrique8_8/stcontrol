package Controladores;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.sql.peticionesSQL;
import modelo.validaciones.Validaciones;

public class CrearCuentaAdmin extends HttpServlet {
		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				doPost(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				
				HttpSession sesion = request.getSession();
				String user = (String) sesion.getAttribute("username");
				
				peticionesSQL peticiones = new peticionesSQL();
				
				if(peticiones.userIsAdmin(user)){
						String username = request.getParameter("username");
						String correo = request.getParameter("correo");
						String contra1 = request.getParameter("pass");
						String contra2 = request.getParameter("pass2");
						String linea = request.getParameter("linea");
						String tipo = request.getParameter("tipoC");
						boolean admin = false;
						if(tipo.equals("a")){
								admin = true;
						}
						
						Validaciones v = new Validaciones();
						System.out.println("username: " + username + " correo " + correo + " contra1 " + contra1 + " contra2 " + contra2 + " linea " + linea + " tipo " + tipo);
						if(v.validarPassword(contra1, contra2) && v.validarCorreo(correo) && v.validacionCadenaBasica(username, 20) && v.validacionCadenaBasica(linea, 2) && !peticiones.existeMiembro(username)){
								peticiones.crearMiembro(username, correo, contra1, linea, admin);
								response.sendRedirect("pages/pageAdmin/admin.html?creada=true");
						}else{
								response.sendRedirect("pages/pageAdmin/admin.html?creada=false");
						}
				}else{
						response.sendRedirect("pages/pageLogin/login.jsp");
				}
		}

		/**
		 * Returns a short description of the servlet.
		 *
		 * @return a String containing servlet description
		 */
		@Override
		public String getServletInfo() {
				return "Short description";
		}// </editor-fold>

}
