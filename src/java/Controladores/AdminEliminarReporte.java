package Controladores;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.sql.peticionesSQL;

public class AdminEliminarReporte extends HttpServlet {


		// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
		/**
		 * Handles the HTTP <code>GET</code> method.
		 *
		 * @param request servlet request
		 * @param response servlet response
		 * @throws ServletException if a servlet-specific error occurs
		 * @throws IOException if an I/O error occurs
		 */
		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				doPost(request, response);
		}

		/**
		 * Handles the HTTP <code>POST</code> method.
		 *
		 * @param request servlet request
		 * @param response servlet response
		 * @throws ServletException if a servlet-specific error occurs
		 * @throws IOException if an I/O error occurs
		 */
		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				HttpSession sesion = request.getSession();
				String user = (String) sesion.getAttribute("username");
				
				peticionesSQL peticiones = new peticionesSQL();
				
				String json;
				if(peticiones.userIsAdmin(user)){
						JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);
						int idReporteEliminar = data.get("idReporte").getAsInt();
						int id_es = peticiones.getEstacion(idReporteEliminar);
						
						peticiones.borrarIncidencia(idReporteEliminar);
						peticiones.disminuirEst(id_es);
						
						JsonObject jsonPrev = new JsonObject();
						jsonPrev.addProperty("eliminado", Boolean.TRUE);
						json = new Gson().toJson(jsonPrev);
						
				}else{
						JsonObject jsonPrev = new JsonObject();
						jsonPrev.addProperty("eliminado", Boolean.FALSE);
						json = new Gson().toJson(jsonPrev);
				}
				
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write(json);
				
		}

		/**
		 * Returns a short description of the servlet.
		 *
		 * @return a String containing servlet description
		 */
		@Override
		public String getServletInfo() {
				return "Short description";
		}// </editor-fold>

}
