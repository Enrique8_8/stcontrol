package Controladores;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.EntradasForo;
import modelo.sql.peticionesSQL;

public class ObtenerEntrada extends HttpServlet {

		protected void processRequest(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				response.setContentType("text/html;charset=UTF-8");
				try (PrintWriter out = response.getWriter()) {
						/*
						 * TODO output your page here. You may use following sample code.
						 */
						out.println("<!DOCTYPE html>");
						out.println("<html>");
						out.println("<head>");
						out.println("<title>Servlet ObtenerEntrada</title>");						
						out.println("</head>");
						out.println("<body>");
						out.println("<h1>Servlet ObtenerEntrada at " + request.getContextPath() + "</h1>");
						out.println("</body>");
						out.println("</html>");
				}
		}

		// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				doPost(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				peticionesSQL peticion = new peticionesSQL();
				
				// Obtencion de la id de la entrada
				JsonObject pJsonObject = new Gson().fromJson( request.getReader(), JsonObject.class );
				int id = pJsonObject.get("id").getAsInt();
				
				// Obtencion del usuario
				HttpSession sesion = request.getSession();
				String miembro = (String) sesion.getAttribute("username");
				
				String json;
				
				// Validacion de que exista la entrada
				if(peticion.existeEntrada(id, miembro)){
						EntradasForo entrada = peticion.getEntrada(id);
						json = new Gson().toJson(entrada);
				}else{
						JsonObject jsonPrev = new JsonObject();
						jsonPrev.addProperty("error", "usuario");
						json = new Gson().toJson(jsonPrev);
				}
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write(json);
				
		}

		@Override
		public String getServletInfo() {
				return "Short description";
		}// </editor-fold>

}
