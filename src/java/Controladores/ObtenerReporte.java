package Controladores;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.Incidencia;
import modelo.sql.peticionesSQL;

public class ObtenerReporte extends HttpServlet {

		protected void processRequest(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				response.setContentType("text/html;charset=UTF-8");
				try (PrintWriter out = response.getWriter()) {
						/*
						 * TODO output your page here. You may use following sample code.
						 */
						out.println("<!DOCTYPE html>");
						out.println("<html>");
						out.println("<head>");
						out.println("<title>Servlet ObtenerReporte</title>");						
						out.println("</head>");
						out.println("<body>");
						out.println("<h1>Servlet ObtenerReporte at " + request.getContextPath() + "</h1>");
						out.println("</body>");
						out.println("</html>");
				}
		}

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				doPost(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				peticionesSQL peticion = new peticionesSQL();
				JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);
				
				int idReporte = data.get("id").getAsInt();
				
				HttpSession sesion = request.getSession();
				String nombre = (String) sesion.getAttribute("username");
				
				if(peticion.existeReporte(idReporte, nombre)){
						// Verificacion de que el reporte exista
						Incidencia reporte = peticion.getIncidencias(idReporte);
						String json = new Gson().toJson(reporte);
						
						response.setContentType("application/json");
						response.setCharacterEncoding("UTF-8");
						response.getWriter().write(json);
				}else
						// En caso de que no exista
						response.sendRedirect("pages/pageRedireccionamiento/redireccionamiento.jsp?inexistente=true");
		}

		@Override
		public String getServletInfo() {
				return "Short description";
		}// </editor-fold>

}
