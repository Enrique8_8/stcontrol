package Controladores;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.sql.peticionesSQL;
import modelo.validaciones.Validaciones;

public class CrearEntradaF extends HttpServlet {

		protected void processRequest(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				response.setContentType("text/html;charset=UTF-8");
				try (PrintWriter out = response.getWriter()) {
						/*
						 * TODO output your page here. You may use following sample code.
						 */
						out.println("<!DOCTYPE html>");
						out.println("<html>");
						out.println("<head>");
						out.println("<title>Servlet CrearEntradaF</title>");						
						out.println("</head>");
						out.println("<body>");
						out.println("<h1>Servlet CrearEntradaF at " + request.getContextPath() + "</h1>");
						out.println("</body>");
						out.println("</html>");
				}
		}

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				processRequest(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				// Definicion de los objetos generales que se van a ocupar
				HttpSession sesion = request.getSession();
				peticionesSQL peticion = new peticionesSQL();
				
				// Obtencion de los datos del formulario
				String nombre_e = request.getParameter("nameE");
				String descripcion = request.getParameter("ent");
				
				String miembro = (String) sesion.getAttribute("username");
				Validaciones v = new Validaciones();
				
				// Validacion de los datos del usario y la sesion
				if(miembro != null){
						if(v.validacionCadenaBasica(nombre_e, 30) && v.validacionCadenaBasica(descripcion, 120)){
								peticion.crearEntrada(nombre_e, descripcion, miembro);

								 response.sendRedirect("pages/pageRedireccionamiento/redireccionamiento.jsp?creado=true");
						}else
								response.sendRedirect("pages/pageForo/foro.jsp?errorDatos=true");
				}
				else
						// Redireccionamiento en caso de que no haya una sesion iniciada
						response.sendRedirect("pages/pageLogin/login.jsp?redireccionado=true");
				
		}

		@Override
		public String getServletInfo() {
				return "Short description";
		}// </editor-fold>

}
