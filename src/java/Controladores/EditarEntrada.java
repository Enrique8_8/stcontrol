package Controladores;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.EntradasForo;
import modelo.sql.peticionesSQL;
import modelo.validaciones.Validaciones;

public class EditarEntrada extends HttpServlet {

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		try (PrintWriter out = response.getWriter()) {
			/*
			 * TODO output your page here. You may use following sample code.
			 */
			out.println("<!DOCTYPE html>");
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Servlet EditarEntrada</title>");
			out.println("</head>");
			out.println("<body>");
			out.println("<h1>Servlet EditarEntrada at " + request.getContextPath() + "</h1>");
			out.println("</body>");
			out.println("</html>");
		}
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
	// + sign on the left to edit the code.">
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		peticionesSQL peticion = new peticionesSQL();
		HttpSession sesion = request.getSession();

		int id = Integer.parseInt(request.getParameter("idEntrada"));
		String nombre_e = request.getParameter("preg");
		String desc = request.getParameter("desc");

		String miembro = (String) sesion.getAttribute("username");

		Validaciones v = new Validaciones();
		if (peticion.existeMiembro(miembro)) {
			// Hay que validar los datos
			if (peticion.existeEntrada(id, miembro)) {
				if (v.validacionCadenaBasica(nombre_e, 30) && v.validacionCadenaBasica(desc, 120)) {
					// Verifica que la entrada exista, y sea de dicho miembro
					EntradasForo entrada = new EntradasForo(id, nombre_e, desc, nombre_e);
					peticion.editarEntrada(entrada);

					response.sendRedirect("pages/pageCuentaUser/cuentaUsuario.jsp?editado=true");
				} else {
					String route = "pages/pageEditarEntrada/editarEntrada.jsp?id=" + id + "&errorDatos=true";
					response.sendRedirect(route);
				}

			} else {
				// En este caso, el reporte no existe
				response.sendRedirect("pages/pageRedireccionamiento/redireccionamiento.jsp?editado=false");
			}
		}else{
			// Redireccionamiento en caso de que no haya una sesion iniciada
			response.sendRedirect("pages/pageLogin/login.jsp?redireccionado=true");
		}
	}

	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

}
