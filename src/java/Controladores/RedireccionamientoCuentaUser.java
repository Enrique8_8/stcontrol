package Controladores;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.sql.peticionesSQL;

public class RedireccionamientoCuentaUser extends HttpServlet {
		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				doPost(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				HttpSession sesion = request.getSession();
				String user =(String) sesion.getAttribute("username");
				
				if(user != null){
						peticionesSQL peticion = new peticionesSQL();
						if(peticion.existeMiembro(user)){
								if(peticion.userIsAdmin(user))
										response.sendRedirect("pages/pageAdmin/admin.jsp");
								else
										response.sendRedirect("pages/pageCuentaUser/cuentaUsuario.jsp");
						}
						else
								response.sendRedirect("pages/pageLogin/login.jsp?redireccionado=true");
				}else
						response.sendRedirect("pages/pageLogin/login.jsp?redireccionado=true");
		}

		/**
		 * Returns a short description of the servlet.
		 *
		 * @return a String containing servlet description
		 */
		@Override
		public String getServletInfo() {
				return "Short description";
		}// </editor-fold>

}
