package Controladores;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.Miembro;
import modelo.sql.peticionesSQL;
import modelo.validaciones.Validaciones;

public class EditarDatosMiembro extends HttpServlet {

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				doPost(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				
				// Obtencion de los datos del formulario
				String correo = request.getParameter("correo");
				String contra = request.getParameter("password");
				String linea = request.getParameter("linea");
				
				linea = linea.substring(6);
				
				// Obtencion del nombre de usuario
				HttpSession sesion = request.getSession();
				String user = (String) sesion.getAttribute("username");
				
				peticionesSQL peticion = new peticionesSQL();
				Validaciones v = new Validaciones();
				
				if(user != null){
						if( v.validarCorreo(correo) && v.validacionCadenaBasica(linea, 2) && v.validacionCadenaBasica(contra, 20) ){
							Miembro miembro = new Miembro(user, correo, contra, linea);
							peticion.editarMiembro(miembro);
							response.sendRedirect("pages/pageCuentaUser/cuentaUsuario.jsp?edicion=true");
							
						}else
							response.sendRedirect("pages/pageCuentaUser/cuentaUsuario.jsp?edicion=false");
					
				}else{
						response.sendRedirect("pages/pageLogin/login.jsp?redireccionado=true");
				}
		}

}
