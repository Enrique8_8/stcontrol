package Controladores;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.Miembro;
import modelo.sql.peticionesSQL;
import modelo.validaciones.Validaciones;

public class InicioSesion extends HttpServlet {

		
		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				System.out.println("Entra");
				Validaciones v = new Validaciones();
				peticionesSQL peticion = new peticionesSQL();
				
				// Obtencion de los datos ingresados en el formulario
				String username = request.getParameter("username");
				String password = request.getParameter("user-password");
				
				// Validacion de los datos y de la existencia del miembro
				if(v.validacionCadenaBasica(username, 20) && peticion.existeMiembro(username) && v.validacionCadenaBasica(password, 20)){
						// Obtencion de los datos del miembro almacenados
						Miembro user = peticion.getMiembro(username);
						
						// Confirmacion de que los datos son correctos
						if(user.getNombre().equals(username) && user.getPass().equals(password)){
								// Comprobacion en caso de que el usuario sea administrador
								HttpSession sesion = request.getSession();
								sesion.setAttribute("username", username);
								if(user.isAdmin()){
										// Redireccionamiento a la pagina de administradores
										response.sendRedirect("pages/pageAdmin/admin.jsp");
								}
								else{
										// Redireccionamiento a la pagina de inicio de la web
										response.sendRedirect("pages/pageCuentaUser/cuentaUsuario.jsp");
								}
						}
						else
								// Redireccionamiento a la pagina de login, ya que algo estuvo mal
								response.sendRedirect("pages/pageLogin/login.jsp?caracteresInv=true");
						
				}else
						// Redireccionamiento a la pagina de login, ya que algo estuvo mal
						response.sendRedirect("pages/pageLogin/login.jsp?caracteresInv=true");
				
				
				
				
		}

		/**
		 * Returns a short description of the servlet.
		 *
		 * @return a String containing servlet description
		 */
		@Override
		public String getServletInfo() {
				return "Short description";
		}// </editor-fold>

}
