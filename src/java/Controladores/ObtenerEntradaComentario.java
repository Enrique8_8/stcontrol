package Controladores;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Comentario;
import modelo.EntradasForo;
import modelo.sql.peticionesSQL;

public class ObtenerEntradaComentario extends HttpServlet {

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				doPost(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				
				JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);
				int id = data.get("idEntrada").getAsInt();
				
				peticionesSQL peticion = new peticionesSQL();
				EntradasForo entrada = peticion.getEntrada(id);
				List<Comentario> comentarios = peticion.getComentarios(id);
				
				JsonObject jsonPrev  = new JsonObject();
				jsonPrev.addProperty("entrada", new Gson().toJson(entrada));
				jsonPrev.addProperty("comentario", new Gson().toJson(comentarios));
				
				String json = new Gson().toJson(jsonPrev);
				
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write(json);
				
		}

		/**
		 * Returns a short description of the servlet.
		 *
		 * @return a String containing servlet description
		 */
		@Override
		public String getServletInfo() {
				return "Short description";
		}// </editor-fold>

}
