package Controladores;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.sql.peticionesSQL;

@WebServlet(name = "EliminarReporteMiembro", urlPatterns = {"/EliminarReporteMiembro"})
public class EliminarReporteMiembro extends HttpServlet {

		protected void processRequest(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				response.setContentType("text/html;charset=UTF-8");
				try (PrintWriter out = response.getWriter()) {
						/*
						 * TODO output your page here. You may use following sample code.
						 */
						out.println("<!DOCTYPE html>");
						out.println("<html>");
						out.println("<head>");
						out.println("<title>Servlet EliminarReporteMiembro</title>");						
						out.println("</head>");
						out.println("<body>");
						out.println("<h1>Servlet EliminarReporteMiembro at " + request.getContextPath() + "</h1>");
						out.println("</body>");
						out.println("</html>");
				}
		}

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				doPost(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
				HttpSession session = request.getSession();
				String user = (String) session.getAttribute("username");
				String json;
				if(user != null){
						peticionesSQL peticion = new peticionesSQL();

						JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);
						int idReporte = data.get("id").getAsInt();
						
						if(peticion.existeReporte(idReporte, user)){
								int idEst = peticion.getEstacion(idReporte);
								peticion.borrarIncidencia(idReporte);
								peticion.disminuirEst(idEst);
								
								JsonObject jsonPrev = new JsonObject();
								jsonPrev.addProperty("eliminado", Boolean.TRUE);
								json = new Gson().toJson(jsonPrev);
								
						}else{
								JsonObject jsonPrev = new JsonObject();
								jsonPrev.addProperty("eliminado", Boolean.FALSE);
								json = new Gson().toJson(jsonPrev);
						}
				}else{
						JsonObject jsonPrev = new JsonObject();
						jsonPrev.addProperty("cuenta", "error");
						json = new Gson().toJson(jsonPrev);
				}
				
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write(json);
		}

		@Override
		public String getServletInfo() {
				return "Short description";
		}// </editor-fold>

}
