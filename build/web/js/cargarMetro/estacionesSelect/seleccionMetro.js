function cambioSelectLinea(){
    /* Esta funcion va a hacer que se modifiquen los valores del select de estaciones, dependiendo el valor que tenga el select de lineas */


    let select = document.querySelector('#linea');
    
    select.addEventListener('change', e=>{
        e.preventDefault();
        logica(select);
    });
}

function logica(select){
    let linea = select.options[select.selectedIndex].value;
    
    if(linea == '0'){
        document.querySelector('#estaciones').disabled = true;
    }else{
        insertarOpciones(linea);
    }
}

function insertarOpciones(linea){
    // Selecciona el select que va a contener a las estaciones
    let estaciones = document.querySelector('#estaciones');

    // Elimina todos sus hijos
    estaciones.innerHTML = ``;
    
    // Agrega la instruccion
    estaciones.appendChild(ui.crearElemento('option', 'Seleccione una estacion', ['value', '0'], ["disabled", "true"]));


    // Habilita la interaccion
    estaciones.disabled = false;

    // Recorre el array que tiene las estaciones, y carga las que corresponden a la linea seleccinada
    estacionesMetroCompleto.forEach(est=>{
        if(est[1] == linea){
            let opt = ui.crearElemento('option', est[0], ['value', est[0]])
            estaciones.appendChild(opt);
        }
    });
}