/* 
	Este script va a devolver un array con los nombres de las estaciones, asi como con la linea a la que pertenecen
 */
function cargarEstaciones(estacionesJSON, avanzado=false){
	/* Algo */
	let estaciones = [];

	// Recorre el json de los datos para obtener los valores del nombre de la estacion segun su linea
	estacionesJSON.features.forEach((data)=>{
		let est = [];
		let cadena = data.properties.stop_name;

		// Limpia el nombre de la estacion
		for(let letra of cadena){
			if(letra === '_'){
				cadena = recortar(cadena);
			}
		}
		est.push(cadena)
		// recortar(data.properties.stop_name);

		// Verifica a que linea del metro pertenece, y lo agrega
		switch(data.properties.trip_heads){
			case "Observatorio - Pantitlan": 
				est.push("Metro 1");
				break;
			case "Cuatro Caminos - Tasqueña":
				est.push("Metro 2");
				break;
			case "Indios Verdes - Universidad":
				est.push("Metro 3");
				break;
			case "Martin Carrera - Santa Anita":
				est.push("Metro 4");
				break;
			case "Pantitlan - Politecnico":
				est.push("Metro 5");
				break;
			case "Martin Carrera - El Rosario":
				est.push("Metro 6");
				break;
			case "Barranca del Muerto - El Rosario":
				est.push("Metro 7");
				break;
			case "Garibaldi - Constitucion de 1917":
				est.push("Metro 8");
				break;
			case "Tacubaya - Pantitlan":
				est.push("Metro 9");
				break;
			case "La Paz - Pantitlan":
				est.push("Metro A");
				break;
			case "Buenavista - Ciudad Azteca":
				est.push("Metro B");
				break;
			case "Tlahuac - Mixcoac":
				est.push("Metro 12");
				break;
		}

		if(avanzado){
			est.push(data.properties.geopoint);
		}

		// Se añade un array con la estacion, y la linea del metro que le corresponde
		estaciones.push(est);
	});

	return estaciones;
}

/* 
	Esta funcion va a limpiar el nombre de la estacion, quitando los guiones bajos
*/
function recortar(palabra){
	let pos = 0;
	let devolver = palabra;
	for(let letra of palabra){
		if(letra == '_'){
			devolver = (palabra.substring(0, pos));
			break;
		}
		pos++;
	}
	return devolver;
}

/* Esta funcion solo devuelve un array que contiene cada estacion de metro */
function soloEstaciones(estacionesJSON){
	let estaciones = [];
	estacionesJSON.features.forEach((data)=>{
		let cadena = data.properties.stop_name;

		// Limpia el nombre de la estacion
		for(let letra of cadena){
			if(letra === '_'){
				cadena = recortar(cadena);
			}
		}
		estaciones.push(cadena);
	});
	return estaciones;
}
