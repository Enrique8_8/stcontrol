async function cargarMapa(){
    let response = await  peticion('/STControl/ObtenerDatosEstadisticas');
    let dataPrev;
    if(response.status == 200){
        dataPrev  = await response.json();
    }
    
    let estIncPrev = filtradoEstaciones(dataPrev);
    let estGeoPrev = cargarEstaciones(estacionesJSON, true);

    let estGeoInc = filtradoEstacionesGeo(estGeoPrev, estIncPrev);
    
    let mapa = new Mapa(estGeoInc);
    mapa.cargaInicial();

    insertarEstaciones(estGeoPrev)
    eventosLinea();
    eventosNum();
    eventosEst();
    aplicarFiltros(mapa);
}

function insertarEstaciones(estaciones){
    let ui = new Interface();
    let contGral = ui.crearElemento('div', '', ['id', 'OpcionesEst']);

    let metro1 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro2 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro3 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro4 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro5 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro6 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro7 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro8 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro9 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metroA = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metroB = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro12 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']);

    metro1.appendChild( ui.crearElemento('li', 'Linea 1', ['class', 'nombreL']) )
    metro2.appendChild( ui.crearElemento('li', 'Linea 2', ['class', 'nombreL']) )
    metro3.appendChild( ui.crearElemento('li', 'Linea 3', ['class', 'nombreL']) )
    metro4.appendChild( ui.crearElemento('li', 'Linea 4', ['class', 'nombreL']) )
    metro5.appendChild( ui.crearElemento('li', 'Linea 5', ['class', 'nombreL']) )
    metro6.appendChild( ui.crearElemento('li', 'Linea 6', ['class', 'nombreL']) )
    metro7.appendChild( ui.crearElemento('li', 'Linea 7', ['class', 'nombreL']) )
    metro8.appendChild( ui.crearElemento('li', 'Linea 8', ['class', 'nombreL']) )
    metro9.appendChild( ui.crearElemento('li', 'Linea 9', ['class', 'nombreL']) )
    metroA.appendChild( ui.crearElemento('li', 'Linea A', ['class', 'nombreL']) )
    metroB.appendChild( ui.crearElemento('li', 'Linea B', ['class', 'nombreL']) )
    metro12.appendChild( ui.crearElemento('li', 'Linea 12', ['class', 'nombreL']) )

    estaciones.forEach(estacion => {

        let cnt = ui.crearElemento('li','', ['class', 'opc']);

        cnt.innerHTML = `
            <input type="checkbox" name="estacionesForm" id="est${estacion[0]}${estacion[1]}" value="${estacion[0]}">
            <label for="est${estacion[0]}${estacion[1]}" >${estacion[0]}</label>
        `;

        switch (estacion[1]) {
            case 'Metro 1':
                metro1.appendChild( cnt );
                break;
            case 'Metro 2':
                metro2.appendChild( cnt );
                break;
            case 'Metro 3':
                metro3.appendChild( cnt );
                break;
            case 'Metro 4':
                metro4.appendChild( cnt );
                break
            case 'Metro 5':
                metro5.appendChild( cnt );
                break;
            case 'Metro 6':
                metro6.appendChild( cnt );
                break;
            case 'Metro 7':
                metro7.appendChild( cnt );
                break
            case 'Metro 8':
                metro8.appendChild( cnt );
                break;
            case 'Metro 9':
                metro9.appendChild( cnt );
                break;
            case 'Metro A':
                metroA.appendChild( cnt );
                break
            case 'Metro B':
                metroB.appendChild( cnt );
                break;
            case 'Metro 12':
                metro12.appendChild( cnt );
                break;
        }
        
    });
    let estacionesL = [metro1, metro2, metro3, metro4, metro5, metro6, metro7, metro8, metro9, metroA, metroB, metro12];
    estacionesL.forEach(est => {
        contGral.appendChild(est);
    })

    document.querySelector('#agregarEstaciones').appendChild(contGral);
    
}