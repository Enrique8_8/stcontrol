function Mapa(estGeoInc){
    this.mapa = L.map('map').setView([19.419444, -99.140556], 13);
    this.estGeoInc = estGeoInc;
    
    this.tileLayerM = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
        subdomains: 'abcd',
        maxZoom: 18,
        minZoom: 12
    }).addTo(this.mapa);
    
    this.contLineas = L.geoJSON(lineasJSON, {
        style: features => {
            switch (features.properties.name) {
                case "L\u00ednea 1":
                    return {
                        color: "#F04E99",
                        opacity: "0.6",
                        stroke: "#F04E99",
                        weight: "8"
                    };
                    break;
                case "L\u00ednea 2":
                    return {
                        color: "#1874CD",
                        opacity: "0.6",
                        stroke: "#1874CD",
                        weight: "8"
                    };
                    break;
                case "L\u00ednea 3":
                    return {
                        color: "#AE9D27",
                        opacity: "0.6",
                        stroke: "#AE9D27",
                        weight: "8"
                    };
                    break;
                case "L\u00ednea 4":
                    return {
                        color: "#6FB6AE",
                        opacity: "0.6",
                        stroke: "#6FB6AE",
                        weight: "8"
                    };
                    break;
                case "L\u00ednea 5":
                    return {
                        color: "#FFD101",
                        opacity: "0.6",
                        stroke: "#FFD101",
                        weight: "8"
                    };
                    break;
                case "L\u00ednea 6":
                    return {
                        color: "#D41319",
                        opacity: "0.6",
                        stroke: "#D31319",
                        weight: "8"
                    };
                    break;
                case "L\u00ednea 7":
                    return {
                        color: "#E37222",
                        opacity: "0.6",
                        stroke: "#E37222",
                        weight: "8"
                    };
                    break;
                case "L\u00ednea 8":
                    return {
                        color: "#00A261",
                        opacity: "0.6",
                        stroke: "#00A261",
                        weight: "8"
                    };
                    break;
                case "L\u00ednea 9":
                    return {
                        color: "#5B2C2A",
                        opacity: "0.6",
                        stroke: "#5B2C2A",
                        weight: "8"
                    };
                    break;
                case "L\u00ednea A":
                    return {
                        color: "#9D549B",
                        opacity: "0.6",
                        stroke: "#9D549B",
                        weight: "8"
                    };
                    break;
                case "L\u00ednea B":
                    return {
                        color: "#00A261",
                        opacity: "0.6",
                        stroke: "#00A261",
                        weight: "8"
                    };
                    break;
                case "L\u00ednea 12":
                    return {
                        color: "#B89D4C",
                        opacity: "0.6",
                        stroke: "#B89D4C",
                        weight: "8"
                    };
                    break;
            }
        }
    }).addTo(this.mapa);

    this.contMarkers = L.layerGroup();
    console.log("1")
    /* agregarMarkers(dataPrev, estGeoPrev, contMarkers);
    contMarkers.addTo(mapa); */ 

    /* function limpiarMarkers(){
        contMarkers.clearLayers();
    } */
    this.icon0 = L.IconMaterial.icon({
        markerColor: 'rgba(0, 138, 198, .3)',
        outlineColor: 'rgb(0, 138, 198)',
        iconColor: '#ffffff',
        icon: 'trip_origin',
        outlineWidth: 2
    });
    this.icon1 = L.IconMaterial.icon({
        markerColor: 'rgba(50, 252, 50, 0.8)',
        outlineColor: 'rgb(0, 177, 0)',
        iconColor: '#ffffff',
        icon: 'trip_origin',
        outlineWidth: 2
    });
    this.icon2 = L.IconMaterial.icon({
        markerColor: 'rgba(122, 247, 106, .5)',
        outlineColor: 'rgb(0, 177, 0)',
        iconColor: '#ffffff',
        icon: 'trip_origin',
        outlineWidth: 2
    });
    this.icon3 = L.IconMaterial.icon({
        markerColor: 'rgba(251, 255, 0, 0.5)',
        outlineColor: 'rgb(251, 255, 0)',
        iconColor: '#ffffff',
        icon: 'trip_origin',
        outlineWidth: 2
    });
    this.icon4 = L.IconMaterial.icon({
        markerColor: 'rgba(255, 158, 0, 0.5)',
        outlineColor: 'rgb(255, 158, 0)',
        iconColor: '#ffffff',
        icon: 'trip_origin',
        outlineWidth: 2
    });
    this.icon5 = L.IconMaterial.icon({
        markerColor: 'rgba(255, 54, 0, 0.5)',
        outlineColor: 'rgb(255, 54, 0)',
        iconColor: '#ffffff',
        icon: 'trip_origin',
        outlineWidth: 2
    });
    this.icon6 = L.IconMaterial.icon({
        markerColor: 'rgba(0, 0, 0, 0.5)',
        outlineColor: 'rgb(0, 0, 0)',
        iconColor: '#ffffff',
        icon: 'trip_origin',
        outlineWidth: 2
    });
}

// Metodo que verifica que n este entre un cierto rango
Mapa.prototype.validaRango = function(n, min, max){
    return n > min && n <= max;
}
Mapa.prototype.validaRango.r0 = function(n){
    return n == 0;
}
Mapa.prototype.validaRango.r1 = function(n){
    return Mapa.prototype.validaRango(n, 0, 10);
}
Mapa.prototype.validaRango.r2 = function(n){
    return Mapa.prototype.validaRango(n,10, 25);
}
Mapa.prototype.validaRango.r3 = function(n){
    return Mapa.prototype.validaRango(n, 25, 50);
}
Mapa.prototype.validaRango.r4 = function(n){
    return Mapa.prototype.validaRango(n, 50, 100);
}
Mapa.prototype.validaRango.r5 = function(n){
    return Mapa.prototype.validaRango(n, 100, 200);
}
Mapa.prototype.validaRango.r6 = function(n){
    return n > 200;
}


// Metodo para agregar markers a un contenedor
Mapa.prototype.agregarMarkers = function(data){
    // Se recorre el array que contiene todos los datos, para ir creando los markers necesarios
    for( let est of data ){
        let inc = est.incidencias;
        // Creacion de los marker
        let marker;

        // Se agrega el icono correspondiente, dependiendo el numero de reportes de la estacion
        if(inc == 0 )
            marker = L.marker( est.coords, {icon: this.icon0});
        else if(this.validaRango.r1(inc))
            marker = L.marker( est.coords, {icon: this.icon1});
        else if(this.validaRango.r2(inc))
            marker = L.marker( est.coords, {icon: this.icon2});
        else if(this.validaRango.r3(inc))
            marker = L.marker( est.coords, {icon: this.icon3});
        else if(this.validaRango.r4(inc))
            marker = L.marker( est.coords, {icon: this.icon4});
        else if(this.validaRango.r5(inc))
            marker = L.marker( est.coords, {icon: this.icon5});
        else if(inc > 200)
            marker = L.marker( est.coords, {icon: this.icon6});
        else
            console.log("Problema " + est.nombre)
        
        // Se le agrega el popup al marker con el nombre de la estacion
        marker.bindPopup( est.nombre );

        // Se agrega el marker al contenedor de los marker
        this.contMarkers.addLayer(marker);
    }

    // Se agrega el contenedor de los marker al mapa
    this.contMarkers.addTo(this.mapa);

}

// Metodo para limpiar todos los markers del mapa
Mapa.prototype.limpiarMarkers = function(){
    this.contMarkers.clearLayers();
    console.log("Borrando")
}

Mapa.prototype.cargaInicial = function(){
    this.agregarMarkers(this.estGeoInc);
}

// Metodo para aplicar filtros a los markers que aparecen
Mapa.prototype.filtrarMarkers = function(lineas, estaciones, incidencias){
    let estMark = [];
    let incValid = [];
    for(let inc of incidencias){
        switch (inc) {
            case 0:
                incValid.push(this.validaRango.r0);
                break;
            case 1:
                incValid.push(this.validaRango.r1);
                break;
            case 2:
                incValid.push(this.validaRango.r2);
                break;
            case 3:
                incValid.push(this.validaRango.r3);
                break;
            case 4:
                incValid.push(this.validaRango.r4);
                break;
            case 5:
                incValid.push(this.validaRango.r5);
                break;
            case 6:
                incValid.push(this.validaRango.r6);
                break;
        }
    }
    console.log(incValid)

    for(let est of this.estGeoInc){
        let valido = false;

        // Verifica si una linea forma parte de una estacion seleccionada
        for(linea of est.linea){
            for(linea2 of lineas){
                if(linea == linea2){
                    estMark.push(est);
                    valido = true;
                    break;
                }
            }
            if(valido)
                break;
        }
        if(valido)
            continue;

        if(estaciones.includes(est.nombre)){
            console.log(est.nombre)
            estMark.push(est);
            continue;
        }

        for(let inc of incValid){
            if(inc(est.incidencias)){
                estMark.push(est);
                break;
            }
        }
    }
    console.log(estMark)
    this.limpiarMarkers();
    this.agregarMarkers(estMark)
}