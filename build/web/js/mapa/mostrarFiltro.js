function eventosLinea(){
    let btnLinea = document.querySelector('#btnMostrarLinea');

    let cntLinea = document.querySelector('#OpcionesLineas');
    let flecha1L = document.querySelector("#expandirLinea");
    let flecha2L = document.querySelector("#contraerLinea");
    let posL = 'abajo';

    btnLinea.addEventListener('click', e=>{
        e.preventDefault();

        posL = desplegar(posL, flecha1L, flecha2L, cntLinea);

    } );


}

function eventosNum(){
    let btnNum = document.querySelector('#btnMostrarNum');

    let cntNum = document.querySelector('#OpcionesNum');
    let flecha1N = document.querySelector('#expandirNum');
    let flecha2N = document.querySelector('#contraerNum');
    let posN = 'abajo';

    btnNum.addEventListener('click', e=>{
        e.preventDefault();

        posN = desplegar(posN, flecha1N, flecha2N, cntNum);
    })

}

function eventosEst(){
    let btnEst = document.querySelector('#btnMostrarEst');
    
    let cntEst = document.querySelector('#OpcionesEst');
    let flecha1E = document.querySelector('#expandirEst');
    let flecha2E = document.querySelector('#contraerEst');
    let posE = 'abajo'

    console.log("2")
    
    btnEst.addEventListener('click', e=>{
        e.preventDefault();
        
        posE = desplegar(posE, flecha1E, flecha2E, cntEst);
    })
    
    
}

// Esta funcion va a hacer que un contenido de la zona de filtros pueda ser desplegado u ocultado
function desplegar(pos, flecha1, flecha2, cnt){
    if(pos == 'abajo'){
        cnt.style.display = 'flex';
        flecha1.style.display = 'none';
        flecha2.style.display = 'block';
        pos = 'arriba';
    }else if(pos == 'arriba'){
        cnt.style.display = 'none';
        flecha1.style.display = 'block';
        flecha2.style.display = 'none';
        pos = 'abajo';
    }
    return pos;
}