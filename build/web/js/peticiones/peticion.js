async function peticion(url,  ...parametros){
    let data = {};
    
    for(let param of parametros){
        data[param[0]] = param[1];
    }
    
    let response = await fetch(url, {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }, 
        body: JSON.stringify(data)
    });
    return response;
}