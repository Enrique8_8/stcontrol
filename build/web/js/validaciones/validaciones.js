function comprobarValor(valor, length){
    let cadenaBasica = ['<', '>', '=', '-' ,"'", '\\'];
    let valida = true;

    if(valor != "" && valor != null){
        if(valor.length <= length){
            for(let caracter of cadenaBasica){
                if(valor.includes(caracter)){
                    valida = false;
                    break;
                }
            }
        }
        else{
            valida = false;
        }
    }else{
        valida = false;
    }
    return valida;
}