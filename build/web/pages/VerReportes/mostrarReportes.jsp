<!DOCTYPE html>
<html lang="en">

<head>
    <title>Ver Reportes</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Estilos generales-->
    <t:estilosGenerales></t:estilosGenerales>

    <!-- Estilos Propios-->
    <link rel="stylesheet" href="css/stylesMostrar.css">
    <link rel="stylesheet" href="css/stylesFiltros.css">

</head>

<body>
    <t:Header></t:Header>
    
    <div id="contenedorPrincipal">
        <main>
            <h1 class="titulo">Reportes Previos</h1>
            <section id="filtros">
                <!-- Los filtros que se van a aplicar, son: 
                    por linea del metro
                    por estacion del metro
                    por fecha

                -->
                <form action="">
                    <div id="filtrosLineas" class="contFiltro">
                        <h2 class="tituloFiltro">Selecciona las lineas del metro que quieras ver</h2>
                        <div id="contFiltrosLineas">
                            <div class="checkLinea">
                                <label for="Metro1">Linea 1</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro1" value="Metro1">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro2">Linea 2</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro2" value="Metro2">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro3">Linea 3</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro3" value="Metro3">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro4">Linea 4</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro4" value="Metro4">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro5">Linea 5</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro5" value="Metro5">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro6">Linea 6</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro6" value="Metro6">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro7">Linea 7</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro7" value="Metro7">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro8">Linea 8</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro8" value="Metro8">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro9">Linea 9</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro9" value="Metro9">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="MetroA">Linea A</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="MetroA" value="MetroA">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="MetroB">Linea B</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="MetroB" value="MetroB">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro12">Linea 12</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro12" value="Metro12">
                            </div><!-- .checkLinea -->
                        </div><!-- #contLineasFiltros -->

                    </div><!-- #filtrosLineas -->

                    <div id="filtroEstaciones" class="contFiltro">
                        <h2 class="tituloFiltro">Selecciona las estaciones que quieras ver</h2>
                        <div id="contenedorEstaciones"></div>
                    </div><!-- #filtrosEstaciones -->

                    <div id="fechas" class="contFiltro">
                        <h2 class="tituloFiltro">Filtrar por fecha</h2>
                        <div id="fecha1" class="centrar">
                            <label for="fechaInicio">De: </label>
                            <input type="date" name="fechaInicio" id="fechaInicio">
                        </div><!-- fecha1 -->

                        <div id="fecha2" class="centrar">
                            <label for="#fechaFinal">a: </label>
                            <input type="date" name="fechaFinal" id="fechaFinal">
                        </div><!-- #fecha2 -->
                    </div><!-- #fechas -->

                    <div id="botonesFiltros" class="contFiltro centrar">
                        <button type="submit">Aplicar filtros</button>
                        <button type="reset">Limpiar Filtros</button>
                    </div>
                </form>
            </section>
            <section id="contenedorResultados">
                <div class="registro" id="idRegistro">
                    <div class="datosLinea">
                        <div class="encabezados">
                            <h2 class="datoLinea titulo">Linea del metro</h2>
                            <h2 class="datoEst titulo">Nombre de la estacion</h2>
                        </div>
                        <div class="barra"></div>
                    </div><!-- .datosLinea -->

                    <div class="cuerpo">
                        <div class="textCentral">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum ad nisi minima quidem,
                                culpa fugiat labore? Id officiis magni aut nostrum optio ducimus adipisci nobis ut,
                                earum, atque, qui perspiciatis!Illum rerum odit neque culpa, eos tenetur saepe et ad
                                fuga libero minima quos, sunt, reprehenderit corporis aliquid. </p>
                        </div><!-- .textCentral -->

                        <div class="textDerecha">
                            <p>Tipo de incidencial</p>
                            <p>Dia: 00-00-00</p>
                            <p>Hora: 00:00</p>
                        </div><!-- .textDerecha -->

                        <div class="barraInferior centrar">
                            <div class="seguir centrar">
                                <button class="btnSeguir">
                                    <span class="material-icons btnLlena">
                                        star
                                    </span>
                                    <span class="material-icons btnVacia">
                                        star_border
                                    </span>
                                </button>
                                <p class="txtSeguir">Seguir</p>
                            </div><!-- .seguir -->

                            <div class="comentarios centrar">
                                <span class="material-icons">
                                    comment
                                </span>
                                <p>Comentarios</p>
                                <button class="btnComentarios">
                                    <span class="material-icons showComment">
                                        keyboard_arrow_down
                                    </span>
                                    <span class="material-icons hideComment">
                                        keyboard_arrow_up
                                    </span>
                                </button>
                            </div><!-- .comentarios -->

                            <div class="reportar centrar">
                                <button class="btnReportar">
                                    <span class="material-icons">
                                        warning
                                    </span>
                                </button>
                                <p>Reportar</p>
                            </div><!-- .reportar -->

                        </div><!-- .barraInferior -->

                    </div><!-- .cuerpo -->

                    <div class="lateral">
                        <div class="circulo"></div>
                        <div class="circulo"></div>
                        <div class="circulo"></div>
                        <div class="circulo"></div>
                        <div class="circulo"></div>
                    </div>



                </div>

            </section>
        </main>
    </div>

    <t:Footer></t:Footer>
</body>

</html>