class Interface{
    constructor(){ }

    crearElemento(tipo, clases, id){
        let element = document.createElement(tipo);
        
        clases.forEach((classN)=>{
            element.classList.add(classN);
        });
    
        element.id = id;
    
        return element;
    }
}