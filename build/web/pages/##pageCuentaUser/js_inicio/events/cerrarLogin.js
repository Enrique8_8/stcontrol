// Funcion que agrega evento al icono de cerrar ventana, de la ventana del login
function cerrarContLogin(){
    console.log('Entra para cerrar')
    // Selecciona el elemento cerrar
    btnCerrar = document.querySelector('#cerrar');

    // Agrega el evento
    btnCerrar.addEventListener('click', e=>{
        // Elimina el elemento
        document.querySelector('#contenedor_sesion').remove();

        // Modifica la variable global abierto, haciendo que despues se pueda permitir volver a abrir
        // la ventana de login
        abierto = false;
        alertaExt = false;
    });
}