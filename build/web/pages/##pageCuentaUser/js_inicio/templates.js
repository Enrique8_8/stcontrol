let minLength = 5;
let maxLength = 20;

// Template de crear cuenta
var crearCuenta = `
<div class="medio">
    <img src="assets/Guy.png">
    <h2>Crea tu cuenta de STControl</h2>
</div>

<form class="login" name="crearCuenta" id="crearCuenta">

    <div class="text-control">
        <h1>Ingresa los siguientes datos:</h1>
    </div>

    <div class="data-entry"> 
        <div class="campoLogin" id="data1">
            <label for="username">Nombre de usuario:</label>
            <input type="text" name="username" id="username" required minlength=${minLength} maxlength=${maxLength} placeholder="Nombre de usuario"/>
        </div>
            
        <div class="campoLogin" id="data2">
            <label for="email">Correo electronico:</label>
            <input type="email" name="email" id="email" required placeholder="ejemplo@correo.com"/>
        </div>

        <div class="campoLogin" id="data3">
            <label for="user-password">Contraseña:</label>
            <input type="password" name="user-password" id="user-password" required minlength=${minLength} maxlength=${maxLength} placeholder="Contraseña"/>
        </div>

        <div class="campoLogin" id="data4">
            <label for="vpassword">Confirmacion de contraseña:</label>
            <input type="password" name="vpassword" id="vpassword" required minlength=${minLength} maxlength=${maxLength} placeholder="Contraseña">
        </div>
        <div id="alertaError"></div>
        <button type="submit" form="crearCuenta" value="Ingresar" class="btnSubmit">Ingresar</button>
    </div> 

</form>

`;

// Template de la barra superior
let superior = `

<div id="superiorLogin">

    <ul>
        <li id="crear"><a href="#">Crear Cuenta</a></li>
        <li id="iniciar"><a href="#">Iniciar Sesión</a></li>
    </ul>
    <i class="material-icons" id="cerrar">
        close
    </i>
</div>

`;

// Template de iniciar sesion
var iniciarSesion = `
<div class="medio">
    <img src="assets/Guy.png">
    <h2>Inicia sesión con tu cuenta de STControl</h2>
</div>

<form class="login" name="iniciarSesion" id="iniciarSesion">

    <div class="text-control">
        <h1> Ingresa los siguientes datos: </h1>
    </div>
        
    <div class="data-entry"> 
        <div class="campoInicio">
            <label for="username">Nombre de usuario:</label>
            <input type="text" name="username" id="username" required minlength=${minLength} maxlength=${maxLength} placeholder="Nombre de usuario">
        </div>

        <div class="campoInicio">
            <label for="user-password">Contraseña:</label>
            <input type="password" name="user-password" id="user-password" required minlength=${minLength} maxlength=${maxLength} placeholder="Contraseña">
        </div>

        <button type="submit" form="iniciarSesion" value="Ingresar" class="btnSubmit">Ingresar</button>
    </div> 
</form>
`;

