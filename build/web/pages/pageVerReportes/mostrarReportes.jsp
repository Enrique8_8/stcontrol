<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE html>
<html lang="es">

<head>
    <title>Ver Reportes</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Estilos generales-->
    <t:estilosGenerales></t:estilosGenerales>

    <!-- Estilos Propios-->
    <link rel="stylesheet" href="css/stylesMostrar.css">
    <link rel="stylesheet" href="css/stylesFiltros.css">
    <link href="../../css/stylesLineas.css" rel="stylesheet" type="text/css"/>
    <script src="../../interfaz/Interface.js" type="text/javascript"></script>
</head>

<body>
    <t:Header username="${sessionScope.username}"></t:Header>

    <div id="contenedorPrincipal">
        <main>
            <h1 class="titulo">Reportes Previos</h1>
            <div id="contBtnDesplegar" class="centrado">
                <button id="btnDesplegado" value="mostrar">Ver Filtros</button>
            </div>
            <section id="filtros">

                <form id="formFiltros">
                    <div id="filtrosLineas" class="contFiltro">
                        <h2 class="tituloFiltro">Selecciona las lineas del metro que quieras ver</h2>
                        <div id="contFiltrosLineas">
                            <div class="checkLinea">
                                <label for="Metro1">Linea 1</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro1" value="Metro 1">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro2">Linea 2</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro2" value="Metro 2">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro3">Linea 3</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro3" value="Metro 3">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro4">Linea 4</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro4" value="Metro 4">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro5">Linea 5</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro5" value="Metro 5">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro6">Linea 6</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro6" value="Metro 6">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro7">Linea 7</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro7" value="Metro 7">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro8">Linea 8</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro8" value="Metro 8">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro9">Linea 9</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro9" value="Metro 9">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="MetroA">Linea A</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="MetroA" value="Metro A">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="MetroB">Linea B</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="MetroB" value="Metro B">
                            </div><!-- .checkLinea -->

                            <div class="checkLinea">
                                <label for="Metro12">Linea 12</label>
                                <input type="checkbox" name="LineaMetro" class="checkBox" id="Metro12" value="Metro 12">
                            </div><!-- .checkLinea -->
                        </div><!-- #contLineasFiltros -->

                    </div><!-- #filtrosLineas -->

                    <div id="filtroEstaciones" class="contFiltro">
                        <h2 class="tituloFiltro">Selecciona las estaciones que quieras ver</h2>
                        <div id="contenedorEstaciones"></div>
                    </div><!-- #filtrosEstaciones -->

                    <div id="fechas" class="contFiltro">
                        <h2 class="tituloFiltro">Filtrar por fecha</h2>
                        <div id="fecha1" class="centrar">
                            <label for="fechaInicio">De : </label>
                            <input type="date" name="fechaInicio" id="fechaInicio">
                        </div><!-- fecha1 -->

                        <div id="fecha2" class="centrar">
                            <label for="#fechaFinal">a : </label>
                            <input type="date" name="fechaFinal" id="fechaFinal">
                        </div><!-- #fecha2 -->
                    </div><!-- #fechas -->

                    <div id="botonesFiltros" class="contFiltro centrar">
                        <button type="submit" id="btnAplicarF">Aplicar filtros</button>
                        <button type="reset">Limpiar Filtros</button>
                    </div>
                </form>
            </section>
            <section id="contenedorResultados"></section>
        </main>
    </div>

    <t:Footer></t:Footer>
    <script src="../../js/peticiones/peticion.js" type="text/javascript"></script>
    <script src="js/template.js" type="text/javascript"></script>
    <script src="../../data/estaciones_geoJSON.js"></script>
    <script src="../../js/cargarMetro/cargarEstaciones.js"></script>
    <script src="js/insertarEstF.js"></script>
    <script src="js/events/desplegadoFiltros.js"></script>

    <script src="js/events/aplicarFiltros.js"></script>

    <script src="js/events/cargaInicial.js" type="text/javascript"></script>
</body>

</html>