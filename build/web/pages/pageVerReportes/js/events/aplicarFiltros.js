function aplicarFiltros(){
    let btnFiltros = document.querySelector('#btnAplicarF');

    btnFiltros.addEventListener('click', async e => {
        e.preventDefault();

        let lineasCheck =  document.getElementsByName('LineaMetro'),
            lineas = [], estaciones = [], fechas = [];
            
        console.log("Las lineas son: ", lineasCheck)
        // Obtencion de las lineas con check
        for(let check of lineasCheck){
            if(check.checked == true){
                lineas.push(check.value.substring(6))
            }
        }
    
        // Obtencion de las estaciones con check
        for(let i of [1,2,3,4,5,6,7,8,9,'A', 'B', 12]){
            let estacionesCheckL = document.getElementsByName('estMetro ' + i);
            for(let j of estacionesCheckL){
                if(j.checked == true){
                    estaciones.push( { 'estacion':  j.value, 'linea':  j.name.substring(9) } );
                }
            }
        }
    
        // Obtencion de las fechas
        let fechaInicio = document.querySelector('#fechaInicio').value;
        let fechaFinal = document.querySelector('#fechaFinal').value;
    
        fechas = [fechaInicio, fechaFinal];
        fechas = fechas.filter(value => value != '');

        // Aqui se van a aplicar las validaciones
        
        // Aqui se hara la peticion fetch, y la funcion devolvera la respuesta
        peticion('/STControl/ObtenerReportesF', ['lineas', lineas], ['estaciones', estaciones], ['fechas', fechas])
                .then( response => {
                    if(response.status == 200){
                        document.querySelector('#contenedorResultados').innerHTML = '';
                        console.log("Contenido borrado")
                        insertarContenido(response);
                    }
                }).catch(()=>{console.log("uis qu paso")});

    });

}
