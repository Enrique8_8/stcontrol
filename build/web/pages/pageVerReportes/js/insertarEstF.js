function insertarEstF(){
    let metro = cargarEstaciones(estacionesJSON);
    let ui = new Interface();

    let metro1 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro2 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro3 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro4 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro5 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro6 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro7 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro8 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro9 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metroA = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metroB = ui.crearElemento('ul', '', ['class', 'opcSelectEst']),
    metro12 = ui.crearElemento('ul', '', ['class', 'opcSelectEst']);

    metro1.appendChild( ui.crearElemento('li', 'Linea 1', ['class', 'nombreL']) )
    metro2.appendChild( ui.crearElemento('li', 'Linea 2', ['class', 'nombreL']) )
    metro3.appendChild( ui.crearElemento('li', 'Linea 3', ['class', 'nombreL']) )
    metro4.appendChild( ui.crearElemento('li', 'Linea 4', ['class', 'nombreL']) )
    metro5.appendChild( ui.crearElemento('li', 'Linea 5', ['class', 'nombreL']) )
    metro6.appendChild( ui.crearElemento('li', 'Linea 6', ['class', 'nombreL']) )
    metro7.appendChild( ui.crearElemento('li', 'Linea 7', ['class', 'nombreL']) )
    metro8.appendChild( ui.crearElemento('li', 'Linea 8', ['class', 'nombreL']) )
    metro9.appendChild( ui.crearElemento('li', 'Linea 9', ['class', 'nombreL']) )
    metroA.appendChild( ui.crearElemento('li', 'Linea A', ['class', 'nombreL']) )
    metroB.appendChild( ui.crearElemento('li', 'Linea B', ['class', 'nombreL']) )
    metro12.appendChild( ui.crearElemento('li', 'Linea 12', ['class', 'nombreL']) )

    
    for(let estacion of metro){
        switch (estacion[1]) {
            case 'Metro 1':
                metro1.appendChild( generarOpc(estacion[0], estacion[1], ui) );
                break;
            case 'Metro 2':
                metro2.appendChild( generarOpc(estacion[0], estacion[1], ui) );
                break;
            case 'Metro 3':
                metro3.appendChild( generarOpc(estacion[0], estacion[1], ui) );
                break;
            case 'Metro 4':
                metro4.appendChild( generarOpc(estacion[0], estacion[1], ui) );
                break
            case 'Metro 5':
                metro5.appendChild( generarOpc(estacion[0], estacion[1], ui) );
                break;
            case 'Metro 6':
                metro6.appendChild( generarOpc(estacion[0], estacion[1], ui) );
                break;
            case 'Metro 7':
                metro7.appendChild( generarOpc(estacion[0], estacion[1], ui) );
                break
            case 'Metro 8':
                metro8.appendChild( generarOpc(estacion[0], estacion[1], ui) );
                break;
            case 'Metro 9':
                metro9.appendChild( generarOpc(estacion[0], estacion[1], ui) );
                break;
            case 'Metro A':
                metroA.appendChild( generarOpc(estacion[0], estacion[1], ui) );
                break
            case 'Metro B':
                metroB.appendChild( generarOpc(estacion[0], estacion[1], ui) );
                break;
            case 'Metro 12':
                metro12.appendChild( generarOpc(estacion[0], estacion[1], ui) );
                break;
        }
    }

    let estacionesL = [metro1, metro2, metro3, metro4, metro5, metro6, metro7, metro8, metro9, metroA, metroB, metro12];

    let contenedor = document.querySelector('#contenedorEstaciones');
    for(let est of estacionesL){
        contenedor.appendChild(est);
    }

    function generarOpc(est, linea, ui){
        let cont = ui.crearElemento('li', '', ['class', 'nombreEst']);

        cont.innerHTML = `
            <input type="checkbox" name="est${linea}" value="${est}" id="${est}M${linea}">
            <label for="${est}M${linea}">${est}</label>`;
    
        return cont;
    }
}

