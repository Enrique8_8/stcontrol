function templateReporte(id, linea, estacion, comentario, dia, hora, tipo){
    let ui = new Interface();
    let contenedor = ui.crearElemento('div', '', ['class', 'registro'], ['id', id]);
    contenedor.classList.add('linea' + linea);
    contenedor.innerHTML = `
                    <div class="datosLinea">
                        <div class="encabezados">
                            <h2 class="datoLinea titulo">Metro Linea ${linea}</h2>
                            <h2 class="datoEst titulo">${estacion}</h2>
                        </div>
                        <div class="barra"></div>
                    </div><!-- .datosLinea -->

                    <div class="cuerpo">
                        <div class="textCentral">
                            <p>${comentario}</p>
                        </div><!-- .textCentral -->

                        <div class="textDerecha">
                            <p>${tipo}</p>
                            <p>Dia: ${dia}</p>
                            <p>Hora: ${hora}</p>
                        </div><!-- .textDerecha -->

                    </div><!-- .cuerpo -->

                    <div class="lateral">
                        <div class="circulo"></div>
                        <div class="circulo"></div>
                        <div class="circulo"></div>
                    </div>`;    
    return contenedor;
}