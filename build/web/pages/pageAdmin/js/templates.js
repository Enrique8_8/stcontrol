function templateMiembro(nombre, correo, linea){
    let ui = new Interface();

    let contenedor = ui.crearElemento('div', '', ['class', 'userData'], ['id', nombre]);
    contenedor.innerHTML = `
    <p>${nombre}</p>
    <p>${correo}</p>
    <p>${linea}</p>
    <button class="btnEliminar btnEliminarMiembro">
        <span class="material-icons">
            delete_outline
        </span>
        <p>Eliminar usuario</p>
    </button>`;

    return contenedor;
}

function templateReporte(id, linea, estacion, comentario, tipo, fecha, hora, user){
    let ui = new Interface();

    let contenedor = ui.crearElemento('div', '', ['class', 'registro'], ['id', `rep${id}`], ['class', `linea${linea}`]);
    contenedor.innerHTML = `
    <div class="datosLinea">
        <div class="encabezados">
            <h2 class="datoLinea titulo">Linea ${linea}</h2>
            <h2 class="datoEst titulo">${estacion}</h2>
        </div>
        <div class="barra"></div>
    </div><!-- .datosLinea -->

    <div class="cuerpo">
        <div class="textCentral">
            <p>${comentario}</p>
        </div><!-- .textCentral -->

        <div class="textDerecha">
            <p>${tipo}</p>
            <p>Dia: ${fecha}</p>
            <p>Hora: ${hora}</p>
        </div><!-- .textDerecha -->

        <div class="userReporte">
            <p>Usuario: <span>${user}</span></p>
        </div>

        <div class="contBtnCuerpo centrar">
            <button class="btnEliminar btnEliminarReporte">
                <span class="material-icons">
                    delete_outline
                </span>
                <p>Eliminar registro</p>
            </button>
        </div><!-- .contBtnEliminar -->

    </div><!-- .cuerpo -->
    `;

    return contenedor;
}

function templateEntrada(id, preg, contenido, user){
    let ui = new Interface();
    let contenedor = ui.crearElemento('div', '', ['class', 'entrada'], ['id', `ent${id}`]);

    contenedor.innerHTML = `
    <p class="nick">${user}</p>
    <p class="pregunta">${preg}</p>
    <p class="contenido">${contenido}</p>

    <div class="contBtnCuerpo centrar">
        <button class="btnComent">
            <span class="material-icons">
                comment
            </span>
            Comentarios
            <span class="material-icons btnBajar">
                keyboard_arrow_down
            </span>
            <span class="material-icons btnSubir">
                keyboard_arrow_up
            </span>
        </button>
        <button class="btnEliminar btnEliminarEntrada">
            <span class="material-icons">
                delete_outline
            </span>
            <p>Eliminar entrada </p>
        </button>
    </div><!-- .contBtnEliminar -->

    <div class="contComentarios"></div>
    
    `;
    return contenedor;
}

function templateComentario(id, contenido, user){
    let ui = new Interface();
    let contenedor = ui.crearElemento('div', '', ['class', 'comentarioE'], ['id', `coment${id}`]);
    contenedor.innerHTML = `
    <p class="userComentario">${user}</p>
    <p class="comentComentario">${contenido}</p>
    <button class="btnEliminar btnEliminarComent">
        Eliminar
    </button>
    `;

    return contenedor;
}
