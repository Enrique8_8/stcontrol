function eventosCambioT(){
    console.log("1")
    // Botones de la pagina
    let contenedor = document.querySelector('main'),
        btnUsuarios = document.querySelector('#usuarios'),
        btnReportes = document.querySelector('#reportesR'),
        btnEntradas = document.querySelector('#entradasR'),
        btnCrearCuenta = document.querySelector('#zonaCrearCuenta');

    // Secciones varias
    let seccionP = document.querySelector('#zonaPrincipal'),
        seccionU = document.querySelector('#zonaUsuarios'),
        seccionR = document.querySelector('#zonaReportesBorrar'),
        seccionE = document.querySelector('#zonaEntradasF'),
        seccionC = document.querySelector('#crearCuenta');
    
    let textoDesc = document.querySelector('#nombreCoso');


    btnUsuarios.addEventListener('click', async e => {
        e.preventDefault();
        if(seccionU.style.display == 'none' || seccionU.style.display == ''){
            desaparecer(seccionU, [seccionP, seccionR, seccionE]);
            seccionC.style.display = 'none';
            textoDesc.textContent = 'de Miembros';

            let response = await peticion('/STControl/ObtenerMiembrosAdmin');
            if(response.status == 200){
                let data = await response.json();
                if(data['errorAdmin'] == true){
                    window.location.replace( "../pageLogin/login.jsp?redireccionado=true" );
                }else{
                    insertarMiembros(seccionU, data);
                    borradoMiembro();
                }

            }else
                window.location.reload();
        }
    })

    btnReportes.addEventListener('click', async e => {
        e.preventDefault();
        if(seccionR.style.display == 'none' || seccionR.style.display == ''){
            desaparecer(seccionR, [seccionP, seccionU, seccionE]);
            seccionC.style.display = 'none';
            textoDesc.textContent = 'de Reportes';

            let response = await peticion('/STControl/ObtenerReportes');

            if(response.status == 200){
                let data = await response.json();

                insertarReporte(seccionR, data);
                borradoReporte();

            }else{
                window.location.reload();
            }
        }
    })

    btnEntradas.addEventListener('click', async e => {
        e.preventDefault();
        if(seccionE.style.display == 'none' || seccionE.style.display == ''){
            desaparecer(seccionE, [seccionP, seccionU, seccionR]);
            seccionC.style.display = 'none';
            textoDesc.textContent = 'de Entradas';

            let response = await peticion('/STControl/ObtenerEntradas');

            if(response.status == 200){
                let data = await response.json();

                insertarEntradas(seccionE, data);
                borradoEntrada();
                eventosEntradas();

            }else
                window.location.reload();
        }
    })

    btnCrearCuenta.addEventListener('click', e => {
        e.preventDefault();
        desaparecer(seccionC, [seccionP, seccionU, seccionR, seccionE])
        textoDesc.textContent = '';
    })
}

function desaparecer(muestra, desaparece){
    muestra.style.display = 'block';
    for(let d of desaparece){
        d.style.display = 'none';
        d.innerHTML = '';
    }
}