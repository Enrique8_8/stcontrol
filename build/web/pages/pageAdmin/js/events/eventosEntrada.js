function eventosEntradas(){
    let btnComentarios = document.querySelectorAll('.btnComent');
    for(let btn of btnComentarios){
        btn.addEventListener('click', async e => {
            e.preventDefault();
            let idPrev = btn.parentElement.parentElement.id;

            let btnSubir = document.querySelector(`#${idPrev} .btnSubir`),
                btnBajar = document.querySelector(`#${idPrev} .btnBajar`),
                contComent = document.querySelector(`#${idPrev} .contComentarios`);

            if(btnSubir.style.display == 'none' || btnSubir.style.display == ''){
                btnBajar.style.display = 'none';
                btnSubir.style.display = 'block';
                let id = idPrev.substring(3);

                let response = await peticion('/STControl/ObtenerComentarios', ['idEntrada', id]);
                if(response.status == 200){
                    let data = await response.json();
                    for(let c of data){
                        let comentario = templateComentario(c['id_c'], c['desc'], c['nombre_m']);
                        contComent.appendChild(comentario);
                    }
                    eventoComentarios();
                }
            }else{
                btnBajar.style.display = 'block';
                btnSubir.style.display = 'none';
                contComent.innerHTML = '';
            }

        })
    }
}
function eventoComentarios(){
    let btnComentsBorrar = document.querySelectorAll('.btnEliminarComent');
    for(let btn of btnComentsBorrar){
        btn.addEventListener('click', async e => {
            e.preventDefault();

            let idPrev = btn.parentElement.id;
            let id = idPrev.substring(6);

            let response = await peticion('/STControl/AdminEliminarComentario', ['idComentario', id]);
            if(response.status == 200){
                let data = await response.json();
                if(data['eliminado'] == true){
                    btn.parentElement.remove();
                }else{
                    window.location.replace( "../pageLogin/login.jsp?redireccionado=true" );
                }
            }else{
                alert("No se pudo eliminar el comentario, por favor reintentalo");
            }

        })
    }
}