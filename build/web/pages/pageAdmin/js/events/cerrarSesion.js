function cerrarSesion(){
    let btn = document.querySelector('#btnCerrarSesion');

    btn.addEventListener('click', e => {
        e.preventDefault();

        peticion('/STControl/CerrarSesion')
            .then(response => {
                if(response.status == 200){
                    window.location.replace('../pageIndex/index.jsp');
                }else
                    alert("Hubo un error, por favor vuelve a intentar");
            }).catch( ()  => {
                alert("Hubo un error, por favor vuelve a intentar");
            })


    })
}