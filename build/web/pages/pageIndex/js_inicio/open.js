var btn = document.querySelector('#btn_access');
var ui = new Interface();

let abierto = false;

// Evento en caso de que se presione el boton "Acceder"
btn.addEventListener('click', e=>{
    e.preventDefault();
    if(abierto == false){
        abrirSesion();
        abierto = true;
    }

})

// Inserta un elemento hijo en el padre en el dom
function insertar(identificador, elemento){
    let padre = document.querySelector(identificador);
    padre.appendChild(elemento);
}

// Se ejecuta por defecto, cuando se quiere abrir una sesion
function abrirSesion(){
    let contenedor = ui.crearElemento('div', [], 'contenedor_sesion');

    contenedor.innerHTML = `${superior} ${crearCuenta} ${iniciarSesion}`;

    insertar('#contenedor_principal', contenedor);

    btnCerrar = document.querySelector('#cerrar');
    btnCerrar.addEventListener('click', e=>{
        contenedor.remove();
        abierto = false;
    });
}

function abrirLogin(){
    //Contenido
    console.log("ups");
}
