// Esta funcion agrega el evento de cuando se presiona el boton para acceder a la cuenta
function mostrarLogin(){
    const btn = document.querySelector('#btn_access');


    // Evento en caso de que se presione el boton "Acceder"
    btn.addEventListener('click', e=>{
        e.preventDefault();

        // Verifica que no se haya abierto ya la seccion de login
        if(abierto == false){
            abrirSesion();
            abierto = true;
        }else{
            if(alertaExt == false){
                // inserta una alerta en caso de que ya se haya abierto la seccion de login
                ui.insertarAlerta("#alertaForm", "Ya abriste la seccion de crear cuenta/iniciar sesion");
                alertaExt = true;
                // Elimina la alerta despues de un tiempo
                setTimeout(function(){
                    document.querySelector('#ventanaAlerta').remove();
                    alertaExt = false;
                }, 5000);
            }
        }

    })
}

// Se ejecuta por defecto, cuando se quiere abrir una sesion
function abrirSesion(){
    // Se crea el elemento contenedor
    let contenedor = ui.crearElemento('div', '', ['id','contenedor_sesion']);

    // Se le agrega contenido al elemento contenedor
    contenedor.innerHTML = `
    <div id="alertaForm"> </div>
    <div id="formInstert">
        ${superior} 
        <div class="formularioRelleno"> 
            ${crearCuenta}
        </div>
    </div>`;

    // Insertado del elemento contenedor al cuerpo html
    document.querySelector('body').appendChild(contenedor);

    // Se cargan todos los eventos que corresponden al elemento contenedor
    eventosSup();
    cerrarContLogin();
}