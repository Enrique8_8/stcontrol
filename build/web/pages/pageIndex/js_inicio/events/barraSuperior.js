// Funcion que permite modificar el contenido del contenedor del login
function eventosSup(){
    let crear = document.querySelector('#crear');
    let iniciar = document.querySelector('#iniciar');
    
    // Si se presiona sobre crear, muestra la seccion de crear cuenta
    crear.addEventListener('click', ()=>{
        console.log("Ir a crear");
        verCrear();
    });
    
    // Si se presiona sobre iniciar, muestra la seccion iniciar sesion
    iniciar.addEventListener('click', ()=>{
        console.log("Ir a inicio")
        verInicio();
    });
}

// Modifica el formulario que aparece, muestra el de crear cuenta
function verCrear(){
    let cont = document.querySelector('.formularioRelleno');

    cont.innerHTML = ` ${crearCuenta}`;
}

// Modifica el formulario que aparece, muestra el de iniciar sesion
function verInicio(){
    console.log("iniio")
    let cont = document.querySelector('.formularioRelleno');

    cont.innerHTML = `${iniciarSesion}`;
}

function cambiarTemplate(template){
    let padre = document.querySelector('#id');
    padre.innerHTML = template;
}