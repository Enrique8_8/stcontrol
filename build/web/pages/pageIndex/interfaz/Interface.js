/* 
    Esta clase sera la principal a la hora de manejar elementos del DOM, creandolos dependiendo de 
    las necesidades que se presenten
*/
class Interface{
    constructor(){ }

    // Este metodo crea un elemento del DOM y lo devuelve
    crearElemento(tipo, text='', ...atributos){
        let element = document.createElement(tipo);

        atributos.forEach((atributo)=>{
            element.setAttribute(atributo[0], atributo[1]);
        });

        element.textContent = text;
    
        return element;
    }

    insertarAlerta(padre, mensaje){
        let alerta = document.createElement('div');
        alerta.id = 'ventanaAlerta';

        alerta.innerHTML = `<p>${mensaje}<p>`;
        document.querySelector(padre).appendChild(alerta);
    }


}