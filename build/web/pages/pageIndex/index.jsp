<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE html>
<html lang="es">
<!--
Grupo 4IM8
Equipo: Deep Space
Integrantes: 
        Acevedo Romo Jimena Olivia
        Cervantes Ocaña Jesús Enrique
        Gutiérrez Viveros Cristian Rogelio
        Mendoza Aguilar Eduardo
        Rendón Sierra Carlos Alexis
-->


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>STControl</title>

    <!-- Estilos generales-->
    <t:estilosGenerales></t:estilosGenerales>
    <link href="../../css/stylesMapaF.css" rel="stylesheet" type="text/css"/>
    <link type="text/css" rel="stylesheet" href="../../leaflet/leaflet.css">
    <link type="text/css" rel="stylesheet" href="../../leaflet/leaflet.icon-material.css">

    <script src="../../leaflet/leaflet.js"></script>
    <script src="../../leaflet/leaflet.icon-material.js"></script>


    <!-- Estilos propios -->
    <link rel="stylesheet" href="css/styles.css">
    
</head>

<body>
   
<t:Header username="${sessionScope.username}"></t:Header>
    <div id="hero">
        <img src="assets/metrohero.jpg" alt="Hero">
        <div id="textoHero">
            <p>STControl</p>
            <p>La seguridad en tu viaje</p>
        </div>
    </div>

    <div id="contenedor_principal">

        <section id="mapaSeccion">
            <t:Mapa></t:Mapa>
        </section>

        <div id="pares">
            <section id="funciones">
                <h3 class="titulo">Funcionalidades</h3>
                <div id="tarjetasFunc">
                    <div class="tarjeta">
                        <div class="circulo">
                            <h3 class="tituloFunc">Registros</h3>
                        </div>
                        <div class="cont">
                            <p>Seccion de reportes, en la que podras ver todos los reportes sobre que se tienen registrados en el sistema, asi como tambien podras crear tus propios reportes, a travez de tener iniciada una cuenta de STControl </p>
                            <a href="../pageVerReportes/mostrarReportes.jsp" class="btnFunc">Ver Seccion</a>
                        </div>
                    </div>
                    <div class="tarjeta">
                        <div class="circulo">
                            <h3 class="tituloFunc">Estadisticas</h3>
                        </div>
                        <div class="cont">
                            <p>Seccion de estadisticas, en la que podras visualizar un mapa que te permita ver de manera grafica la localizacion de las estaciones del metro, asi como percibir la cantidad de reportes que se tienen registrados de cada zona</p>
                            <a href="../pageEstadisticas/estadisticasMapa.jsp" class="btnFunc">Ver Seccion</a>
                        </div>
                    </div>
                    <div class="tarjeta">
                        <div class="circulo">
                            <h3 class="tituloFunc">Foro</h3>
                        </div>
                        <div class="cont">
                            <p>Seccion de foro, en la que podras exponer tus dudas acerca del sistema de transporte colectivo metro con los demas usuarios del sistema, interactuando de manera libre con todos los usuarios que hayan creado su cuenta de STControl</p>
                            <a href="../pageForo/foro.jsp" class="btnFunc">Ver Seccion</a>
                        </div>
                    </div>
                    <div class="tarjeta">
                        <div class="circulo">
                            <h3 class="tituloFunc">Acerca de</h3>
                        </div>
                        <div class="cont">
                            <p>Seccion de acerca de, en la que podras los objetivos del desarrollo del proyecto STControl, asi como tambien encontrar informacion sobre Deep Space, el equipo de desarrollo del proyecto </p>
                            <a href="../pageAcercaDe/acercaDe.jsp" class="btnFunc">Ver Seccion</a>
                        </div>
                    </div>
                </div>
            </section>

            <section id="foroEntradas">
                <h3 class="titulo">Ultimas entradas del foro</h3>

                <div id="entradasRecientes"></div>
                <a href="../Foro/Foro.html" id="btnForoEntradas">Ver todas</a>

            </section>
        </div>

    </div>
    
    <t:Footer></t:Footer>

    <script src="../../interfaz/Interface.js"></script>
    <script src="../../js/cargarMetro/cargarEstaciones.js"></script>
    <script src="../../js/cargarMetro/filtradoEstaciones.js"></script>
    <script src="../../js/peticiones/peticion.js"></script>

    <script src="../../data/lineas_geoJSON.js"></script>
    <script src="../../data/estaciones_geoJSON.js"></script>

    <script src="../../js/mapa/mapa.js"></script>
    <script src="../../js/mapa/mostrarFiltro.js"></script>
    <script src="../../js/mapa/aplicarFiltros.js"></script>
    <script src="../../js/mapa/cargarMapa.js"></script>
    <script src="js/templateEntrada.js" type="text/javascript"></script>
    <script src="js/events/cargaIndex.js" type="text/javascript"></script>
</body>

</html>
