document.addEventListener('DOMContentLoaded', e => {
    let parametros = new URLSearchParams( window.location.search );
    let mensaje = document.querySelector('#mensaje');
    if(parametros.has('creado') && parametros.get('creado') == 'true'){
        mensaje.textContent = 'Se ha creado tu registro, te agradecemos por tu participacion'
    }else if(parametros.has('editado') && parametros.get('editado') == 'false'){
        mensaje.textContent = 'No se pudo editar el registro, por favor vuelve a tratar accediendo desde tu cuenta';
    }else{
        mensaje.textContent = 'Recarga'
    }
})