function detectadoParametros(){
    let params = new URLSearchParams(window.location.search);
    let ui = new Interface();
    let cont = document.querySelector('#alertaError');

    if(params.has('caracteresInv') && params.get('caracteresInv')=='true'){
        ui.insertarAlerta(cont, "Por favor, usa caracteres validos al hacer tu registro")
        setTimeout(()=>{
            cont.innerHTML = '';
        }, 5000);
        
    }else if(params.has('redireccionado') && params.get('redireccionado')=='true'){
        ui.insertarAlerta(cont, "Para tener interacciones como crear un reporte, una entrada al foro, o un comentario es necesario tener una sesion iniciada. ")
        setTimeout(()=>{
            cont.innerHTML = '';
        }, 5000);

    }
}