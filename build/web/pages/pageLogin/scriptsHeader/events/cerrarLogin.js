// Funcion que agrega evento al icono de cerrar ventana, de la ventana del login
function cerrarContLogin(){
    
    // Selecciona el elemento cerrar
    btnCerrar = document.querySelector('#cerrar');

    // Agrega el evento
    btnCerrar.addEventListener('click', e=>{
        // Elimina el elemento
        if(alertaExt == true){
            clearTimeout(timeOut)
        }
        document.querySelector('#contenedor_sesion').remove();
        document.querySelector('#contenedor_principal').classList.remove('difuminar')
        // Modifica la variable global abierto, haciendo que despues se pueda permitir volver a abrir la ventana de login
        abierto = false;
        alertaExt = false;
    });
}