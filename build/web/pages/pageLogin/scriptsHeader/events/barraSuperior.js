
function eventosSup(){
    /* 
     * Funcion que permite modificar el contenido del contenedor del login
     */
    let crear = document.querySelector('#crear');
    let iniciar = document.querySelector('#iniciar');
    
    // Si se presiona sobre crear, muestra la seccion de crear cuenta
    crear.addEventListener('click', ()=>{
        console.log("Ir a crear");
        verCrear();
    });
    
    // Si se presiona sobre iniciar, muestra la seccion iniciar sesion
    iniciar.addEventListener('click', ()=>{
        console.log("Ir a inicio");
        verInicio();
    });
}

// Modifica el formulario que aparece, muestra el de crear cuenta
function verCrear(){
    console.log("crear cuenta");
    cambiarTemplate(crearCuenta);
}

// Modifica el formulario que aparece, muestra el de iniciar sesion
function verInicio(){
    console.log("inicio");
    cambiarTemplate(iniciarSesion);
}

function cambiarTemplate(template){
    document.querySelector('#formularioRelleno').innerHTML = template;
}