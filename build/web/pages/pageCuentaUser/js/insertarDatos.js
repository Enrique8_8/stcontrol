function insertarMiembro(data_prev){
    let data = JSON.parse(data_prev)
    let nombreU = document.querySelector('#nombreUsuario'), 
        correoU = document.querySelector('#correo'),
        passwordU = document.querySelector('#password'),
        lineaU = document.querySelector('#linea');
    
    nombreU.textContent = data.nombre;
    correoU.value = data['correo'];
    passwordU.value = data['pass'];
    lineaU.value = "Metro " + data['linea'];

    document.querySelector('#textoSeccionLinea1').textContent += ` ${data['linea']}`;
    document.querySelector('#textoSeccionLinea2').textContent += ` ${data['linea']}`;
    
}

function insertarReportes(data_prev) {
    let data = JSON.parse(data_prev);
    let contReporte = document.querySelector('#contReportes');
    for(let coso of data){
        let reporte = templateReportes(coso['id_i'], coso['tipo'], coso['linea'],coso['estacion'], coso['coment'], coso['fecha'], coso['hora']);

        contReporte.appendChild(reporte);
    }
}

function insertarForo(data_prev) {
    let data = JSON.parse(data_prev)
    let contEnt = document.querySelector('#entradasForo');
    for(let coso of data){
        let entrada = templateEntrada(coso['id_e'], coso['nombre_e'], coso['desc']);

        contEnt.appendChild(entrada);
    }
}

function insertarReportesLinea(data_prev){
    let data = JSON.parse(data_prev);
    let contRepL = document.querySelector('#contReportesLinea');

    for(let coso of data){
        let reporte = templateReportes(coso['id_i'], coso['tipo'], coso['linea'],coso['estacion'], coso['coment'], coso['fecha'], coso['hora'], false);
        contRepL.appendChild(reporte);
    }
}

function insertarComentariosUser(data_prev){
    let data = JSON.parse(data_prev);
    let contComentario = document.querySelector('#contComentariosUser');
    for(let coso of data){
        let coment = templateComentarioUser(coso['id_c'], coso['desc'], coso['id_e']);
        contComentario.appendChild(coment);
    }
}