function eventosReportes(){
    let botonesEdit = document.querySelectorAll('.editarRep');
    let botonesBorrar = document.querySelectorAll('.borrarRep');

    // Asignacion del evento de editar
    for(let btn of botonesEdit){
        btn.addEventListener('click', e =>  {
            e.preventDefault();
            let contenedor = btn.parentElement.parentElement;

            let id = contenedor.id;

            // Redireccionamiento para ir al formulario de reporte
            let url = "../pageEditarReporte/editarReporte.jsp?id=" + id;
            window.location.replace( url );

        });
    }

    // Asignacion del evento de borrar
    for(let btn of botonesBorrar){
        btn.addEventListener('click', async e => {
            e.preventDefault();
            console.log("Entro")
            let id = btn.parentElement.parentElement.id;
            
            if(confirm("Confirma que deseas eliminar este reporte")){
                peticion("/STControl/EliminarReporteMiembro", ['id', id]).then((response)=>{
                    if(response.status == 200)
                        btn.parentElement.parentElement.remove();
                    else
                        alert("No se puede borrar")
                    
                }).catch(()=>{
                    alert('uis');
                });
            }
        });
    }

}