function eventosEntradasF(){
    let btnEditar = document.querySelectorAll('.editEntrada');
    let btnBorrar = document.querySelectorAll('.borrarEntrada');
    let btnComents = document.querySelectorAll('.btnComentariosEntrada');

    for(let btn of btnEditar){
        btn.addEventListener('click', e => {
            e.preventDefault();

            let contenedor = btn.parentElement.parentElement;
            console.log(contenedor)
            let id = contenedor.id;

            // Redireccionamiento para ir al formulario de editar entrada
            let url = "../pageEditarEntrada/editarEntrada.jsp?id=" + id.substring(7);
            window.location.replace( url );
        })
    }

    for(let btn of btnBorrar){
        btn.addEventListener('click', async e => {
            e.preventDefault();

            let contenedor = btn.parentElement.parentElement;
            
            if(confirm("Confirma que quieres eliminar esa entrada del foro")){
                let id = contenedor.id.substring(7);
                let response = await peticion('/STControl/EliminarEntradaMiembro', ['id',id])
                if(response.status == 200){
                    let data = await response.json();
                    confirmBorrado(data, contenedor);
                }else
                    alert("No se puede borrar tu entrada, por favor reintentalo")
                
            }
        })
    }

    for(let btn of btnComents){
        btn.addEventListener('click', async e => {
            e.preventDefault();

            let idPadre = btn.parentElement.parentElement.id.substring(7);
            let contPrevioComent = document.querySelector(`#entrada${idPadre} .contComentarios`);
            if(contPrevioComent.style.display == '' || contPrevioComent.style.display == 'none'){
                contPrevioComent.style.display = 'block';
                let response = await peticion('/STControl/ObtenerComentarios', ['idEntrada', idPadre]);
                let data = await response.json();

                for(let coment of data){
                    document.querySelector(`#entrada${idPadre} .contComentarios .contComentarioComent`).appendChild(templateComentario(coment['id_c'], coment['desc'], coment['nombre_m']));
                }

                let crearComent = document.createElement('div');
                crearComent.classList.add('crearComentario');
                crearComent.innerHTML = `
                    <textarea type="text" name="comentario" class="comentarioCrearText" placeholder="Crear comentario" rows="2" cols="40" maxlength="120" minlength="1" title="Maximo 120 caracteres"></textarea>
                    <button class="btnCrearComentario">
                        <span class="material-icons">
                            send
                        </span>
                    </button>`;
                document.querySelector(`#entrada${idPadre} .contComentarios .contBtnCrearComentario`).appendChild(crearComent);
                eventoEnviarComentario(`entrada${idPadre}`,idPadre );
            }else{
                contPrevioComent.style.display = 'none'
                document.querySelector(`#entrada${idPadre} .contComentarios .contComentarioComent`).innerHTML = '';
                document.querySelector(`#entrada${idPadre} .contComentarios .contBtnCrearComentario`).innerHTML = '';
            }
            
        })
    }

}
