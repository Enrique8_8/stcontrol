function eventosConfig(){
    let formulario = document.querySelector('#formDatosCuenta');

    let btnEditar = document.querySelector('#btnEditarCuenta');
    let btnCancelar = document.querySelector('#btnCancelarConfig');
    let btnBorrarCuenta = document.querySelector('#btnBorrarCuenta');

    let campos = document.querySelectorAll('.campoConfig');
    let contConfig = document.querySelector('#bntConfig');
    let contCambios = document.querySelector('#btnCambios');

    let borrar = true;

    btnEditar.addEventListener('click', e => {
        e.preventDefault();
        for(let campo of campos)
            campo.disabled = false;
        
        contConfig.style.display = "none";
        contCambios.style.display = "flex";

        // Hay que buscar la ruta para que esto jale
        borrar = false;
        formulario.action = "/STControl/EditarDatosMiembro";
    });

    btnCancelar.addEventListener('click', e =>{
        e.preventDefault();

        for(let campo of campos)
            campo.disabled = true;
        
        contConfig.style.display = "flex";
        contCambios.style.display = "none";

        // Hay que buscar la ruta para que esto jale
        borrar = true;
        formulario.action = "/STControl/EliminarCuenta";
    });

    /* Creo que esto hace que ya no se ejecute lo que se debe de ejecutar 
        En caso de que no jale el action con esto, entonces hay que buscarle como
    */
    formulario.addEventListener('submit', e => {
        e.preventDefault();

        console.log("uis")
        if(borrar){
            if(confirm("¿De verdad quieres borrar tu cuenta?")){
                formulario.submit();
                console.log("Borrado")
            }
        }else{
            formulario.submit();
            console.log("Cambios hechos");
        }
        
    })
}
eventosConfig();
