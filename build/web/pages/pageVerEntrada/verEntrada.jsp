<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ver entrada</title>

        <!-- Estilos generales-->
        <t:estilosGenerales></t:estilosGenerales>

            <!-- Estilos Propios-->
            <link rel="stylesheet" href="stylesVerEntrada.css">
        </head>

        <body>
        <t:Header username="${sessionScope.username}"></t:Header>
            <div id="contenedorPrincipal">
                <div id="contenedorEntrada">
                    <h1>Entrada</h1>
                    <div id="contEntrada">
                        <div id="entrada">
                            <p id="nick"></p>
                            <p id="nombre"></p>
                            <p id="contenido"></p>
                        </div>

                        <div id="contComentarios">
                            <div id="contComentarioComent">

                            </div>
                            <div id="contBtnCrearComentario">
                                <textarea type="text" name="comentario" class="comentarioCrearText" placeholder="Crear comentario"
                                          rows="2" cols="20" maxlength="120" minlength="1" title="Maximo 120 caracteres"></textarea>

                                <button id="btnCrearComentario">
                                    <span class="material-icons">
                                        send
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <t:Footer></t:Footer>


        <script src="../../js/peticiones/peticion.js" type="text/javascript"></script>
        <script src="js/events/cargaInicial.js"></script>

    </body>

</html>