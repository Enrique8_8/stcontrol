document.addEventListener('DOMContentLoaded', async e => {
    e.preventDefault();

    let urlSearch = new URLSearchParams(window.location.search);
    if(urlSearch.has('id')){
        let idEntrada = urlSearch.get('id');
        let response = await peticion('/STControl/ObtenerEntradaComentario', ['idEntrada', idEntrada]);
        if(response.status == 200){
            let data = await response.json();
            
            insertarData(JSON.parse(data['entrada']), JSON.parse(data['comentario']));

        }
    }else{
        window.location.replace(' ../pageIndex/index.jsp ');
    }

})

function insertarData(entrada, comentarios){
    let nick = document.querySelector('#nick'), 
        nombre = document.querySelector('#nombre'),
        cont = document.querySelector('#contenido'),
        contComentario = document.querySelector('#contComentarioComent');
    
    nick.textContent = entrada['nombre_m'];
    nombre.textContent = entrada['nombre_e'];
    cont.textContent = entrada['desc'];

    for(let coment of comentarios){
        let contenedor = document.createElement('div');
        contenedor.classList.add('comentario');
        contenedor.innerHTML = `
            <p>${coment['desc']}</p>
            <p>${coment['nombre_m']}</p>
        `

        contComentario.appendChild(contenedor);

    }

}

