function cambiarDeep() {
  document.getElementById("Titulo").innerHTML = "DeepSpace";
  document.getElementById("informacion").innerHTML =
    "Deep Space busca mostrar ante el mundo la competitividad mexicana en el entorno tecnológico, demostrando la alta capacidad que nuestro país tiene para imponerse ante mercados cada vez más masivos. Estamos comprometidos en darle al público un software útil, fácil de entender y verídico. Buscamos ser la página más confiable a la hora de consultar la información, por lo que brindamos el mayor soporte posible ante cualquier situación que se presenta durante el uso de nuestro programa. De la misma manera, sometemos a nuestros productos mostrados a una rigurosa prueba de calidad buscando que el software a ofrecer esté libre de errores, sea útil y esté acorde a lo que nuestro público prefiera. ";
}

function cambiarOPA() {
  document.getElementById("Titulo").innerHTML = "Objetivo de STControl";
  document.getElementById("informacion").innerHTML =
    "Mostrar las rutas más seguras a través de gráficas y un mapa. Se registran los incidentes de cada línea. Informar a los usuarios de la aplicación sobre los eventos recientes . Ayudar a los usuarios a dar a conocer lo que les ha sucedido por medio de reportes. Mejorar la experiencia de los usuarios en el Metro asi como mas segura";
}

function cambiarNosotros() {
  document.getElementById("Titulo").innerHTML = "Informacion de DeepSpace";
  document.getElementById("informacion").innerHTML =
    "Queremos ser una empresa reconocida por brindar datos sobre la seguridad de diferentes transportes públicos. Queremos hacer una página web que permita obtener información acerca de los fallos en la seguridad del metro de la zona Metropolitana y así ayudar a prevenir futuros percances a las personas que lo usen. ";
}

function cambiarMision() {
  document.getElementById("Titulo").innerHTML = "Mision";
  document.getElementById("informacion").innerHTML =
    "Somos una empresa dedicada a la creación de software que ayude a disminuir y eliminar diferentes problemáticas sociales actuales en el entorno en el que nos encontramos y de esta manera mejorar la calidad de vida de las personas que conforman nuestra sociedad. ";
}

function cambiarVision() {
  document.getElementById("Titulo").innerHTML = "Vision";
  document.getElementById("informacion").innerHTML =
    "Deep Space es una empresa desarrolladora de software con el propósito de ayudar a las personas que utilizan el transporte público tratando de mejorar la seguridad de las personas que se transportan en él.  ";
}

function cambiarObjetivo() {
  document.getElementById("Titulo").innerHTML = "Objetivo";
  document.getElementById("informacion").innerHTML =
    "Tener un catálogo variado y muy completo, acorde a las necesidades de la población común, de jóvenes y de empresasAccesibilidad a la hora de obtener softwareBrindar información segura y de calidad a nuestros usuarios.Extensión por toda el Área Metropolitana. Que los programas sean de fácil entendimiento para el usuario.Añadir calidad y veracidad al producto.";
}

function cambiarContacto() {
  document.getElementById("Titulo").innerHTML = "Contacto";
  document.getElementById("informacion").innerHTML =
    "Numero Telefonico: 9090909090  Correo@correo.com";
}

function cambiarAyuda() {
  document.getElementById("Titulo").innerHTML = "Ayuda";
  document.getElementById("informacion").innerHTML =
    "Cualquier duda o sugerencia a nuestro programa por favor contactenos en nuestro numero telefonico o correo electronico ";
}

document.getElementById("DeepSpace").onclick = function() {
  cambiarDeep();
};

document.getElementById("ObjetivoPA").onclick = function() {
  cambiarOPA();
};

document.getElementById("Nosotros").onclick = function() {
  cambiarNosotros();
};

document.getElementById("Mision").onclick = function() {
  cambiarMision();
};

document.getElementById("Vision").onclick = function() {
  cambiarVision();
};

document.getElementById("ObjetivoDS").onclick = function() {
  cambiarObjetivo();
};

document.getElementById("Contacto").onclick = function() {
  cambiarContacto();
};

document.getElementById("Ayuda").onclick = function() {
  cambiarAyuda();
};
