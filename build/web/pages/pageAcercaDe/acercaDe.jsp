<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE html>
<html>

<head>
    <title>Acerca de</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Estilos generales-->
    <t:estilosGenerales></t:estilosGenerales>

    <!-- Estilos propios-->
    <link href="css/EstiloAcerca.css" rel="stylesheet" type="text/css" />

</head>

<body>
    <t:Header username="${sessionScope.username}"></t:Header>

    <div class="contenedorPrincipal">

        <div class="Info">
            <!-- Trabajar con este -->
            <h1 class="titulos" id="Titulo">Objetivo de STControl</h1>

            <p class="Texto" id="informacion">
                Mostrar las rutas más seguras a través de gráficas y un mapa.
                Se registran los incidentes de cada línea.
                Informar a los usuarios de la aplicación sobre los eventos recientes .
                Ayudar a los usuarios a dar a conocer lo que les ha sucedido por medio de reportes.
                Mejorar la experiencia de los usuarios en el Metro asi como mas segura.

            </p>
        </div>

        <!-- Indice -->

        <div id="Indice">
            <div class="Estatico">
                <h1 class="titulos">Acerca de</h1>
                <ul>
                    <li class="Expande" id="ObjetivoPA">
                        <p>Objetivo de STControl</p>
                    </li>
                    <li class="Expande" id="DeepSpace">
                        <p>DeepSpace</p>
                    </li>
                    <li class="Expande">
                        <p id="Nosotros">Sobre Nosotros</p>

                        <ul>
                            <li id="Mision">
                                <p>Mision</p>
                            </li>
                            <li id="Vision">
                                <p>Vision</p>
                            </li>
                            <li id="ObjetivoDS">
                                <p>Objetivo</p>
                            </li>
                        </ul>
                    </li>

                    <li class="Expande" id="Contacto">
                        <p>Contacto</p>
                    </li>
                    <li class="Expande" id="Ayuda">
                        <p>Ayuda</p>
                    </li>
                </ul>
            </div>

            <div id="barra"></div>

        </div>

    </div>

    <t:Footer></t:Footer>
   
    <script src="Ayuda.js" type="text/javascript"></script>
</body>

</html>

<!-- <li class="Expandees" id="Mision"><a HREF=#>Mision</a>
                    <li class="Expandep" id="Vision"><a HREF=#>Vision</a>
                    <li class="Expandep" id="ObjetivoDS"><a HREF=#>Objetivo</a> -->