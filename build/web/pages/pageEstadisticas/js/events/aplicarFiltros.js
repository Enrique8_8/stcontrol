function aplicarFiltros(mapa){
    let formulario = document.querySelector('#formFiltros');

    formulario.addEventListener('submit', e => {
        e.preventDefault();
        let lineas = [], estacion = [], inc = [];

        let lineasCheck = document.getElementsByName('LineaMetro');
        let estCheck = document.getElementsByName('estacionesForm');
        let numsCheck = document.getElementsByName('numIncidencias');
        /*
            Todavia hay que buscar un modo de filtrar estos datos, para despues mandar una peticion fetch que sirva para obtener la informacion que se necesita
        */
        for(let l of lineasCheck){
            console.log(l)
            if(l.checked == true)
                lineas.push(l.value);
        }
        for(let e of estCheck){
            if(e.checked == true){
                estacion.push(e.value);
            }
        }
        for(let i of numsCheck){
            if(i.checked == true)
                inc.push(Number.parseInt(i.value));
        }

        mapa.filtrarMarkers(lineas, estacion, inc);

    })
}


function obtenerDatos(nombre){
    console.log("Obtencion de los campos de: ", nombre);

    let campos = document.getElementsByName(nombre);

    let resultados = [];

    for(let campo of campos){
        if(campo.checked){
            resultados.push(campo.value);
        }
    }

    return resultados;

}