<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Estadisticas</title>

        <!-- Estilos generales-->
        <t:estilosGenerales></t:estilosGenerales>
        
            <link href="../../css/stylesMapaF.css" rel="stylesheet" type="text/css"/>
            <link type="text/css" rel="stylesheet" href="../../leaflet/leaflet.css">
            <link type="text/css" rel="stylesheet" href="../../leaflet/leaflet.icon-material.css">

            <script src="../../leaflet/leaflet.js"></script>
            <script src="../../leaflet/leaflet.icon-material.js"></script>

            <link type="text/css" rel="stylesheet" href="css/stylesMapa.css">

        </head>

        <body>
        <t:Header username="${sessionScope.username}"></t:Header>

            <div id="contenedorPrincipal">
               <t:Mapa></t:Mapa>
            </div>

        <t:Footer></t:Footer>
        <script src="../../interfaz/Interface.js"></script>
        <script src="../../js/cargarMetro/cargarEstaciones.js"></script>
        <script src="../../js/cargarMetro/filtradoEstaciones.js"></script>
        <script src="../../js/peticiones/peticion.js"></script>

        <script src="../../data/lineas_geoJSON.js"></script>
        <script src="../../data/estaciones_geoJSON.js"></script>

        <script src="../../js/mapa/mapa.js"></script>
        <script src="../../js/mapa/mostrarFiltro.js"></script>
        <script src="../../js/mapa/aplicarFiltros.js"></script>
        <script src="../../js/mapa/cargarMapa.js"></script>

        <script src="js/events/cargaPagina.js"></script>
        
    </body>

</html>