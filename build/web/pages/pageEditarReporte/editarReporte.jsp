<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Entrada</title>
    <!-- Estilos generales -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600|Overpass+Mono:400,600|Poppins:400,600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Monoton&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="../../css/Normalize.css">
    <link rel="stylesheet" href="../../css/stylesHeader.css">
    <link rel="stylesheet" href="../../css/stylesFooter.css">
    <link rel="stylesheet" href="../../interfaz/interface.css">

    <link rel="stylesheet" href="../../css/stylesGenerales.css"> -->
    <t:estilosGenerales></t:estilosGenerales> 
   
        <!-- Estilo propio -->
    <link rel="stylesheet" href="css/stylesEditarReporte.css">
    
</head>
<body>
    <t:Header username="${sessionScope.username}"></t:Header>
    <div id="contenedorPrincipal">
        <form action="/STControl/EditarReporte" method="post">
            <div id="contenedorForm">

                <div class="conjuntoDatos">
                    <legend>Estación</legend>
                    <div id="contData1">
                        <div class="camposEst" id="contLineas">
                            <label for="linea">Selecciona la linea</label>
                            <select id="linea" name="linea" required>
                                <option value="0" selected>Selecciona una opción</option>
                                <option value="Metro 1">Línea 1</option>
                                <option value="Metro 2">Línea 2</option>
                                <option value="Metro 3">Línea 3</option>
                                <option value="Metro 4">Línea 4</option>    
                                <option value="Metro 5">Línea 5</option>
                                <option value="Metro 6">Línea 6</option>
                                <option value="Metro 7">Línea 7</option>
                                <option value="Metro 8">Línea 8</option>
                                <option value="Metro 9">Línea 9</option>
                                <option value="Metro A">Línea A</option>
                                <option value="Metro B">Línea B</option>
                                <option value="Metro 12">Línea 12</option>
                            </select>
                        </div><!-- #contLineas -->

                        <div class="camposEst" id="contEstaciones">
                            <label for="estaciones">Selecciona la estacion</label>
                            <select id="estaciones" name="estaciones" required>
                                <option value="0" selected>Selecciona una estación</option>
                            </select>
                        </div><!-- #contEstaciones -->
                    </div><!-- #contData1 -->
                </div><!-- .conjuntoDatos Linea-->

                <div class="conjuntoDatos">
                    <legend>Fecha</legend>
                    <div id="contData2">

                        <div id="contDia">
                            <label for="dia">Dia: </label>
                            <input type="date" name="dia" id="dia" required>
                        </div><!-- #contDia -->

                        <div id="contHora">
                            <label for="hora">Hora: </label>
                            <input type="time" name="hora" id="hora" required>
                        </div><!-- #contHora-->
                    </div><!-- #contData2-->
                </div><!-- .conjuntoDatos Fecha / Hora-->

                <div class="conjuntoDatos">
                    <legend>Descripción de lo ocurrido</legend>

                    <div id="contData3">
                        <textarea name="desc" id="desc" cols="50" rows="15" minlenght="0" maxlenght="50" required></textarea>

                        <div id="pregunta">
                            <label for="tipo">¿Que tipo de incidente fue?</label>
                            <select name="tipo" id="tipo" required>
                                <option selected disabled>Selecciona una opcion</option>
                                <option value="asalto">Asalto</option>
                                <option value="robo">Robo</option>
                                <option value="acoso">Acoso</option>
                                <option value="pelea">Pelea</option>
                                <option value="gritos">Gritos</option>
                                <option value="otros">Otros</option>
                            </select>
                        </div>
                                
                    </div><!-- #contData3-->
                </div><!-- .conjuntoDatos -->

                <div id="contBtn">
                    <button class="btn_form" type="submit" value="0" name="idReporte" id="btnSubmit">Aceptar</button>
                    <button class="btn_form" type="reset" value="Limpiar" name="btn_limpiar" >Limpiar</button>
                </div><!-- #contBtn -->
            </div>

        </form>
    </div>
    <t:Footer></t:Footer>

    <script src="../../data/estaciones_geoJSON.js"></script>
    <script src="../../js/cargarMetro/cargarEstaciones.js"></script>
    <script src="../../js/cargarMetro/estacionesSelect/seleccionMetro.js"></script>
    <script src="../../interfaz/Interface.js"></script>

    <script src="js/events/cargaInicial.js"></script>
    <script src="../../js/peticiones/peticion.js"></script>
</body>
</html>