<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Entrada del Foro</title>

    <!-- Estilos generales -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600|Overpass+Mono:400,600|Poppins:400,600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Monoton&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="../../css/Normalize.css">
    <link rel="stylesheet" href="../../css/stylesHeader.css">
    <link rel="stylesheet" href="../../css/stylesFooter.css">
    <link rel="stylesheet" href="../../interfaz/interface.css">

    <link rel="stylesheet" href="../../css/stylesGenerales.css"> -->
    <t:estilosGenerales></t:estilosGenerales> 

    <!-- Estilo propio -->
    <link rel="stylesheet" href="css/stylesEditarEntrada.css">
</head>
<body>
    <t:Header username="${sessionScope.username}"></t:Header>
    <div id="contenedorPrincipal">
        <main>
            <h2>Editar Entrada del Foro</h2>

            <form action="/STControl/EditarEntrada" method="post">
                <input type="text" name="preg" id="preg">
                <textarea name="desc" id="desc" cols="30" rows="10"></textarea>
                <div id="botonesForm">
                    <button type="submit" name="idEntrada" id="btnSubmit">Aceptar</button>
                    <button type="reset">Limpiar</button>
                </div>
            </form>
        </main>
    </div>
    <t:Footer></t:Footer>
    <script src="../../js/peticiones/peticion.js" type="text/javascript"></script>
    <script src="js/events/cargaPagina.js" type="text/javascript"></script>
</body>
</html>

<%-- Hacer el servlet de editar entrada
    Hacer el servlet de obtener entrada
    hacer la consulta de obtener entrada
 --%>