document.addEventListener('DOMContentLoaded', async e => {
    e.preventDefault();

    let parametros = new URLSearchParams( window.location.search );
    let id;
    if(parametros.has('id')){
        id = parametros.get('id');
        let response = await peticion('/STControl/ObtenerEntrada', ['id', id]);
        if(response.status == 200){
            let data = await response.json();
            insertarDatos(data);
        }else{
            window.location.reload();
        }
        if(parametros.has('errorDatos') && parametros.get('errorDatos') == 'true'){
            alert("Hubo un error en los datos, por favor vuelve a intentar editar tu entrada");
        }
    }else{
        window.location.replace("../pageIndex/index.jsp");
    }
    

});

function insertarDatos(data){
    document.querySelector('#preg').value = data['nombre_e'];
    document.querySelector('#desc').value = data['desc'];
    document.querySelector('#btnSubmit').value = data['id_e'];
}