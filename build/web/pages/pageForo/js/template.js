function templateEntrada(id, username, nombre, descripcion){
    
    let ui = new Interface();
    let idB = "entrada" + id;
    
    let contenedor = ui.crearElemento('div', '', ['id', idB], ['class', 'entrada'])

    contenedor.innerHTML = `
                    <p class="nick">${username}</p>
                    <p class="nombre">${nombre}</p>
                    <p class="contenido">${descripcion}</p>

                    <p class="comentarios">
                        <i class="material-icons"> comment </i>
                        <span>Comentarios</span>
                        <i class="material-icons"> keyboard_arrow_down </i>
                    </p>

                    <div class="contComentarios">
                        <div class="contComentarioComent"></div>
                        <div class="contBtnCrearComentario"></div>
                    </div>`;
                    
    
    return contenedor;
}

function templateComentario(idComentario, comentario, user){
    let ui = new Interface();
    
    let contenedor = ui.crearElemento('div', '', ['class', 'comentario'], ['id', `coment${idComentario}`]);
    contenedor.innerHTML = `
        <p>${comentario}</p>
        <p>${user} </p>`;

    return contenedor;
}

function templateCrearComentario(){
    let ui = new Interface();

    let contenedor = ui.crearElemento('div', '', ['class', 'crearComentario']);
    contenedor.innerHTML = `
        <textarea type="text" name="comentario" class="comentarioCrearText" placeholder="Crear comentario" rows="2" cols="40" maxlength="120" minlength="1" title="Maximo 120 caracteres"></textarea>
        <button class="btnCrearComentario">
            <span class="material-icons">
                send
            </span>
        </button>`;
    return contenedor;
}