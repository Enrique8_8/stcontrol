function eventosComentarios(){
    let btnMostrarComentarios = document.querySelectorAll('.comentarios');

    for(let btn of btnMostrarComentarios){
        btn.addEventListener('click', e => {
            e.preventDefault();

            let idEntrada = btn.parentElement.id;
            console.log("la id es: " + idEntrada)
            
            let contenedor = document.querySelector('#'+idEntrada + ' .contComentarios')
            
            if(contenedor.style.display == "none" || contenedor.style.display == ""){
                contenedor.style.display = "block";
                let idPeticion = idEntrada.substring(7);
                peticion('/STControl/ObtenerComentarios', ['idEntrada', idPeticion])
                    .then( async response => {
                        if(response.status == 200){
                            let data = await response.json();

                            for(let coment of data){
                                document.querySelector('#'+idEntrada + ' .contComentarios .contComentarioComent').appendChild(templateComentario(coment['id_c'], coment['desc'], coment['nombre_m']));
                            }
                            document.querySelector('#'+idEntrada + ' .contComentarios .contBtnCrearComentario').appendChild(templateCrearComentario());
                            eventoEnviarComentario(idEntrada, idPeticion);
                        }
                    }).catch(() => {
                        console.log("uis");
                    });

            }else{
                contenedor.style.display = "none";
                document.querySelector(`#${idEntrada} .contComentarios .contComentarioComent`).innerHTML = '';
                document.querySelector(`#${idEntrada} .contComentarios .contBtnCrearComentario`).innerHTML = '';
            }
        });
    }

}
