<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE html>
<html>

<head>
    <title>Crear Registro</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Estilos generales-->
    <t:estilosGenerales></t:estilosGenerales>

    <!-- Estilos Propios-->
    <link rel="stylesheet" href="../../interfaz/interface.css">

    <link rel="stylesheet" href="css/stylesForm.css">


</head>

<body>
    <t:Header username="${sessionScope.username}"></t:Header>

    <div id="contenedor_principal">
        <main>
            <div class="barra" id="b1"></div><!-- #b1 -->
            <div id="central">
                <h1>Crear Registro</h1>

                <div id="errorAlerta"></div>

                <form action="../../CrearRegistro" method="post" id="formularioCrearRegistro">
                    <div id="contenedorForm">

                        <div class="conjuntoDatos">
                            <legend>Estación</legend>
                            <div id="contData1">
                                <div class="camposEst" id="contLineas">
                                    <label for="linea">Selecciona la linea</label>
                                    <select id="linea" name="linea" required>
                                        <option value="0" selected disabled>Selecciona una opción</option>
                                        <option value="Metro 1">Línea 1</option>
                                        <option value="Metro 2">Línea 2</option>
                                        <option value="Metro 3">Línea 3</option>
                                        <option value="Metro 4">Línea 4</option>
                                        <option value="Metro 5">Línea 5</option>
                                        <option value="Metro 6">Línea 6</option>
                                        <option value="Metro 7">Línea 7</option>
                                        <option value="Metro 8">Línea 8</option>
                                        <option value="Metro 9">Línea 9</option>
                                        <option value="Metro A">Línea A</option>
                                        <option value="Metro B">Línea B</option>
                                        <option value="Metro 12">Línea 12</option>
                                    </select>
                                </div><!-- #contLineas -->

                                <div class="camposEst" id="contEstaciones">
                                    <label for="estaciones">Selecciona la estacion</label>
                                    <select id="estaciones" name="estaciones" disabled>
                                        <option value="0" selected>Selecciona una estación</option>
                                    </select>
                                </div><!-- #contEstaciones -->
                            </div><!-- #contData1 -->
                        </div><!-- .conjuntoDatos Linea-->

                        <div class="conjuntoDatos">
                            <legend>Fecha</legend>
                            <div id="contData2">

                                <div id="contDia">
                                    <label for="dia">Dia: </label>
                                    <input type="date" name="dia" id="dia" required>
                                </div><!-- #contDia -->

                                <div id="contHora">
                                    <label for="hora">Hora: </label>
                                    <input type="time" name="hora" id="hora" required>
                                </div><!-- #contHora-->
                            </div><!-- #contData2-->
                        </div><!-- .conjuntoDatos Fecha / Hora-->

                        <div class="conjuntoDatos">
                            <legend>Descripción de lo ocurrido</legend>

                            <div id="contData3">
                                <textarea name="desc" id="desc" cols="50" rows="15" minlenght="0" maxlenght="50"
                                    required></textarea>

                                <div id="pregunta">
                                    <label for="tipo">¿Que tipo de incidente fue?</label>
                                    <select name="tipo" id="tipo" required>
                                        <option selected disabled>Selecciona una opcion</option>
                                        <option value="asalto">Asalto</option>
                                        <option value="robo">Robo</option>
                                        <option value="acoso">Acoso</option>
                                        <option value="pelea">Pelea</option>
                                        <option value="gritos">Gritos</option>
                                        <option value="otros">Otros</option>
                                    </select>
                                </div>

                            </div><!-- #contData3-->
                        </div><!-- .conjuntoDatos -->


                        <div id="contBtn">
                            <input class="btn_form" type="submit" value="Enviar" name="btn_enviar" id="btnSubmit">
                            <input class="btn_form" type="reset" value="Limpiar" name="btn_limpiar">
                        </div><!-- #contBtn -->
                    </div>

                </form>
            </div><!-- #central -->
            <div class="barra" id="b2"></div><!-- #b2 -->

        </main>
    </div>

    <t:Footer></t:Footer>

    <script src="../../data/estaciones_geoJSON.js"></script>
    <script src="../../js/cargarMetro/cargarEstaciones.js"></script>
    <script src="../../js/cargarMetro/estacionesSelect/seleccionMetro.js"></script>
    <script src="../../interfaz/Interface.js"></script>

    <script src="js/parametros.js"></script>
    <script src="js/events/seleccionOtros.js"></script>
    <script src="js/events/cargaPagina.js"></script>

</body>

</html>