package org.apache.jsp.tag.web;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Footer_tag
    extends javax.servlet.jsp.tagext.SimpleTagSupport
    implements org.apache.jasper.runtime.JspSourceDependent {


  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private JspContext jspContext;
  private java.io.Writer _jspx_sout;
  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public void setJspContext(JspContext ctx) {
    super.setJspContext(ctx);
    java.util.ArrayList<String> _jspx_nested = null;
    java.util.ArrayList<String> _jspx_at_begin = null;
    java.util.ArrayList<String> _jspx_at_end = null;
    this.jspContext = new org.apache.jasper.runtime.JspContextWrapper(ctx, _jspx_nested, _jspx_at_begin, _jspx_at_end, null);
  }

  public JspContext getJspContext() {
    return this.jspContext;
  }
  private javax.servlet.jsp.tagext.JspFragment footer;

  public javax.servlet.jsp.tagext.JspFragment getFooter() {
    return this.footer;
  }

  public void setFooter(javax.servlet.jsp.tagext.JspFragment footer) {
    this.footer = footer;
  }

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void doTag() throws JspException, java.io.IOException {
    PageContext _jspx_page_context = (PageContext)jspContext;
    HttpServletRequest request = (HttpServletRequest) _jspx_page_context.getRequest();
    HttpServletResponse response = (HttpServletResponse) _jspx_page_context.getResponse();
    HttpSession session = _jspx_page_context.getSession();
    ServletContext application = _jspx_page_context.getServletContext();
    ServletConfig config = _jspx_page_context.getServletConfig();
    JspWriter out = jspContext.getOut();
    if( getFooter() != null ) {
      _jspx_page_context.setAttribute("footer", getFooter());
}

    try {
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<footer>\n");
      out.write("        <div id=\"footer_sup\">\n");
      out.write("            <div id=\"footer_ctc\">\n");
      out.write("                <p>Contacto</p>\n");
      out.write("                <p>Numero Telefonico: 9090909090</p>\n");
      out.write("                <p>Correo@correo.com</p>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <div id=\"footer_ubi\">\n");
      out.write("                <div id=\"footer_txt\">\n");
      out.write("                    <p>Ubicacion</p>\n");
      out.write("                    <p>Mar Mediterráneo 227, Popotla, 11400 Ciudad de México, CDMX</p>\n");
      out.write("                </div>\n");
      out.write("                <img src=\"../../assets/Ubicacion.png\" alt=\"Ubicacion de Deep Space\">\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <p>Derechos Reservados Deep Space</p>\n");
      out.write("</footer>");
    } catch( Throwable t ) {
      if( t instanceof SkipPageException )
          throw (SkipPageException) t;
      if( t instanceof java.io.IOException )
          throw (java.io.IOException) t;
      if( t instanceof IllegalStateException )
          throw (IllegalStateException) t;
      if( t instanceof JspException )
          throw (JspException) t;
      throw new JspException(t);
    } finally {
      ((org.apache.jasper.runtime.JspContextWrapper) jspContext).syncEndTagFile();
    }
  }
}
