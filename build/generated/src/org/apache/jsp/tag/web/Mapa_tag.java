package org.apache.jsp.tag.web;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Mapa_tag
    extends javax.servlet.jsp.tagext.SimpleTagSupport
    implements org.apache.jasper.runtime.JspSourceDependent {


  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private JspContext jspContext;
  private java.io.Writer _jspx_sout;
  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public void setJspContext(JspContext ctx) {
    super.setJspContext(ctx);
    java.util.ArrayList<String> _jspx_nested = null;
    java.util.ArrayList<String> _jspx_at_begin = null;
    java.util.ArrayList<String> _jspx_at_end = null;
    this.jspContext = new org.apache.jasper.runtime.JspContextWrapper(ctx, _jspx_nested, _jspx_at_begin, _jspx_at_end, null);
  }

  public JspContext getJspContext() {
    return this.jspContext;
  }
  private javax.servlet.jsp.tagext.JspFragment mapa;

  public javax.servlet.jsp.tagext.JspFragment getMapa() {
    return this.mapa;
  }

  public void setMapa(javax.servlet.jsp.tagext.JspFragment mapa) {
    this.mapa = mapa;
  }

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void doTag() throws JspException, java.io.IOException {
    PageContext _jspx_page_context = (PageContext)jspContext;
    HttpServletRequest request = (HttpServletRequest) _jspx_page_context.getRequest();
    HttpServletResponse response = (HttpServletResponse) _jspx_page_context.getResponse();
    HttpSession session = _jspx_page_context.getSession();
    ServletContext application = _jspx_page_context.getServletContext();
    ServletConfig config = _jspx_page_context.getServletConfig();
    JspWriter out = jspContext.getOut();
    if( getMapa() != null ) {
      _jspx_page_context.setAttribute("mapa", getMapa());
}

    try {
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<div id=\"contenedorPrincipal\">\n");
      out.write("    <h1 class=\"encabezado\">Mapa del Metro</h1>\n");
      out.write("    <div id=\"contenidoMapa\">\n");
      out.write("        <div id=\"map\"></div>\n");
      out.write("\n");
      out.write("        <div id=\"filtrosMapa\">\n");
      out.write("            <form id=\"formFiltros\">\n");
      out.write("                <h2 class=\"encabezado\">Filtros</h2>\n");
      out.write("                <div class=\"filtro\" id=\"filtroLineas\">\n");
      out.write("                    <div class=\"encabezadoFiltro\">\n");
      out.write("                        <h3 class=\"encabezado\">Lineas del metro</h3>\n");
      out.write("                        <button type=\"button\" class=\"btnMostrarFiltro\" id=\"btnMostrarLinea\">\n");
      out.write("                            <span class=\"material-icons\" id=\"expandirLinea\">\n");
      out.write("                                expand_more\n");
      out.write("                            </span>\n");
      out.write("                            <span class=\"material-icons\" id=\"contraerLinea\">\n");
      out.write("                                expand_less\n");
      out.write("                            </span>\n");
      out.write("                        </button>\n");
      out.write("                    </div><!-- #.encabeadoFiltro -->\n");
      out.write("\n");
      out.write("                    <div id=\"agregarLineas\">\n");
      out.write("                        <p class=\"descripcion\">Lineas de metro a visializar</p>\n");
      out.write("                        <div id=\"OpcionesLineas\">\n");
      out.write("                            <div class=\"opc opcLinea\">\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro1\" value=\"1\">\n");
      out.write("                                <label for=\"Metro1\">Metro Linea 1</label>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <div class=\"opc opcLinea\">\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro2\" value=\"2\">\n");
      out.write("                                <label for=\"Metro2\">Metro Linea 2</label>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <div class=\"opc opcLinea\">\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro3\" value=\"3\">\n");
      out.write("                                <label for=\"Metro3\">Metro Linea 3</label>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <div class=\"opc opcLinea\">\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro4\" value=\"4\">\n");
      out.write("                                <label for=\"Metro4\">Metro Linea 4</label>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <div class=\"opc opcLinea\">\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro5\" value=\"5\">\n");
      out.write("                                <label for=\"Metro5\">Metro Linea 5</label>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <div class=\"opc opcLinea\">\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro6\" value=\"6\">\n");
      out.write("                                <label for=\"Metro6\">Metro Linea 6</label>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <div class=\"opc opcLinea\">\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro7\" value=\"7\">\n");
      out.write("                                <label for=\"Metro7\">Metro Linea 7</label>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <div class=\"opc opcLinea\">\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro8\" value=\"8\">\n");
      out.write("                                <label for=\"Metro8\">Metro Linea 8</label>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <div class=\"opc opcLinea\">\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro9\" value=\"9\">\n");
      out.write("                                <label for=\"Metro9\">Metro Linea 9</label>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <div class=\"opc opcLinea\">\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" id=\"MetroA\" value=\"A\">\n");
      out.write("                                <label for=\"MetroA\">Metro Linea A</label>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <div class=\"opc opcLinea\">\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" id=\"MetroB\" value=\"B\">\n");
      out.write("                                <label for=\"MetroB\">Metro Linea B</label>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <div class=\"opc opcLinea\">\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" id=\"12\" value=\"Metro12\">\n");
      out.write("                                <label for=\"Metro12\">Metro Linea 12</label>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div><!-- .filtro -->\n");
      out.write("\n");
      out.write("                <div class=\"filtro\" id=\"filtroEstaciones\">\n");
      out.write("                    <div class=\"encabezadoFiltro\">\n");
      out.write("                        <h3 class=\"encabezado\">Estaciones del metro</h3>\n");
      out.write("                        <button type=\"button\" class=\"btnMostrarFiltro\" id=\"btnMostrarEst\">\n");
      out.write("                            <span class=\"material-icons\" id=\"expandirEst\">\n");
      out.write("                                expand_more\n");
      out.write("                            </span>\n");
      out.write("                            <span class=\"material-icons\" id=\"contraerEst\">\n");
      out.write("                                expand_less\n");
      out.write("                            </span>\n");
      out.write("                        </button>\n");
      out.write("                    </div><!-- .encabezadoFiltro -->\n");
      out.write("\n");
      out.write("                    <div id=\"agregarEstaciones\">\n");
      out.write("                        <p class=\"descripcion\">Estaciones del metro a visualizar</p>\n");
      out.write("                    </div>\n");
      out.write("                </div><!-- .filtro -->\n");
      out.write("\n");
      out.write("                <div class=\"filtro\" id=\"filtroNumero\">\n");
      out.write("                    <div class=\"encabezadoFiltro\">\n");
      out.write("                        <h3 class=\"encabezado\">Numero de reportes</h3>\n");
      out.write("                        <button type=\"button\" class=\"btnMostrarFiltro\" id=\"btnMostrarNum\">\n");
      out.write("                            <span class=\"material-icons\" id=\"expandirNum\">\n");
      out.write("                                expand_more\n");
      out.write("                            </span>\n");
      out.write("                            <span class=\"material-icons\" id=\"contraerNum\">\n");
      out.write("                                expand_less\n");
      out.write("                            </span>\n");
      out.write("                        </button>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                    <div id=\"agegarNumero\">\n");
      out.write("                        <p class=\"descripcion\">Segun el numero de reportes registrados</p>\n");
      out.write("\n");
      out.write("                        <div id=\"OpcionesNum\">\n");
      out.write("                            <div class=\"opc opcNum\">\n");
      out.write("                                <input type=\"checkbox\" name=\"numIncidencias\" id=\"num0\" value=\"0\">\n");
      out.write("                                <label for=\"num0\">Sin Reportes</label>\n");
      out.write("                            </div><!-- .opc -->\n");
      out.write("\n");
      out.write("                            <div class=\"opc opcNum\">\n");
      out.write("                                <input type=\"checkbox\" name=\"numIncidencias\" id=\"num1\" value=\"1\">\n");
      out.write("                                <label for=\"num1\">1 - 10 Reportes</label>\n");
      out.write("                            </div><!-- .opc -->\n");
      out.write("\n");
      out.write("                            <div class=\"opc opcNum\">\n");
      out.write("                                <input type=\"checkbox\" name=\"numIncidencias\" id=\"num2\" value=\"2\">\n");
      out.write("                                <label for=\"num2\">11 - 25 Reportes</label>\n");
      out.write("                            </div><!-- .opc -->\n");
      out.write("\n");
      out.write("                            <div class=\"opc opcNum\">\n");
      out.write("                                <input type=\"checkbox\" name=\"numIncidencias\" id=\"num3\" value=\"3\">\n");
      out.write("                                <label for=\"num3\">26 - 50 Reportes</label>\n");
      out.write("                            </div><!-- .opc -->\n");
      out.write("\n");
      out.write("                            <div class=\"opc opcNum\">\n");
      out.write("                                <input type=\"checkbox\" name=\"numIncidencias\" id=\"num4\" value=\"4\">\n");
      out.write("                                <label for=\"num4\">51 - 100 Reportes</label>\n");
      out.write("                            </div><!-- .opc -->\n");
      out.write("\n");
      out.write("                            <div class=\"opc opcNum\">\n");
      out.write("                                <input type=\"checkbox\" name=\"numIncidencias\" id=\"num5\" value=\"5\">\n");
      out.write("                                <label for=\"num5\">101 - 200 Reportes</label>\n");
      out.write("                            </div><!-- .opc -->\n");
      out.write("\n");
      out.write("                            <div class=\"opc opcNum\">\n");
      out.write("                                <input type=\"checkbox\" name=\"numIncidencias\" id=\"num6\" value=\"6\">\n");
      out.write("                                <label for=\"num6\">+200 Reportes </label>\n");
      out.write("                            </div><!-- .opc -->\n");
      out.write("\n");
      out.write("                        </div>\n");
      out.write("\n");
      out.write("                    </div>\n");
      out.write("                </div><!-- .filtro -->\n");
      out.write("                <button type=\"submit\" id=\"aplicarFiltros\">Aplicar Filtros</button>\n");
      out.write("                <button type=\"reset\" id=\"btnReset\">Limpiar Filtros</button>\n");
      out.write("\n");
      out.write("            </form>\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("        <div id=\"acotaciones\">\n");
      out.write("            <h2 class=\"encabezado\">Acotaciones</h2>\n");
      out.write("            <ul>\n");
      out.write("                <li>\n");
      out.write("                    <div id=\"color1\"></div>\n");
      out.write("                    <p>Sin Reportes</p>\n");
      out.write("                </li>\n");
      out.write("                <li>\n");
      out.write("                    <div id=\"color2\"></div>\n");
      out.write("                    <p>1 - 10 Reportes</p>\n");
      out.write("                </li>\n");
      out.write("                <li>\n");
      out.write("                    <div id=\"color3\"></div>\n");
      out.write("                    <p>10 - 25 Reportes</p>\n");
      out.write("                </li>\n");
      out.write("                <li>\n");
      out.write("                    <div id=\"color4\"></div>\n");
      out.write("                    <p>25 - 50 Reportes</p>\n");
      out.write("                </li>\n");
      out.write("                <li>\n");
      out.write("                    <div id=\"color5\"></div>\n");
      out.write("                    <p>50 - 100 Reportes</p>\n");
      out.write("                </li>\n");
      out.write("                <li>\n");
      out.write("                    <div id=\"color6\"></div>\n");
      out.write("                    <p>100 - 200 Reportes</p>\n");
      out.write("                </li>\n");
      out.write("                <li>\n");
      out.write("                    <div id=\"color7\"></div>\n");
      out.write("                    <p>+200 Reportes</p>\n");
      out.write("                </li>\n");
      out.write("            </ul>\n");
      out.write("\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("</div>");
    } catch( Throwable t ) {
      if( t instanceof SkipPageException )
          throw (SkipPageException) t;
      if( t instanceof java.io.IOException )
          throw (java.io.IOException) t;
      if( t instanceof IllegalStateException )
          throw (IllegalStateException) t;
      if( t instanceof JspException )
          throw (JspException) t;
      throw new JspException(t);
    } finally {
      ((org.apache.jasper.runtime.JspContextWrapper) jspContext).syncEndTagFile();
    }
  }
}
