package org.apache.jsp.pages.pageEstadisticas;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class estadisticasMapa_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(3);
    _jspx_dependants.add("/WEB-INF/tags/estilosGenerales.tag");
    _jspx_dependants.add("/WEB-INF/tags/Header.tag");
    _jspx_dependants.add("/WEB-INF/tags/Footer.tag");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("\n");
      out.write("    <head>\n");
      out.write("        <meta charset=\"UTF-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("        <title>Estadisticas</title>\n");
      out.write("\n");
      out.write("        <!-- Estilos generales-->\n");
      out.write("        ");
      if (_jspx_meth_t_estilosGenerales_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("            \n");
      out.write("<!--             <link href=\"https://fonts.googleapis.com/css?family=Montserrat:400,500,600|Overpass+Mono:400 600|Poppins:400,600&display=swap\" rel=\"stylesheet\">\n");
      out.write("            <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">\n");
      out.write("            <link href=\"https://fonts.googleapis.com/css?family=Monoton&display=swap\" rel=\"stylesheet\">\n");
      out.write("\n");
      out.write("            <link rel=\"stylesheet\" href=\"../../css/Normalize.css\">\n");
      out.write("            <link rel=\"stylesheet\" href=\"../../css/stylesHeader.css\">\n");
      out.write("            <link rel=\"stylesheet\" href=\"../../css/stylesFooter.css\">\n");
      out.write("            <link rel=\"stylesheet\" href=\"../../interfaz/interface.css\">\n");
      out.write("\n");
      out.write("            <link rel=\"stylesheet\" href=\"../../css/stylesGenerales.css\"> -->\n");
      out.write("\n");
      out.write("            <!-- Estilos Propios-->\n");
      out.write("            <link type=\"text/css\" rel=\"stylesheet\" href=\"../../leaflet/leaflet.css\">\n");
      out.write("            <link type=\"text/css\" rel=\"stylesheet\" href=\"../../leaflet/leaflet.icon-material.css\">\n");
      out.write("\n");
      out.write("            <script src=\"../../leaflet/leaflet.js\"></script>\n");
      out.write("            <script src=\"../../leaflet/leaflet.icon-material.js\"></script>\n");
      out.write("\n");
      out.write("            <script src=\"../../data/estaciones_geoJSON.js\" async></script>\n");
      out.write("            <script src=\"../../data/lineas_geoJSON.js\" async></script>\n");
      out.write("\n");
      out.write("            <link type=\"text/css\" rel=\"stylesheet\" href=\"css/stylesMapa.css\">\n");
      out.write("\n");
      out.write("        </head>\n");
      out.write("\n");
      out.write("        <body>\n");
      out.write("        ");
      if (_jspx_meth_t_Header_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("            <div id=\"contenedorPrincipal\">\n");
      out.write("                <h1 class=\"encabezado\">Mapa del Metro</h1>\n");
      out.write("                <div id=\"contenidoMapa\">\n");
      out.write("                    <div id=\"map\"></div>\n");
      out.write("\n");
      out.write("                    <div id=\"filtrosMapa\">\n");
      out.write("                        <form id=\"formFiltros\">\n");
      out.write("                            <h2 class=\"encabezado\">Filtros</h2>\n");
      out.write("                            <div class=\"filtro\" id=\"filtroLineas\">\n");
      out.write("                                <div class=\"encabezadoFiltro\">\n");
      out.write("                                    <h3 class=\"encabezado\">Lineas del metro</h3>\n");
      out.write("                                    <button type=\"button\" class=\"btnMostrarFiltro\" id=\"btnMostrarLinea\">\n");
      out.write("                                        <span class=\"material-icons\" id=\"expandirLinea\">\n");
      out.write("                                            expand_more\n");
      out.write("                                        </span>\n");
      out.write("                                        <span class=\"material-icons\" id=\"contraerLinea\">\n");
      out.write("                                            expand_less\n");
      out.write("                                        </span>\n");
      out.write("                                    </button>\n");
      out.write("                                </div><!-- #.encabeadoFiltro -->\n");
      out.write("\n");
      out.write("                                <div id=\"agregarLineas\">\n");
      out.write("                                    <p class=\"descripcion\">Lineas de metro a visializar</p>\n");
      out.write("                                    <div id=\"OpcionesLineas\">\n");
      out.write("                                        <div class=\"opc opcLinea\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro1\" value=\"1\">\n");
      out.write("                                            <label for=\"Metro1\">Metro Linea 1</label>\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                        <div class=\"opc opcLinea\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro2\" value=\"2\">\n");
      out.write("                                            <label for=\"Metro2\">Metro Linea 2</label>\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                        <div class=\"opc opcLinea\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro3\" value=\"3\">\n");
      out.write("                                            <label for=\"Metro3\">Metro Linea 3</label>\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                        <div class=\"opc opcLinea\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro4\" value=\"4\">\n");
      out.write("                                            <label for=\"Metro4\">Metro Linea 4</label>\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                        <div class=\"opc opcLinea\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro5\" value=\"5\">\n");
      out.write("                                            <label for=\"Metro5\">Metro Linea 5</label>\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                        <div class=\"opc opcLinea\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro6\" value=\"6\">\n");
      out.write("                                            <label for=\"Metro6\">Metro Linea 6</label>\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                        <div class=\"opc opcLinea\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro7\" value=\"7\">\n");
      out.write("                                            <label for=\"Metro7\">Metro Linea 7</label>\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                        <div class=\"opc opcLinea\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro8\" value=\"8\">\n");
      out.write("                                            <label for=\"Metro8\">Metro Linea 8</label>\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                        <div class=\"opc opcLinea\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"LineaMetro\" id=\"Metro9\" value=\"9\">\n");
      out.write("                                            <label for=\"Metro9\">Metro Linea 9</label>\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                        <div class=\"opc opcLinea\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"LineaMetro\" id=\"MetroA\" value=\"A\">\n");
      out.write("                                            <label for=\"MetroA\">Metro Linea A</label>\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                        <div class=\"opc opcLinea\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"LineaMetro\" id=\"MetroB\" value=\"B\">\n");
      out.write("                                            <label for=\"MetroB\">Metro Linea B</label>\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                        <div class=\"opc opcLinea\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"LineaMetro\" id=\"12\" value=\"Metro12\">\n");
      out.write("                                            <label for=\"Metro12\">Metro Linea 12</label>\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                            </div><!-- .filtro -->\n");
      out.write("\n");
      out.write("                            <div class=\"filtro\" id=\"filtroEstaciones\">\n");
      out.write("                                <div class=\"encabezadoFiltro\">\n");
      out.write("                                    <h3 class=\"encabezado\">Estaciones del metro</h3>\n");
      out.write("                                    <button type=\"button\" class=\"btnMostrarFiltro\" id=\"btnMostrarEst\">\n");
      out.write("                                        <span class=\"material-icons\" id=\"expandirEst\">\n");
      out.write("                                            expand_more\n");
      out.write("                                        </span>\n");
      out.write("                                        <span class=\"material-icons\" id=\"contraerEst\">\n");
      out.write("                                            expand_less\n");
      out.write("                                        </span>\n");
      out.write("                                    </button>\n");
      out.write("                                </div><!-- .encabezadoFiltro -->\n");
      out.write("\n");
      out.write("                                <div id=\"agregarEstaciones\">\n");
      out.write("                                    <p class=\"descripcion\">Estaciones del metro a visualizar</p>\n");
      out.write("                                </div>\n");
      out.write("                            </div><!-- .filtro -->\n");
      out.write("\n");
      out.write("                            <div class=\"filtro\" id=\"filtroNumero\">\n");
      out.write("                                <div class=\"encabezadoFiltro\">\n");
      out.write("                                    <h3 class=\"encabezado\">Numero de reportes</h3>\n");
      out.write("                                    <button type=\"button\" class=\"btnMostrarFiltro\" id=\"btnMostrarNum\">\n");
      out.write("                                        <span class=\"material-icons\" id=\"expandirNum\">\n");
      out.write("                                            expand_more\n");
      out.write("                                        </span>\n");
      out.write("                                        <span class=\"material-icons\" id=\"contraerNum\">\n");
      out.write("                                            expand_less\n");
      out.write("                                        </span>\n");
      out.write("                                    </button>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                                <div id=\"agegarNumero\">\n");
      out.write("                                    <p class=\"descripcion\">Segun el numero de reportes registrados</p>\n");
      out.write("\n");
      out.write("                                    <div id=\"OpcionesNum\">\n");
      out.write("                                        <div class=\"opc opcNum\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"numIncidencias\" id=\"num0\" value=\"0\">\n");
      out.write("                                            <label for=\"num0\">Sin Reportes</label>\n");
      out.write("                                        </div><!-- .opc -->\n");
      out.write("\n");
      out.write("                                        <div class=\"opc opcNum\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"numIncidencias\" id=\"num1\" value=\"1\">\n");
      out.write("                                            <label for=\"num1\">1 - 10 Reportes</label>\n");
      out.write("                                        </div><!-- .opc -->\n");
      out.write("\n");
      out.write("                                        <div class=\"opc opcNum\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"numIncidencias\" id=\"num2\" value=\"2\">\n");
      out.write("                                            <label for=\"num2\">11 - 25 Reportes</label>\n");
      out.write("                                        </div><!-- .opc -->\n");
      out.write("\n");
      out.write("                                        <div class=\"opc opcNum\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"numIncidencias\" id=\"num3\" value=\"3\">\n");
      out.write("                                            <label for=\"num3\">26 - 50 Reportes</label>\n");
      out.write("                                        </div><!-- .opc -->\n");
      out.write("\n");
      out.write("                                        <div class=\"opc opcNum\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"numIncidencias\" id=\"num4\" value=\"4\">\n");
      out.write("                                            <label for=\"num4\">51 - 100 Reportes</label>\n");
      out.write("                                        </div><!-- .opc -->\n");
      out.write("\n");
      out.write("                                        <div class=\"opc opcNum\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"numIncidencias\" id=\"num5\" value=\"5\">\n");
      out.write("                                            <label for=\"num5\">101 - 200 Reportes</label>\n");
      out.write("                                        </div><!-- .opc -->\n");
      out.write("\n");
      out.write("                                        <div class=\"opc opcNum\">\n");
      out.write("                                            <input type=\"checkbox\" name=\"numIncidencias\" id=\"num6\" value=\"6\">\n");
      out.write("                                            <label for=\"num6\">+200 Reportes </label>\n");
      out.write("                                        </div><!-- .opc -->\n");
      out.write("\n");
      out.write("                                    </div>\n");
      out.write("\n");
      out.write("                                </div>\n");
      out.write("                            </div><!-- .filtro -->\n");
      out.write("                            <button type=\"submit\" id=\"aplicarFiltros\">Aplicar Filtros</button>\n");
      out.write("                            <button type=\"reset\" id=\"btnReset\">Limpiar Filtros</button>\n");
      out.write("\n");
      out.write("                        </form>\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                    <div id=\"acotaciones\">\n");
      out.write("                        <h2 class=\"encabezado\">Acotaciones</h2>\n");
      out.write("                        <ul>\n");
      out.write("                            <li>\n");
      out.write("                                <div id=\"color1\"></div>\n");
      out.write("                                <p>Sin Reportes</p>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <div id=\"color2\"></div>\n");
      out.write("                                <p>1 - 10 Reportes</p>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <div id=\"color3\"></div>\n");
      out.write("                                <p>10 - 25 Reportes</p>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <div id=\"color4\"></div>\n");
      out.write("                                <p>25 - 50 Reportes</p>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <div id=\"color5\"></div>\n");
      out.write("                                <p>50 - 100 Reportes</p>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <div id=\"color6\"></div>\n");
      out.write("                                <p>100 - 200 Reportes</p>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <div id=\"color7\"></div>\n");
      out.write("                                <p>+200 Reportes</p>\n");
      out.write("                            </li>\n");
      out.write("                        </ul>\n");
      out.write("\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("        ");
      if (_jspx_meth_t_Footer_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("        <script src=\"../../interfaz/Interface.js\"></script>\n");
      out.write("        <script src=\"../../js/cargarMetro/cargarEstaciones.js\"></script>\n");
      out.write("        <script src=\"../../js/cargarMetro/filtradoEstaciones.js\"></script>\n");
      out.write("        <!-- <script src=\"js/dataPrev.js\"></script> -->\n");
      out.write("\n");
      out.write("        <script src=\"../../data/lineas_geoJSON.js\"></script>\n");
      out.write("        <script src=\"../../data/estaciones_geoJSON.js\"></script>\n");
      out.write("        <script src=\"../../js/mapa/mapa.js\"></script>\n");
      out.write("\n");
      out.write("        <script src=\"js/events/mostrarFiltro.js\"></script>\n");
      out.write("            \n");
      out.write("        <script src=\"js/events/aplicarFiltros.js\"></script>\n");
      out.write("            \n");
      out.write("        <script src=\"js/events/cargaPagina.js\"></script>\n");
      out.write("        \n");
      out.write("    </body>\n");
      out.write("\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_t_estilosGenerales_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:estilosGenerales
    org.apache.jsp.tag.web.estilosGenerales_tag _jspx_th_t_estilosGenerales_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.estilosGenerales_tag.class) : new org.apache.jsp.tag.web.estilosGenerales_tag();
    _jspx_th_t_estilosGenerales_0.setJspContext(_jspx_page_context);
    _jspx_th_t_estilosGenerales_0.doTag();
    return false;
  }

  private boolean _jspx_meth_t_Header_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:Header
    org.apache.jsp.tag.web.Header_tag _jspx_th_t_Header_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.Header_tag.class) : new org.apache.jsp.tag.web.Header_tag();
    _jspx_th_t_Header_0.setJspContext(_jspx_page_context);
    _jspx_th_t_Header_0.setUsername((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.username}", java.lang.String.class, (PageContext)_jspx_page_context, null));
    _jspx_th_t_Header_0.doTag();
    return false;
  }

  private boolean _jspx_meth_t_Footer_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:Footer
    org.apache.jsp.tag.web.Footer_tag _jspx_th_t_Footer_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.Footer_tag.class) : new org.apache.jsp.tag.web.Footer_tag();
    _jspx_th_t_Footer_0.setJspContext(_jspx_page_context);
    _jspx_th_t_Footer_0.doTag();
    return false;
  }
}
