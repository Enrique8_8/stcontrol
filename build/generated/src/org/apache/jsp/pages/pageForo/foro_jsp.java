package org.apache.jsp.pages.pageForo;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class foro_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(3);
    _jspx_dependants.add("/WEB-INF/tags/estilosGenerales.tag");
    _jspx_dependants.add("/WEB-INF/tags/Header.tag");
    _jspx_dependants.add("/WEB-INF/tags/Footer.tag");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"es\">\n");
      out.write("\n");
      out.write("<head>\n");
      out.write("    <meta charset=\"UTF-8\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("    <title>Foro</title>\n");
      out.write("\n");
      out.write("    <!-- Estilos generales-->\n");
      out.write("    ");
      if (_jspx_meth_t_estilosGenerales_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("<!-- Estilos Propios-->\n");
      out.write("   \n");
      out.write("    <link rel=\"stylesheet\" href=\"css/stylesForo.css\">\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("    ");
      if (_jspx_meth_t_Header_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("    <div id=\"contenedor_principal\">\n");
      out.write("\n");
      out.write("        <div id=\"contenedorEntradas\">\n");
      out.write("            <h1>Ultimas entradas</h1>\n");
      out.write("            <div id=\"entradasRegistradas\">\n");
      out.write("                <section class=\"entrada\">\n");
      out.write("                    <p class=\"nick\">Nickname</p>\n");
      out.write("                    <p class=\"time\">Hora: 00:00</p>\n");
      out.write("                    <p class=\"contenido\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit quis\n");
      out.write("                        repellendus debitis quam quasi ex voluptatem ut, aut quo esse. Ad sapiente necessitatibus\n");
      out.write("                        consequatur, quisquam nulla enim? Aut, consequatur soluta!Deleniti recusandae dolor dignissimos\n");
      out.write("                        impedit laboriosam nostrum. Sunt dolores quisquam ex. Veritatis, in, placeat minima possimus\n");
      out.write("                        perspiciatis doloribus perferendis, voluptatum iure iste quas saepe aspernatur eligendi\n");
      out.write("                        laboriosam ad. Explicabo, fuga.</p>\n");
      out.write("\n");
      out.write("                    <p class=\"comentarios\">\n");
      out.write("                        <i class=\"material-icons\"> comment </i>\n");
      out.write("                        <span>Comentarios</span>\n");
      out.write("                        <i class=\"material-icons\"> keyboard_arrow_down </i>\n");
      out.write("                    </p>\n");
      out.write("\n");
      out.write("                    <p class=\"reportar\">\n");
      out.write("                        <i class=\"material-icons\">\n");
      out.write("                            warning\n");
      out.write("                        </i>\n");
      out.write("                        <span>Reportar</span>\n");
      out.write("                    </p>\n");
      out.write("                </section>\n");
      out.write("\n");
      out.write("                <section class=\"entrada\">\n");
      out.write("                    <p class=\"nick\">Nickname</p>\n");
      out.write("                    <p class=\"time\">Hora: 00:00</p>\n");
      out.write("                    <p class=\"contenido\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit quis\n");
      out.write("                        repellendus debitis quam quasi ex voluptatem ut, aut quo esse. Ad sapiente necessitatibus\n");
      out.write("                        consequatur, quisquam nulla enim? Aut, consequatur soluta!Deleniti recusandae dolor dignissimos\n");
      out.write("                        impedit laboriosam nostrum. Sunt dolores quisquam ex. Veritatis, in, placeat minima possimus\n");
      out.write("                        perspiciatis doloribus perferendis, voluptatum iure iste quas saepe aspernatur eligendi\n");
      out.write("                        laboriosam ad. Explicabo, fuga.</p>\n");
      out.write("\n");
      out.write("                    <p class=\"comentarios\">\n");
      out.write("                        <i class=\"material-icons\"> comment </i>\n");
      out.write("                        <span>Comentarios</span>\n");
      out.write("                        <i class=\"material-icons\"> keyboard_arrow_down </i>\n");
      out.write("                    </p>\n");
      out.write("\n");
      out.write("                    <p class=\"reportar\">\n");
      out.write("                        <i class=\"material-icons\">\n");
      out.write("                            warning\n");
      out.write("                        </i>\n");
      out.write("                        <span>Reportar</span>\n");
      out.write("                    </p>\n");
      out.write("                </section>\n");
      out.write("\n");
      out.write("                <section class=\"entrada\">\n");
      out.write("                    <p class=\"nick\">Nickname</p>\n");
      out.write("                    <p class=\"time\">Hora: 00:00</p>\n");
      out.write("                    <p class=\"contenido\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit quis\n");
      out.write("                        repellendus debitis quam quasi ex voluptatem ut, aut quo esse. Ad sapiente necessitatibus\n");
      out.write("                        consequatur, quisquam nulla enim? Aut, consequatur soluta!Deleniti recusandae dolor dignissimos\n");
      out.write("                        impedit laboriosam nostrum. Sunt dolores quisquam ex. Veritatis, in, placeat minima possimus\n");
      out.write("                        perspiciatis doloribus perferendis, voluptatum iure iste quas saepe aspernatur eligendi\n");
      out.write("                        laboriosam ad. Explicabo, fuga.</p>\n");
      out.write("\n");
      out.write("                    <p class=\"comentarios\">\n");
      out.write("                        <i class=\"material-icons\"> comment </i>\n");
      out.write("                        <span>Comentarios</span>\n");
      out.write("                        <i class=\"material-icons\"> keyboard_arrow_down </i>\n");
      out.write("                    </p>\n");
      out.write("\n");
      out.write("                    <p class=\"reportar\">\n");
      out.write("                        <i class=\"material-icons\">\n");
      out.write("                            warning\n");
      out.write("                        </i>\n");
      out.write("                        <span>Reportar</span>\n");
      out.write("                    </p>\n");
      out.write("                </section>\n");
      out.write("\n");
      out.write("                <section class=\"entrada\">\n");
      out.write("                    <p class=\"nick\">Nickname</p>\n");
      out.write("                    <p class=\"time\">Hora: 00:00</p>\n");
      out.write("                    <p class=\"contenido\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit quis\n");
      out.write("                        repellendus debitis quam quasi ex voluptatem ut, aut quo esse. Ad sapiente necessitatibus\n");
      out.write("                        consequatur, quisquam nulla enim? Aut, consequatur soluta!Deleniti recusandae dolor dignissimos\n");
      out.write("                        impedit laboriosam nostrum. Sunt dolores quisquam ex. Veritatis, in, placeat minima possimus\n");
      out.write("                        perspiciatis doloribus perferendis, voluptatum iure iste quas saepe aspernatur eligendi\n");
      out.write("                        laboriosam ad. Explicabo, fuga.</p>\n");
      out.write("\n");
      out.write("                    <p class=\"comentarios\">\n");
      out.write("                        <i class=\"material-icons\"> comment </i>\n");
      out.write("                        <span>Comentarios</span>\n");
      out.write("                        <i class=\"material-icons\"> keyboard_arrow_down </i>\n");
      out.write("                    </p>\n");
      out.write("\n");
      out.write("                    <p class=\"reportar\">\n");
      out.write("                        <i class=\"material-icons\">\n");
      out.write("                            warning\n");
      out.write("                        </i>\n");
      out.write("                        <span>Reportar</span>\n");
      out.write("                    </p>\n");
      out.write("                </section>\n");
      out.write("\n");
      out.write("            </div><!-- #entradasRegistradas -->\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("        <div id=\"contenedorCrear\">\n");
      out.write("            <div id=\"barra\"></div>\n");
      out.write("\n");
      out.write("            <div id=\"crearEntrada\">\n");
      out.write("                <h2>Crear Entrada</h2>\n");
      out.write("\n");
      out.write("                <form id=\"enviarEntrada\" name=\"enviarEntrada\">\n");
      out.write("                    <textarea name=\"ent\" id=\"ent\" cols=\"18\" rows=\"18\"\" placeholder=\"Introduce tu entrada\"></textarea>\n");
      out.write("\n");
      out.write("                    <button type=\"submit\" name=\"btnEnviar\" id=\"btnEnviar\">Enviar</button>\n");
      out.write("                </form>\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("    </div>\n");
      out.write("    \n");
      out.write("    ");
      if (_jspx_meth_t_Footer_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_t_estilosGenerales_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:estilosGenerales
    org.apache.jsp.tag.web.estilosGenerales_tag _jspx_th_t_estilosGenerales_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.estilosGenerales_tag.class) : new org.apache.jsp.tag.web.estilosGenerales_tag();
    _jspx_th_t_estilosGenerales_0.setJspContext(_jspx_page_context);
    _jspx_th_t_estilosGenerales_0.doTag();
    return false;
  }

  private boolean _jspx_meth_t_Header_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:Header
    org.apache.jsp.tag.web.Header_tag _jspx_th_t_Header_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.Header_tag.class) : new org.apache.jsp.tag.web.Header_tag();
    _jspx_th_t_Header_0.setJspContext(_jspx_page_context);
    _jspx_th_t_Header_0.doTag();
    return false;
  }

  private boolean _jspx_meth_t_Footer_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:Footer
    org.apache.jsp.tag.web.Footer_tag _jspx_th_t_Footer_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.Footer_tag.class) : new org.apache.jsp.tag.web.Footer_tag();
    _jspx_th_t_Footer_0.setJspContext(_jspx_page_context);
    _jspx_th_t_Footer_0.doTag();
    return false;
  }
}
