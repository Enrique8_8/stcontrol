package org.apache.jsp.pages.pageIndex;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(4);
    _jspx_dependants.add("/WEB-INF/tags/estilosGenerales.tag");
    _jspx_dependants.add("/WEB-INF/tags/Header.tag");
    _jspx_dependants.add("/WEB-INF/tags/Mapa.tag");
    _jspx_dependants.add("/WEB-INF/tags/Footer.tag");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"es\">\n");
      out.write("<!--\n");
      out.write("Grupo 4IM8\n");
      out.write("Equipo: Deep Space\n");
      out.write("Integrantes: \n");
      out.write("        Acevedo Romo Jimena Olivia\n");
      out.write("        Cervantes Ocaña Jesús Enrique\n");
      out.write("        Gutiérrez Viveros Cristian Rogelio\n");
      out.write("        Mendoza Aguilar Eduardo\n");
      out.write("        Rendón Sierra Carlos Alexis\n");
      out.write("-->\n");
      out.write("\n");
      out.write("\n");
      out.write("<head>\n");
      out.write("    <meta charset=\"UTF-8\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("    <title>STControl</title>\n");
      out.write("\n");
      out.write("    <!-- Estilos generales-->\n");
      out.write("    ");
      if (_jspx_meth_t_estilosGenerales_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    <link href=\"../../css/stylesMapaF.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("    <link type=\"text/css\" rel=\"stylesheet\" href=\"../../leaflet/leaflet.css\">\n");
      out.write("    <link type=\"text/css\" rel=\"stylesheet\" href=\"../../leaflet/leaflet.icon-material.css\">\n");
      out.write("\n");
      out.write("    <script src=\"../../leaflet/leaflet.js\"></script>\n");
      out.write("    <script src=\"../../leaflet/leaflet.icon-material.js\"></script>\n");
      out.write("\n");
      out.write("\n");
      out.write("    <!-- Estilos propios -->\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/styles.css\">\n");
      out.write("    \n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("   \n");
      if (_jspx_meth_t_Header_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    <div id=\"hero\">\n");
      out.write("        <img src=\"assets/metrohero.jpg\" alt=\"Hero\">\n");
      out.write("        <div id=\"textoHero\">\n");
      out.write("            <p>STControl</p>\n");
      out.write("            <p>La seguridad en tu viaje</p>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("    <div id=\"contenedor_principal\">\n");
      out.write("\n");
      out.write("        <section id=\"mapaSeccion\">\n");
      out.write("            ");
      if (_jspx_meth_t_Mapa_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("        </section>\n");
      out.write("\n");
      out.write("        <div id=\"pares\">\n");
      out.write("            <section id=\"funciones\">\n");
      out.write("                <h3 class=\"titulo\">Funcionalidades</h3>\n");
      out.write("                <div id=\"tarjetasFunc\">\n");
      out.write("                    <div class=\"tarjeta\">\n");
      out.write("                        <div class=\"circulo\">\n");
      out.write("                            <h3 class=\"tituloFunc\">Registros</h3>\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"cont\">\n");
      out.write("                            <p>Seccion de reportes, en la que podras ver todos los reportes sobre que se tienen registrados en el sistema, asi como tambien podras crear tus propios reportes, a travez de tener iniciada una cuenta de STControl </p>\n");
      out.write("                            <a href=\"../pageVerReportes/mostrarReportes.jsp\" class=\"btnFunc\">Ver Seccion</a>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"tarjeta\">\n");
      out.write("                        <div class=\"circulo\">\n");
      out.write("                            <h3 class=\"tituloFunc\">Estadisticas</h3>\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"cont\">\n");
      out.write("                            <p>Seccion de estadisticas, en la que podras visualizar un mapa que te permita ver de manera grafica la localizacion de las estaciones del metro, asi como percibir la cantidad de reportes que se tienen registrados de cada zona</p>\n");
      out.write("                            <a href=\"../pageEstadisticas/estadisticasMapa.jsp\" class=\"btnFunc\">Ver Seccion</a>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"tarjeta\">\n");
      out.write("                        <div class=\"circulo\">\n");
      out.write("                            <h3 class=\"tituloFunc\">Foro</h3>\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"cont\">\n");
      out.write("                            <p>Seccion de foro, en la que podras exponer tus dudas acerca del sistema de transporte colectivo metro con los demas usuarios del sistema, interactuando de manera libre con todos los usuarios que hayan creado su cuenta de STControl</p>\n");
      out.write("                            <a href=\"../pageForo/foro.jsp\" class=\"btnFunc\">Ver Seccion</a>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"tarjeta\">\n");
      out.write("                        <div class=\"circulo\">\n");
      out.write("                            <h3 class=\"tituloFunc\">Acerca de</h3>\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"cont\">\n");
      out.write("                            <p>Seccion de acerca de, en la que podras los objetivos del desarrollo del proyecto STControl, asi como tambien encontrar informacion sobre Deep Space, el equipo de desarrollo del proyecto </p>\n");
      out.write("                            <a href=\"../pageAcercaDe/acercaDe.jsp\" class=\"btnFunc\">Ver Seccion</a>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </section>\n");
      out.write("\n");
      out.write("            <section id=\"foroEntradas\">\n");
      out.write("                <h3 class=\"titulo\">Ultimas entradas del foro</h3>\n");
      out.write("\n");
      out.write("                <div id=\"entradasRecientes\"></div>\n");
      out.write("                <a href=\"../Foro/Foro.html\" id=\"btnForoEntradas\">Ver todas</a>\n");
      out.write("\n");
      out.write("            </section>\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("    </div>\n");
      out.write("    \n");
      out.write("    ");
      if (_jspx_meth_t_Footer_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("    <script src=\"../../interfaz/Interface.js\"></script>\n");
      out.write("    <script src=\"../../js/cargarMetro/cargarEstaciones.js\"></script>\n");
      out.write("    <script src=\"../../js/cargarMetro/filtradoEstaciones.js\"></script>\n");
      out.write("    <script src=\"../../js/peticiones/peticion.js\"></script>\n");
      out.write("\n");
      out.write("    <script src=\"../../data/lineas_geoJSON.js\"></script>\n");
      out.write("    <script src=\"../../data/estaciones_geoJSON.js\"></script>\n");
      out.write("\n");
      out.write("    <script src=\"../../js/mapa/mapa.js\"></script>\n");
      out.write("    <script src=\"../../js/mapa/mostrarFiltro.js\"></script>\n");
      out.write("    <script src=\"../../js/mapa/aplicarFiltros.js\"></script>\n");
      out.write("    <script src=\"../../js/mapa/cargarMapa.js\"></script>\n");
      out.write("    <script src=\"js/templateEntrada.js\" type=\"text/javascript\"></script>\n");
      out.write("    <script src=\"js/events/cargaIndex.js\" type=\"text/javascript\"></script>\n");
      out.write("</body>\n");
      out.write("\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_t_estilosGenerales_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:estilosGenerales
    org.apache.jsp.tag.web.estilosGenerales_tag _jspx_th_t_estilosGenerales_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.estilosGenerales_tag.class) : new org.apache.jsp.tag.web.estilosGenerales_tag();
    _jspx_th_t_estilosGenerales_0.setJspContext(_jspx_page_context);
    _jspx_th_t_estilosGenerales_0.doTag();
    return false;
  }

  private boolean _jspx_meth_t_Header_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:Header
    org.apache.jsp.tag.web.Header_tag _jspx_th_t_Header_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.Header_tag.class) : new org.apache.jsp.tag.web.Header_tag();
    _jspx_th_t_Header_0.setJspContext(_jspx_page_context);
    _jspx_th_t_Header_0.setUsername((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.username}", java.lang.String.class, (PageContext)_jspx_page_context, null));
    _jspx_th_t_Header_0.doTag();
    return false;
  }

  private boolean _jspx_meth_t_Mapa_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:Mapa
    org.apache.jsp.tag.web.Mapa_tag _jspx_th_t_Mapa_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.Mapa_tag.class) : new org.apache.jsp.tag.web.Mapa_tag();
    _jspx_th_t_Mapa_0.setJspContext(_jspx_page_context);
    _jspx_th_t_Mapa_0.doTag();
    return false;
  }

  private boolean _jspx_meth_t_Footer_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:Footer
    org.apache.jsp.tag.web.Footer_tag _jspx_th_t_Footer_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.Footer_tag.class) : new org.apache.jsp.tag.web.Footer_tag();
    _jspx_th_t_Footer_0.setJspContext(_jspx_page_context);
    _jspx_th_t_Footer_0.doTag();
    return false;
  }
}
