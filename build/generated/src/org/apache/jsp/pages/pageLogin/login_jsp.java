package org.apache.jsp.pages.pageLogin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(3);
    _jspx_dependants.add("/WEB-INF/tags/estilosGenerales.tag");
    _jspx_dependants.add("/WEB-INF/tags/Header.tag");
    _jspx_dependants.add("/WEB-INF/tags/Footer.tag");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"es\">\n");
      out.write("\n");
      out.write("<head>\n");
      out.write("    <meta charset=\"UTF-8\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("    <title>Login</title>\n");
      out.write("\n");
      out.write("    ");
      if (_jspx_meth_t_estilosGenerales_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    \n");
      out.write("    <link rel=\"stylesheet\" href=\"stylesLogin.css\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"../../interfaz/interface.css\">\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("    ");
      if (_jspx_meth_t_Header_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    \n");
      out.write("    <div id=\"contenedorPrincipal\">\n");
      out.write("        <main class=\"centrado\">\n");
      out.write("            <section id=\"beneficios\" class=\"centrado-vertical\">\n");
      out.write("                <h1 class=\"titulo\">Con tu cuenta de STControl puedes: </h1>\n");
      out.write("                <ul class=\"centrado-vertical\">\n");
      out.write("                    <li>Crear reportes sobre incidentes ocurridos en el Metro</li>\n");
      out.write("                    <li>Realizar un seguimiento a los incidentes que desees</li>\n");
      out.write("                    <li>Interactuar con la comunidad de usuarios</li>\n");
      out.write("                    <li>Poder visualizar y modificar tus reportes creados</li>\n");
      out.write("                </ul>\n");
      out.write("            </section>\n");
      out.write("\n");
      out.write("            <section id=\"login\">\n");
      out.write("\n");
      out.write("                <div id=\"superiorLogin\" class=\"centrado\">\n");
      out.write("\n");
      out.write("                    <ul>\n");
      out.write("                        <li id=\"crear\"><a href=\"#\">Crear Cuenta</a></li>\n");
      out.write("                        <li id=\"iniciar\"><a href=\"#\">Iniciar Sesión</a></li>\n");
      out.write("                    </ul>\n");
      out.write("\n");
      out.write("                </div><!-- #superiorLogin -->\n");
      out.write("\n");
      out.write("                <div class=\"medio\">\n");
      out.write("                    <img src=\"../../assets/Guy.png\" alt=\"Usuario Anonimo\"/>\n");
      out.write("                    <h2 class=\"titulo\" id=\"textoDesc\">Crea tu cuenta de STControl</h2>\n");
      out.write("                </div><!-- .zonaMedia -->\n");
      out.write("\n");
      out.write("                <div id=\"alertaError\"></div><!-- #alertaError -->\n");
      out.write("\n");
      out.write("                <div id=\"formularioRelleno\">\n");
      out.write("                    <div id=\"formCrearCuenta\">\n");
      out.write("                        <form class=\"login\" name=\"crearCuenta\" id=\"crearCuenta\" method=\"post\" action=\"../../CrearCuentaC\">\n");
      out.write("\n");
      out.write("                            <div class=\"text-control\">\n");
      out.write("                                <h3 class=\"titulo\">Ingresa los siguientes datos:</h3>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <div class=\"data-entry\">\n");
      out.write("                                <div class=\"campoLogin centrado\" id=\"data1\">\n");
      out.write("                                    <label for=\"username\">Nombre de usuario:</label>\n");
      out.write("                                    <input type=\"text\" name=\"username\" id=\"username\" required minlength=\"1\"\n");
      out.write("                                        maxlength=\"20\" placeholder=\"Nombre de usuario\" pattern=\"[-a-zA-Z0-9~!$%^&*_=+,ñ. }{\\\\?]{0,20}\"/>\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                                <div class=\"campoLogin centrado\" id=\"data2\">\n");
      out.write("                                    <label for=\"email\">Correo electronico:</label>\n");
      out.write("                                    <input type=\"email\" name=\"email\" id=\"email\" required\n");
      out.write("                                        placeholder=\"ejemplo@correo.com\" minlength=\"7\" maxlength=\"30\" pattern=\"(?i)^[\\\\w!#$%&’*+/=?`{|}~^-]+(?:\\\\.[\\\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\\\.)+[a-zA-Z]{2,6}$\"/>\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                                <div class=\"campoLogin centrado\" id=\"data3\">\n");
      out.write("                                    <label for=\"user-password\">Contraseña:</label>\n");
      out.write("                                    <input type=\"password\" name=\"user-password\" id=\"user-password\" required\n");
      out.write("                                        minlength=\"1\" maxlength=\"20\" placeholder=\"Contraseña\" pattern=\"[-a-zA-Z0-9~!$%^&*_=+,ñ. }{\\\\?]{0,20}\"/>\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                                <div class=\"campoLogin centrado\" id=\"data4\">\n");
      out.write("                                    <label for=\"vpassword\">Confirmacion de contraseña:</label>\n");
      out.write("                                    <input type=\"password\" name=\"vpassword\" id=\"vpassword\" required minlength=\"1\"\n");
      out.write("                                        maxlength=\"20\" placeholder=\"Contraseña\" pattern=\"[-a-zA-Z0-9~!$%^&*_=+,ñ. }{\\\\?]{0,20}\">\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                                <div class=\"campoLogin centrado\" id=\"data5\">\n");
      out.write("                                    <label for=\"linea\">Linea de metro que desees seguir:\n");
      out.write("                                    </label>\n");
      out.write("                                    <select id=\"linea\" name=\"linea\" required>\n");
      out.write("                                        <option selected disabled>Selecciona una opción</option>\n");
      out.write("                                        <option value=\"Metro 1\">Línea 1</option>\n");
      out.write("                                        <option value=\"Metro 2\">Línea 2</option>\n");
      out.write("                                        <option value=\"Metro 3\">Lí­nea 3</option>\n");
      out.write("                                        <option value=\"Metro 4\">Línea 4</option>\n");
      out.write("                                        <option value=\"Metro 5\">Línea 5</option>\n");
      out.write("                                        <option value=\"Metro 6\">Línea 6</option>\n");
      out.write("                                        <option value=\"Metro 7\">Línea 7</option>\n");
      out.write("                                        <option value=\"Metro 8\">Línea 8</option>\n");
      out.write("                                        <option value=\"Metro 9\">Línea 9</option>\n");
      out.write("                                        <option value=\"Metro A\">Línea A</option>\n");
      out.write("                                        <option value=\"Metro B\">Línea B</option>\n");
      out.write("                                        <option value=\"Metro 12\">Línea 12</option>\n");
      out.write("                                    </select>\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                                <button type=\"submit\" form=\"crearCuenta\" value=\"Ingresar\"\n");
      out.write("                                    class=\"btnSubmit\">Ingresar</button>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                        </form>\n");
      out.write("                    </div><!-- #formCrearCuenta --> \n");
      out.write("                </div><!-- .formularioRelleno -->\n");
      out.write("\n");
      out.write("            </section>\n");
      out.write("            <script src=\"js/detectadoParametros.js\"></script>\n");
      out.write("            <script src=\"js/templatesLogin.js\"></script>\n");
      out.write("            <script src=\"../../interfaz/Interface.js\"></script>\n");
      out.write("            <script src=\"js/login.js\"></script>\n");
      out.write("            <script src=\"js/events/cargaInicial.js\"></script>\n");
      out.write("        </main>\n");
      out.write("    </div>\n");
      out.write("    \n");
      out.write("    ");
      if (_jspx_meth_t_Footer_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    \n");
      out.write("</body>\n");
      out.write("\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_t_estilosGenerales_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:estilosGenerales
    org.apache.jsp.tag.web.estilosGenerales_tag _jspx_th_t_estilosGenerales_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.estilosGenerales_tag.class) : new org.apache.jsp.tag.web.estilosGenerales_tag();
    _jspx_th_t_estilosGenerales_0.setJspContext(_jspx_page_context);
    _jspx_th_t_estilosGenerales_0.doTag();
    return false;
  }

  private boolean _jspx_meth_t_Header_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:Header
    org.apache.jsp.tag.web.Header_tag _jspx_th_t_Header_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.Header_tag.class) : new org.apache.jsp.tag.web.Header_tag();
    _jspx_th_t_Header_0.setJspContext(_jspx_page_context);
    _jspx_th_t_Header_0.setUsername((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.username}", java.lang.String.class, (PageContext)_jspx_page_context, null));
    _jspx_th_t_Header_0.doTag();
    return false;
  }

  private boolean _jspx_meth_t_Footer_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:Footer
    org.apache.jsp.tag.web.Footer_tag _jspx_th_t_Footer_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.Footer_tag.class) : new org.apache.jsp.tag.web.Footer_tag();
    _jspx_th_t_Footer_0.setJspContext(_jspx_page_context);
    _jspx_th_t_Footer_0.doTag();
    return false;
  }
}
