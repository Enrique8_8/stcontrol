package org.apache.jsp.pages.pageVerReportes;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class mostrarReportes_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(3);
    _jspx_dependants.add("/WEB-INF/tags/estilosGenerales.tag");
    _jspx_dependants.add("/WEB-INF/tags/Header.tag");
    _jspx_dependants.add("/WEB-INF/tags/Footer.tag");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"es\">\n");
      out.write("\n");
      out.write("<head>\n");
      out.write("    <title>Ver Reportes</title>\n");
      out.write("\n");
      out.write("    <meta charset=\"UTF-8\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("\n");
      out.write("    <!-- Estilos generales-->\n");
      out.write("    ");
      if (_jspx_meth_t_estilosGenerales_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("    <!-- Estilos Propios-->\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/stylesMostrar.css\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/stylesFiltros.css\">\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("    ");
      if (_jspx_meth_t_Header_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    \n");
      out.write("    <div id=\"contenedorPrincipal\">\n");
      out.write("        <main>\n");
      out.write("            <h1 class=\"titulo\">Reportes Previos</h1>\n");
      out.write("            <section id=\"filtros\">\n");
      out.write("                <!-- Los filtros que se van a aplicar, son: \n");
      out.write("                    por linea del metro\n");
      out.write("                    por estacion del metro\n");
      out.write("                    por fecha\n");
      out.write("\n");
      out.write("                -->\n");
      out.write("                <form action=\"\">\n");
      out.write("                    <div id=\"filtrosLineas\" class=\"contFiltro\">\n");
      out.write("                        <h2 class=\"tituloFiltro\">Selecciona las lineas del metro que quieras ver</h2>\n");
      out.write("                        <div id=\"contFiltrosLineas\">\n");
      out.write("                            <div class=\"checkLinea\">\n");
      out.write("                                <label for=\"Metro1\">Linea 1</label>\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" class=\"checkBox\" id=\"Metro1\" value=\"Metro1\">\n");
      out.write("                            </div><!-- .checkLinea -->\n");
      out.write("\n");
      out.write("                            <div class=\"checkLinea\">\n");
      out.write("                                <label for=\"Metro2\">Linea 2</label>\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" class=\"checkBox\" id=\"Metro2\" value=\"Metro2\">\n");
      out.write("                            </div><!-- .checkLinea -->\n");
      out.write("\n");
      out.write("                            <div class=\"checkLinea\">\n");
      out.write("                                <label for=\"Metro3\">Linea 3</label>\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" class=\"checkBox\" id=\"Metro3\" value=\"Metro3\">\n");
      out.write("                            </div><!-- .checkLinea -->\n");
      out.write("\n");
      out.write("                            <div class=\"checkLinea\">\n");
      out.write("                                <label for=\"Metro4\">Linea 4</label>\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" class=\"checkBox\" id=\"Metro4\" value=\"Metro4\">\n");
      out.write("                            </div><!-- .checkLinea -->\n");
      out.write("\n");
      out.write("                            <div class=\"checkLinea\">\n");
      out.write("                                <label for=\"Metro5\">Linea 5</label>\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" class=\"checkBox\" id=\"Metro5\" value=\"Metro5\">\n");
      out.write("                            </div><!-- .checkLinea -->\n");
      out.write("\n");
      out.write("                            <div class=\"checkLinea\">\n");
      out.write("                                <label for=\"Metro6\">Linea 6</label>\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" class=\"checkBox\" id=\"Metro6\" value=\"Metro6\">\n");
      out.write("                            </div><!-- .checkLinea -->\n");
      out.write("\n");
      out.write("                            <div class=\"checkLinea\">\n");
      out.write("                                <label for=\"Metro7\">Linea 7</label>\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" class=\"checkBox\" id=\"Metro7\" value=\"Metro7\">\n");
      out.write("                            </div><!-- .checkLinea -->\n");
      out.write("\n");
      out.write("                            <div class=\"checkLinea\">\n");
      out.write("                                <label for=\"Metro8\">Linea 8</label>\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" class=\"checkBox\" id=\"Metro8\" value=\"Metro8\">\n");
      out.write("                            </div><!-- .checkLinea -->\n");
      out.write("\n");
      out.write("                            <div class=\"checkLinea\">\n");
      out.write("                                <label for=\"Metro9\">Linea 9</label>\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" class=\"checkBox\" id=\"Metro9\" value=\"Metro9\">\n");
      out.write("                            </div><!-- .checkLinea -->\n");
      out.write("\n");
      out.write("                            <div class=\"checkLinea\">\n");
      out.write("                                <label for=\"MetroA\">Linea A</label>\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" class=\"checkBox\" id=\"MetroA\" value=\"MetroA\">\n");
      out.write("                            </div><!-- .checkLinea -->\n");
      out.write("\n");
      out.write("                            <div class=\"checkLinea\">\n");
      out.write("                                <label for=\"MetroB\">Linea B</label>\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" class=\"checkBox\" id=\"MetroB\" value=\"MetroB\">\n");
      out.write("                            </div><!-- .checkLinea -->\n");
      out.write("\n");
      out.write("                            <div class=\"checkLinea\">\n");
      out.write("                                <label for=\"Metro12\">Linea 12</label>\n");
      out.write("                                <input type=\"checkbox\" name=\"LineaMetro\" class=\"checkBox\" id=\"Metro12\" value=\"Metro12\">\n");
      out.write("                            </div><!-- .checkLinea -->\n");
      out.write("                        </div><!-- #contLineasFiltros -->\n");
      out.write("\n");
      out.write("                    </div><!-- #filtrosLineas -->\n");
      out.write("\n");
      out.write("                    <div id=\"filtroEstaciones\" class=\"contFiltro\">\n");
      out.write("                        <h2 class=\"tituloFiltro\">Selecciona las estaciones que quieras ver</h2>\n");
      out.write("                        <div id=\"contenedorEstaciones\"></div>\n");
      out.write("                    </div><!-- #filtrosEstaciones -->\n");
      out.write("\n");
      out.write("                    <div id=\"fechas\" class=\"contFiltro\">\n");
      out.write("                        <h2 class=\"tituloFiltro\">Filtrar por fecha</h2>\n");
      out.write("                        <div id=\"fecha1\" class=\"centrar\">\n");
      out.write("                            <label for=\"fechaInicio\">De: </label>\n");
      out.write("                            <input type=\"date\" name=\"fechaInicio\" id=\"fechaInicio\">\n");
      out.write("                        </div><!-- fecha1 -->\n");
      out.write("\n");
      out.write("                        <div id=\"fecha2\" class=\"centrar\">\n");
      out.write("                            <label for=\"#fechaFinal\">a: </label>\n");
      out.write("                            <input type=\"date\" name=\"fechaFinal\" id=\"fechaFinal\">\n");
      out.write("                        </div><!-- #fecha2 -->\n");
      out.write("                    </div><!-- #fechas -->\n");
      out.write("\n");
      out.write("                    <div id=\"botonesFiltros\" class=\"contFiltro centrar\">\n");
      out.write("                        <button type=\"submit\">Aplicar filtros</button>\n");
      out.write("                        <button type=\"reset\">Limpiar Filtros</button>\n");
      out.write("                    </div>\n");
      out.write("                </form>\n");
      out.write("            </section>\n");
      out.write("            <section id=\"contenedorResultados\">\n");
      out.write("                <div class=\"registro\" id=\"idRegistro\">\n");
      out.write("                    <div class=\"datosLinea\">\n");
      out.write("                        <div class=\"encabezados\">\n");
      out.write("                            <h2 class=\"datoLinea titulo\">Linea del metro</h2>\n");
      out.write("                            <h2 class=\"datoEst titulo\">Nombre de la estacion</h2>\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"barra\"></div>\n");
      out.write("                    </div><!-- .datosLinea -->\n");
      out.write("\n");
      out.write("                    <div class=\"cuerpo\">\n");
      out.write("                        <div class=\"textCentral\">\n");
      out.write("                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum ad nisi minima quidem,\n");
      out.write("                                culpa fugiat labore? Id officiis magni aut nostrum optio ducimus adipisci nobis ut,\n");
      out.write("                                earum, atque, qui perspiciatis!Illum rerum odit neque culpa, eos tenetur saepe et ad\n");
      out.write("                                fuga libero minima quos, sunt, reprehenderit corporis aliquid. </p>\n");
      out.write("                        </div><!-- .textCentral -->\n");
      out.write("\n");
      out.write("                        <div class=\"textDerecha\">\n");
      out.write("                            <p>Tipo de incidencial</p>\n");
      out.write("                            <p>Dia: 00-00-00</p>\n");
      out.write("                            <p>Hora: 00:00</p>\n");
      out.write("                        </div><!-- .textDerecha -->\n");
      out.write("\n");
      out.write("                        <div class=\"barraInferior centrar\">\n");
      out.write("                            <div class=\"seguir centrar\">\n");
      out.write("                                <button class=\"btnSeguir\">\n");
      out.write("                                    <span class=\"material-icons btnLlena\">\n");
      out.write("                                        star\n");
      out.write("                                    </span>\n");
      out.write("                                    <span class=\"material-icons btnVacia\">\n");
      out.write("                                        star_border\n");
      out.write("                                    </span>\n");
      out.write("                                </button>\n");
      out.write("                                <p class=\"txtSeguir\">Seguir</p>\n");
      out.write("                            </div><!-- .seguir -->\n");
      out.write("\n");
      out.write("                            <div class=\"comentarios centrar\">\n");
      out.write("                                <span class=\"material-icons\">\n");
      out.write("                                    comment\n");
      out.write("                                </span>\n");
      out.write("                                <p>Comentarios</p>\n");
      out.write("                                <button class=\"btnComentarios\">\n");
      out.write("                                    <span class=\"material-icons showComment\">\n");
      out.write("                                        keyboard_arrow_down\n");
      out.write("                                    </span>\n");
      out.write("                                    <span class=\"material-icons hideComment\">\n");
      out.write("                                        keyboard_arrow_up\n");
      out.write("                                    </span>\n");
      out.write("                                </button>\n");
      out.write("                            </div><!-- .comentarios -->\n");
      out.write("\n");
      out.write("                            <div class=\"reportar centrar\">\n");
      out.write("                                <button class=\"btnReportar\">\n");
      out.write("                                    <span class=\"material-icons\">\n");
      out.write("                                        warning\n");
      out.write("                                    </span>\n");
      out.write("                                </button>\n");
      out.write("                                <p>Reportar</p>\n");
      out.write("                            </div><!-- .reportar -->\n");
      out.write("\n");
      out.write("                        </div><!-- .barraInferior -->\n");
      out.write("\n");
      out.write("                    </div><!-- .cuerpo -->\n");
      out.write("\n");
      out.write("                    <div class=\"lateral\">\n");
      out.write("                        <div class=\"circulo\"></div>\n");
      out.write("                        <div class=\"circulo\"></div>\n");
      out.write("                        <div class=\"circulo\"></div>\n");
      out.write("                        <div class=\"circulo\"></div>\n");
      out.write("                        <div class=\"circulo\"></div>\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("            </section>\n");
      out.write("        </main>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("    ");
      if (_jspx_meth_t_Footer_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("</body>\n");
      out.write("\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_t_estilosGenerales_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:estilosGenerales
    org.apache.jsp.tag.web.estilosGenerales_tag _jspx_th_t_estilosGenerales_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.estilosGenerales_tag.class) : new org.apache.jsp.tag.web.estilosGenerales_tag();
    _jspx_th_t_estilosGenerales_0.setJspContext(_jspx_page_context);
    _jspx_th_t_estilosGenerales_0.doTag();
    return false;
  }

  private boolean _jspx_meth_t_Header_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:Header
    org.apache.jsp.tag.web.Header_tag _jspx_th_t_Header_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.Header_tag.class) : new org.apache.jsp.tag.web.Header_tag();
    _jspx_th_t_Header_0.setJspContext(_jspx_page_context);
    _jspx_th_t_Header_0.doTag();
    return false;
  }

  private boolean _jspx_meth_t_Footer_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:Footer
    org.apache.jsp.tag.web.Footer_tag _jspx_th_t_Footer_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.Footer_tag.class) : new org.apache.jsp.tag.web.Footer_tag();
    _jspx_th_t_Footer_0.setJspContext(_jspx_page_context);
    _jspx_th_t_Footer_0.doTag();
    return false;
  }
}
