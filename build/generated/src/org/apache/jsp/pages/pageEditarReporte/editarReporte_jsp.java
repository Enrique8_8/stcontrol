package org.apache.jsp.pages.pageEditarReporte;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class editarReporte_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(3);
    _jspx_dependants.add("/WEB-INF/tags/estilosGenerales.tag");
    _jspx_dependants.add("/WEB-INF/tags/Header.tag");
    _jspx_dependants.add("/WEB-INF/tags/Footer.tag");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"es\">\n");
      out.write("<head>\n");
      out.write("    <meta charset=\"UTF-8\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("    <title>Editar Entrada</title>\n");
      out.write("    <!-- Estilos generales -->\n");
      out.write("    <!-- <link href=\"https://fonts.googleapis.com/css?family=Montserrat:400,600|Overpass+Mono:400,600|Poppins:400,600&display=swap\" rel=\"stylesheet\">\n");
      out.write("    <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">\n");
      out.write("    <link href=\"https://fonts.googleapis.com/css?family=Monoton&display=swap\" rel=\"stylesheet\">\n");
      out.write("\n");
      out.write("    <link rel=\"stylesheet\" href=\"../../css/Normalize.css\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"../../css/stylesHeader.css\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"../../css/stylesFooter.css\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"../../interfaz/interface.css\">\n");
      out.write("\n");
      out.write("    <link rel=\"stylesheet\" href=\"../../css/stylesGenerales.css\"> -->\n");
      out.write("    ");
      if (_jspx_meth_t_estilosGenerales_0(_jspx_page_context))
        return;
      out.write(" \n");
      out.write("   \n");
      out.write("        <!-- Estilo propio -->\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/stylesEditarReporte.css\">\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("    ");
      if (_jspx_meth_t_Header_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    <div id=\"contenedorPrincipal\">\n");
      out.write("        <form action=\"/STControl/CrearRegistro\" method=\"post\">\n");
      out.write("            <div id=\"contenedorForm\">\n");
      out.write("\n");
      out.write("                <div class=\"conjuntoDatos\">\n");
      out.write("                    <legend>Estación</legend>\n");
      out.write("                    <div id=\"contData1\">\n");
      out.write("                        <div class=\"camposEst\" id=\"contLineas\">\n");
      out.write("                            <label for=\"linea\">Selecciona la linea</label>\n");
      out.write("                            <select id=\"linea\" name=\"linea\" required>\n");
      out.write("                                <option value=\"0\" selected>Selecciona una opción</option>\n");
      out.write("                                <option value=\"Metro 1\">Línea 1</option>\n");
      out.write("                                <option value=\"Metro 2\">Línea 2</option>\n");
      out.write("                                <option value=\"Metro 3\">Línea 3</option>\n");
      out.write("                                <option value=\"Metro 4\">Línea 4</option>    \n");
      out.write("                                <option value=\"Metro 5\">Línea 5</option>\n");
      out.write("                                <option value=\"Metro 6\">Línea 6</option>\n");
      out.write("                                <option value=\"Metro 7\">Línea 7</option>\n");
      out.write("                                <option value=\"Metro 8\">Línea 8</option>\n");
      out.write("                                <option value=\"Metro 9\">Línea 9</option>\n");
      out.write("                                <option value=\"Metro A\">Línea A</option>\n");
      out.write("                                <option value=\"Metro B\">Línea B</option>\n");
      out.write("                                <option value=\"Metro 12\">Línea 12</option>\n");
      out.write("                            </select>\n");
      out.write("                        </div><!-- #contLineas -->\n");
      out.write("\n");
      out.write("                        <div class=\"camposEst\" id=\"contEstaciones\">\n");
      out.write("                            <label for=\"estaciones\">Selecciona la estacion</label>\n");
      out.write("                            <select id=\"estaciones\" name=\"estaciones\" required>\n");
      out.write("                                <option value=\"0\" selected>Selecciona una estación</option>\n");
      out.write("                            </select>\n");
      out.write("                        </div><!-- #contEstaciones -->\n");
      out.write("                    </div><!-- #contData1 -->\n");
      out.write("                </div><!-- .conjuntoDatos Linea-->\n");
      out.write("\n");
      out.write("                <div class=\"conjuntoDatos\">\n");
      out.write("                    <legend>Fecha</legend>\n");
      out.write("                    <div id=\"contData2\">\n");
      out.write("\n");
      out.write("                        <div id=\"contDia\">\n");
      out.write("                            <label for=\"dia\">Dia: </label>\n");
      out.write("                            <input type=\"date\" name=\"dia\" id=\"dia\" required>\n");
      out.write("                        </div><!-- #contDia -->\n");
      out.write("\n");
      out.write("                        <div id=\"contHora\">\n");
      out.write("                            <label for=\"hora\">Hora: </label>\n");
      out.write("                            <input type=\"time\" name=\"hora\" id=\"hora\" required>\n");
      out.write("                        </div><!-- #contHora-->\n");
      out.write("                    </div><!-- #contData2-->\n");
      out.write("                </div><!-- .conjuntoDatos Fecha / Hora-->\n");
      out.write("\n");
      out.write("                <div class=\"conjuntoDatos\">\n");
      out.write("                    <legend>Descripción de lo ocurrido</legend>\n");
      out.write("\n");
      out.write("                    <div id=\"contData3\">\n");
      out.write("                        <textarea name=\"desc\" id=\"desc\" cols=\"50\" rows=\"15\" minlenght=\"0\" maxlenght=\"50\" required></textarea>\n");
      out.write("\n");
      out.write("                        <div id=\"pregunta\">\n");
      out.write("                            <label for=\"tipo\">¿Que tipo de incidente fue?</label>\n");
      out.write("                            <select name=\"tipo\" id=\"tipo\" required>\n");
      out.write("                                <option selected disabled>Selecciona una opcion</option>\n");
      out.write("                                <option value=\"asalto\">Asalto</option>\n");
      out.write("                                <option value=\"robo\">Robo</option>\n");
      out.write("                                <option value=\"acoso\">Acoso</option>\n");
      out.write("                                <option value=\"pelea\">Pelea</option>\n");
      out.write("                                <option value=\"gritos\">Gritos</option>\n");
      out.write("                                <option value=\"otros\">Otros</option>\n");
      out.write("                            </select>\n");
      out.write("                        </div>\n");
      out.write("                                \n");
      out.write("                    </div><!-- #contData3-->\n");
      out.write("                </div><!-- .conjuntoDatos -->\n");
      out.write("\n");
      out.write("                <div id=\"contBtn\">\n");
      out.write("                    <button class=\"btn_form\" type=\"submit\" value=\"Enviar\" name=\"btn_enviar\" id=\"btnSubmit\">Aceptar</button>\n");
      out.write("                    <button class=\"btn_form\" type=\"reset\" value=\"Limpiar\" name=\"btn_limpiar\">Limpiar</button>\n");
      out.write("                </div><!-- #contBtn -->\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("        </form>\n");
      out.write("    </div>\n");
      out.write("    ");
      if (_jspx_meth_t_Footer_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("    <script src=\"../../data/estaciones_geoJSON.js\"></script>\n");
      out.write("    <script src=\"../../js/cargarMetro/cargarEstaciones.js\"></script>\n");
      out.write("    <script src=\"../../js/cargarMetro/estacionesSelect/seleccionMetro.js\"></script>\n");
      out.write("    <script src=\"../../interfaz/Interface.js\"></script>\n");
      out.write("\n");
      out.write("    <script src=\"js/events/cargaInicial.js\"></script>\n");
      out.write("    <script src=\"../../js/peticiones/peticion.js\"></script>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_t_estilosGenerales_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:estilosGenerales
    org.apache.jsp.tag.web.estilosGenerales_tag _jspx_th_t_estilosGenerales_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.estilosGenerales_tag.class) : new org.apache.jsp.tag.web.estilosGenerales_tag();
    _jspx_th_t_estilosGenerales_0.setJspContext(_jspx_page_context);
    _jspx_th_t_estilosGenerales_0.doTag();
    return false;
  }

  private boolean _jspx_meth_t_Header_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:Header
    org.apache.jsp.tag.web.Header_tag _jspx_th_t_Header_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.Header_tag.class) : new org.apache.jsp.tag.web.Header_tag();
    _jspx_th_t_Header_0.setJspContext(_jspx_page_context);
    _jspx_th_t_Header_0.doTag();
    return false;
  }

  private boolean _jspx_meth_t_Footer_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  t:Footer
    org.apache.jsp.tag.web.Footer_tag _jspx_th_t_Footer_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(org.apache.jsp.tag.web.Footer_tag.class) : new org.apache.jsp.tag.web.Footer_tag();
    _jspx_th_t_Footer_0.setJspContext(_jspx_page_context);
    _jspx_th_t_Footer_0.doTag();
    return false;
  }
}
