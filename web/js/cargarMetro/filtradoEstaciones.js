function filtradoEstaciones(arrayEstaciones){
    let resultado = {};
    let existentes = {};

    for(let elem of arrayEstaciones){
        if(!(elem.estacion in existentes)){
            resultado[elem.estacion] = {
                incidencias: elem.incidencias,
                lineas: [elem.linea]
            };
            existentes[elem.estacion] = 1;
        }else{
            resultado[elem.estacion].incidencias += elem.incidencias;
            resultado[elem.estacion].lineas.push(elem.linea);
        }
        
    }
    return resultado;
}

function filtradoEstacionesGeo(estaciones, estInc){
    let resultado = [];
    let existentes = {};
    for(let elem of estaciones){
        if(!(elem[0] in existentes)){
            let incidencias = estInc[elem[0]].incidencias;
            existentes[elem[0]] = 1;
            resultado.push({
                nombre: elem[0],
                incidencias: incidencias,
                coords: elem[2],
                linea: estInc[elem[0]].lineas
            })
        }
    }
    return resultado;
}