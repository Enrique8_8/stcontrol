// Funcion que le agrega evento al boton de enviar comentario
function eventoEnviarComentario(padre, idPeticion){
    let btn = document.querySelector(`#${padre} .btnCrearComentario`);
    btn.addEventListener('click', e => {
        e.preventDefault();
        console.log("la id del padre es: " + padre)
        let textArea = document.querySelector(`#${padre} .crearComentario textarea`);
        let comentario = textArea.value;

        peticion('/STControl/CrearComentario', ['comentario', comentario], ['idEntrada', idPeticion])
            .then( async response => {
                if(response.status == 200){
                    let data = await response.json();
                    
                    if(data['error'] == true){
                        let ui = new Interface();
                        switch (data['tipo']) {
                            case 'usuario':
                                window.location.href = "../pageLogin/login.jsp?redireccionado=true";
                                break;
                        
                            case 'datos':
                                setTimeout(()=>{
                                    ui.insertarAlerta(document.querySelector(`#${padre} .contComentarios`), "Por favor, no uses caracteres invalidos");
                                }, 5000);
                                break;
                            case 'entrada':
                                window.location.reload();
                                alert("Por favor, vuelve a intentar insertar tu comentario");
                                break;
                        }
                    }else{
                        
                        let contComent = document.querySelector(`#${padre} .contComentarios .contComentarioComent`);
                        
                        contComent.appendChild(templateComentario(data['idComentario'], comentario, data['usuario']));
                        textArea.value = '';
                        
                    }
                }
            }).catch(() => {
                console.log("uis crear");
            })

    })
}