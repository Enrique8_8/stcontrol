function parametros(){
    let params = new URLSearchParams( window.location.search );
    if(params.has('errorDatos') && params.get('errorDatos') == 'true'){
        let cont = document.querySelector('#errorAlerta');
        let ui = new Interface();
        ui.insertarAlerta(cont, 'Hubo un error al procesar los datos, por favor vuelve a intentar');
        setTimeout( ()=>{
            cont.innerHTML = '';
        }, 5000);
    }
}