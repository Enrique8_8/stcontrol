document.addEventListener('DOMContentLoaded', async  e => {
    e.preventDefault();
    let response = await peticion("/STControl/ObtenerEntradas");
    if(response.status == 200){
        let data = await response.json();
        let contenedor = document.querySelector('#entradasRegistradas');

        for(let entrada of data){
            let entradaHtml = templateEntrada(entrada['id_e'], entrada['nombre_m'], entrada['nombre_e'], entrada['desc']);
            contenedor.appendChild(entradaHtml);
        }
    }
    parametros();
    eventosComentarios();
});