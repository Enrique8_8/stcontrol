<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Foro</title>

    <!-- Estilos generales-->
    <t:estilosGenerales></t:estilosGenerales>

    <!-- <link
        href="https://fonts.googleapis.com/css?family=Montserrat:400,600|Overpass+Mono:400,600|Poppins:400,600&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Monoton&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="../../css/Normalize.css">
    <link rel="stylesheet" href="../../css/stylesHeader.css">
    <link rel="stylesheet" href="../../css/stylesFooter.css">
    
    <link rel="stylesheet" href="../../css/stylesGenerales.css"> -->
    
    
    <link rel="stylesheet" href="../../interfaz/interface.css">
    <!-- Estilos Propios-->

    <link rel="stylesheet" href="css/stylesForo.css">
    <script src="../../interfaz/Interface.js" type="text/javascript"></script>

</head>

<body>
    <t:Header username="${sessionScope.username}"></t:Header>

    <div id="contenedor_principal">

        <div id="contenedorEntradas">
            <h1>Ultimas entradas</h1>
            <div id="errorAlerta"></div>
            <div id="entradasRegistradas">
                    

            </div><!-- #entradasRegistradas -->
        </div>

        <div id="contenedorCrear">
            <div id="barra"></div>

            <div id="crearEntrada">
                <h2>Crear Entrada</h2>
                <form id="enviarEntrada" name="enviarEntrada" method="post" action="../../CrearEntradaF">
                    <input type="text" name="nameE" id="nameE" placeholder="Introduce tu pregunta" minlength="1"
                        maxlength="29">

                    <textarea name="ent" id="ent" cols="18" rows="18" placeholder="Introduce tu entrada" minlenght="1"
                        maxlenght="119"></textarea>

                    <button type="submit" name="btnEnviar" id="btnEnviar">Enviar</button>
                </form>

            </div>
        </div>


    </div>

    <t:Footer></t:Footer>
    <script src="../../js/CrearComentario/crearComentario.js" type="text/javascript"></script>
    <script src="../../js/peticiones/peticion.js" type="text/javascript"></script>
    <script src="js/template.js" type="text/javascript"></script>
    <script src="js/parametros.js"></script>
    <script src="js/events/eventosComentarios.js"></script>
    <script src="js/events/cargaInicial.js" type="text/javascript"></script>
</body>

</html>