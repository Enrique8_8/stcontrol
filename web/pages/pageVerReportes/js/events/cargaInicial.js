document.addEventListener('DOMContentLoaded', e => {
    e.preventDefault();
    peticion("/STControl/ObtenerReportes")
            .then((response)=>{
                insertarContenido(response);
            }).catch(() => {
                console.log("uis")
            });
            
    insertarEstF();
    desplegadoFiltros();
    aplicarFiltros();

});

async function insertarContenido(response){
    if(response.status == 200){
        let data = await response.json();
        let contenedor = document.querySelector('#contenedorResultados');
        console.log("la coso es: ", data);
        for(let reporte of data){
            let rep = templateReporte(reporte['id_i'],reporte['linea'], reporte['estacion'], reporte['coment'], reporte['fecha'], reporte['hora'], reporte['tipo']);

            contenedor.appendChild(rep);
        }
    }
}
