<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Redireccionamiento</title>
        <t:estilosGenerales></t:estilosGenerales>

        <link rel="stylesheet" href="css/stylesRedireccionamiento.css">
    </head>
    <body>
        <t:Header username="${sessionScope.username}"></t:Header>
        <div id="contenedorPrincipal">
            <h1>Estas siendo redireccionado</h1>
            <p id="mensaje"></p>
        </div>
        <script src="js/events/cargaInicial.js"></script>
    </body>
    <t:Footer></t:Footer>
</html>
