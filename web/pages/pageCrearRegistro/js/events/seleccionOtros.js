function cambioSelect(){
    // Funcion que agrega un evento al select que contiene el tipo de incidente que se esta reportando
    let tipo = document.querySelector('#tipo');
    tipo.addEventListener('change', e=>{
        e.preventDefault();

        let seleccion = tipo.options[tipo.selectedIndex].value;

        let contenedor = document.querySelector('#contData3');

        // Revisa que se haya seleccionado la opcion de otros
        if(seleccion == 'otros'){
            // Crea un contenedor, para poder insertar esto
            let contInsert = ui.crearElemento('div', '', ['id', 'datoOtros'])
            contInsert.innerHTML = `
                <label for="otro">Escribe el tipo de incidente: </label>
                <input type="text" name="otro" id="otro" minlenght="1" maxlenght="20" required>`;
            contenedor.appendChild(contInsert);
        }else{
            let hijo = document.querySelector('#datoOtros');
            if( hijo != null){
                contenedor.removeChild(hijo);
            }
        }
        
    })
}