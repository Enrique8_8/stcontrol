var estacionesMetroCompleto = cargarEstaciones(estacionesJSON);
var ui = new Interface();

document.addEventListener('DOMContentLoaded', async e=>{
    e.preventDefault();

    let parametros = new URLSearchParams( window.location.search );

    if(parametros.has('id')){
        let id = parametros.get('id');

        let data_prev = await peticion('/STControl/ObtenerReporte', ['id', id])
        let data = await data_prev.json();
        
        console.log("entra, la data es: " +data);
        document.querySelector('#linea').value ="Metro " + data['linea'];
        
        // Carga las estaciones al select de estaciones segun la linea del metro que tenga
        logica(document.querySelector('#linea'));
        
        // Agrega el valor de la estacion
        document.querySelector('#btnSubmit').value = id;
        document.querySelector('#estaciones').value = data['estacion'];
        document.querySelector('#dia').value = data['fecha'];
        document.querySelector('#hora').value = data['hora'];
        document.querySelector('#desc').value = data['coment'];
        document.querySelector('#tipo').value = data['tipo'];
                
        cambioSelectLinea();
        if(parametros.has('errorDatos') && parametros.get('errorDatos') == 'true'){
            alert("Hubo un error en los datos, por favor vuelve a intentar editar tu reporte");
        }
    }else{
        window.location.replace("../pageIndex/index.jsp");
    }

    
})