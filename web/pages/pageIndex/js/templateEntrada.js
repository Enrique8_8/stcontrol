function templateEntrada(ne, desc, nm){
    let ui = new Interface();
    let contenendor = ui.crearElemento('div', '', ['class', 'entradaForo']);
    contenendor.innerHTML = `
        <div class="datosEntrada">
            <p class="nombreEntrada">${ne}</p>
            <p class="contenidoEntrada">${desc}</p>
        </div>
        <p class="userEntrada">${nm}</p>
    `;
    return contenendor;
}