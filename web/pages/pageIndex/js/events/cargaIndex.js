document.addEventListener('DOMContentLoaded', async e => {
    e.preventDefault();

    cargarMapa();

    let response = await peticion('/STControl/ObtenerUltimasEntradas');
    if(response.status == 200){
        let contenedor = document.querySelector('#entradasRecientes');
        let data = await response.json();
        for(let d of data){
            let entrada = templateEntrada(d['nombre_e'], d['desc'], d['nombre_m']);
            contenedor.appendChild(entrada);
        }
    }else
        window.location.reload();

})