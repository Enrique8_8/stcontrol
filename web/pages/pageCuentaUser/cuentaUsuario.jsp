<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<!Doctype html>
<html>

<head>
    <title>Mi Cuenta</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Estilos generales-->
    <t:estilosGenerales></t:estilosGenerales>
    
    <!-- Estilos Propios-->
    <link href="css/stylesCuenta1.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../../css/stylesLineas.css">

</head>

<body>
    <t:Header username="${sessionScope.username}"></t:Header>

    <div id="contenedorPrincipal">
        <div id="aside">
            <div id="indice">
                <h3 class="titulo" id="nombreUsuario">Mi cuenta</h3>
                <img src="../../assets/Guy.png" alt="Icono Usuario">
                <ul>
                    <li><a href="#config">Configuracion de la cuenta</a></li>
                    <li><a href="#seccionReportes">Mis reportes</a></li>
                    <li><a href="#seccionReportesLinea" id="textoSeccionLinea1">Reportes linea</a></li>
                    <li><a href="#foro">Mi actividad en el foro</a></li>
                    <li><a href="#seccionComent">Mis comentarios</a></li>
                </ul>
                <section id="cerrarSesion">
                    <button type="button" id="btnSesion">Cerrar Sesion</button>
                </section>
            </div><!-- #indice -->

            <div id="barra"></div>
        </div>

        <div id="contenedorCuenta">
            <div id="errorAlerta"></div>
            <section id="config">
                <h3 class="titulo">Configuracion de la cuenta</h3>
                <div id="camposModificar">
                    <form action="/STControl/EliminarCuentaMiembro" id="formDatosCuenta" method="post">
                        <div class="datoUsuario">
                            <label class="texto" for="correo">Correo Electronico: </label>
                            <input class="campo campoConfig" type="text" name="correo" id="correo" disabled>
                        </div><!-- .datoUsuario -->

                        <div class="datoUsuario">
                            <label class="texto" for="password">Contraseña:</label>
                            <input class="campo campoConfig" type="password" name="password" id="password" disabled>
                        </div><!-- .datoUsuario -->

                        <div class="datoUsuario">
                            <label class="texto" for="linea">Linea preferente</label>
                            <select class="campo campoConfig" id="linea" name="linea" required disabled>
                                <option value="Metro 1">Línea 1</option>
                                <option value="Metro 2">Línea 2</option>
                                <option value="Metro 3">Línea 3</option>
                                <option value="Metro 4">Línea 4</option>
                                <option value="Metro 5">Línea 5</option>
                                <option value="Metro 6">Línea 6</option>
                                <option value="Metro 7">Línea 7</option>
                                <option value="Metro 8">Línea 8</option>
                                <option value="Metro 9">Línea 9</option>
                                <option value="Metro A">Línea A</option>
                                <option value="Metro B">Línea B</option>
                                <option value="Metro 12">Línea 12</option>
                            </select>
                        </div><!-- #contLineas -->

                        <div id="bntConfig">
                            <button id="btnEditarCuenta" type="button" title="Editar datos">Editar datos</button>
                            <button id="btnBorrarCuenta" type="submit" title="Eliminar cuenta">Borrar cuenta</button>
                        </div>
                        <div id="btnCambios">
                            <button id="btnAplicarCambios" type="submit" title="Aplicar cambios">Aplicar
                                Cambios</button>
                            <button id="btnCancelarConfig" type="button" title="Cancelar cambios">Cancelar</button>
                        </div>
                    </form>
                </div><!-- #camposModificar -->


            </section><!-- #config -->

            <section id="seccionReportes">
                <h3 class="titulo">Mis Reportes</h3>
                <div class="cont" id="contReportes"></div><!-- #contReportes -->
            </section><!-- #reportes -->

            <section id="seccionReportesLinea">
                <h3 class="titulo" id="textoSeccionLinea2">Reportes linea</h3>
                <div class="cont" id="contReportesLinea"></div><!-- #contReportesLinea -->
            </section>

            <section id="foro">
                <h3 class="titulo">Mi actividad en el foro</h3>
                <div class="cont" id="contForo">
                    <div id="entradasForo"></div>
                    <div id="comentariosForo"></div>
                </div><!-- #contForo -->
            </section><!-- #foro -->

            <section id="seccionComent">
                <h3 class="titulo">Mis comentarios</h3>
                <div id="contComentariosUser"></div>
            </section>

        </div><!-- #contenedorCuenta -->



    </div><!-- #contenedorPrincipal -->

    <t:Footer></t:Footer>
    <script src="../../js/CrearComentario/crearComentario.js" type="text/javascript"></script>
    <script src="../../js/peticiones/peticion.js" type="text/javascript"></script>
    <script src="../../interfaz/Interface.js"></script>
    <script src="js/templatesUser.js" type="text/javascript"></script>
    <script src="js/insertarDatos.js" type="text/javascript"></script>
    <script src="js/parametros.js"></script>

    <script src="js/confirmBorrado.js" type="text/javascript"></script>
    <script src="js/events/eventosCargaU.js" type="text/javascript"></script>
    <script src="js/events/eventosConfig.js" type="text/javascript"></script>
    <script src="js/events/eventosReportes.js" type="text/javascript"></script>
    <script src="js/events/eventosEntradasF.js" type="text/javascript"></script>
    <script src="js/events/eventosComentarios.js" type="text/javascript"></script>
    <script src="js/events/eventosCerrarS.js" type="text/javascript"></script>
</body>


</html>
