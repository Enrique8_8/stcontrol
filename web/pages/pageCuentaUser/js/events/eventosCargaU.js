function eventosCargaU(){
    document.addEventListener('DOMContentLoaded', async e => {
        e.preventDefault();

        // Ejecucion de la peticione fetch para obtener los datos del usuario
        let response = await peticion('/STControl/ObtederDatosUser');
        
        if(response.status == 200){
            let data = await response.json();
            
            // incrustado de los datos en la pagina        
            insertarMiembro(data['datosUser']);
            insertarReportes(data['reportesUser']);
            insertarForo(data['entradasUser']);
            insertarReportesLinea(data['reportesLinea']);
            insertarComentariosUser(data['comentarios']);
        }
        
        eventosReportes();
        eventosEntradasF();
        eventosCerrar();
        eventosComentarios();
        parametros();
    })
}
eventosCargaU();
