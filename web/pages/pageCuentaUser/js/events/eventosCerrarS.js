function eventosCerrar(){
    let btn = document.querySelector("#btnSesion");
    
    btn.addEventListener('click', async e=>{
        e.preventDefault();

        let response = await peticion('/STControl/CerrarSesion');

        if(response.status == 200){
            let url = "../pageIndex/index.jsp";
            window.location.replace(url);
        }
    });
}