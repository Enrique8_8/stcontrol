function eventosComentarios(){
    let btnBorrarC = document.querySelectorAll('.btnBorrarComentario');
    let btnVerMas = document.querySelectorAll('.btnVerEntradaComent');

    for(let btn of btnBorrarC){
        btn.addEventListener('click', async e => {
            e.preventDefault();

            let idPrev = btn.parentElement.parentElement.id;
            let id = idPrev.substring(6);
            console.log("la id para borrar es: " + id)
            let response = await peticion('/STControl/EliminarComentarioMiembro', ['idComentario', id]);

            if(response.status == 200){
                let data = await response.json();
                confirmBorrado(data, btn.parentElement.parentElement)
            }else
                alert("No se puede borrar tu entrada, por favor reintentalo")

        })
    }

    for(let btn of btnVerMas){
        btn.addEventListener('click', e => {
            e.preventDefault();
            
            let id = btn.classList.item(1);
            let url = '../pageVerEntrada/verEntrada.jsp?id=' + id.substring(7);
            window.location.replace( url );
            

        })
    }
}