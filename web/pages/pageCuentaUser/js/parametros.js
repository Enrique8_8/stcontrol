function parametros(){
    let params = new URLSearchParams( window.location.search );

    if(params.has('edicion') && params.get('edicion') == 'false'){
        let cont = document.querySelector('#errorAlerta');
        let ui = new Interface();
        ui.insertarAlerta(cont, 'Hubo un error al editar el registro, por favor vuelvelo a intentar');
        setTimeout( ()=>{
            cont.innerHTML = '';
        }, 5000);
    }else if(params.has('edicion') && params.get('edicion') == 'true'){
        let cont = document.querySelector('#errorAlerta');
        let ui = new Interface();
        ui.insertarConfirmacion(cont, 'Tu registro pudo ser editado sin problemas');
        setTimeout(() => {
            cont.innerHTML = '';
        },5000);
    }
}