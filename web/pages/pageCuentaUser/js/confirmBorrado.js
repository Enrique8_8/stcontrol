function confirmBorrado(data, padre){
    if(data.cuenta == 'error'){
        window.location.replace("../pageIndex/index.jsp");
    }else{
        if(data.eliminado)
            padre.remove();
        else
            alert("Hubo un error al eliminar tu registro, por favor vuelve a tratar");
    }
}