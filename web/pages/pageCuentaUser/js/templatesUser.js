function templateReportes(id, tipo, linea, estacion, descripcion, fecha, hora, botones = true){
    let contenedor = document.createElement('div');
    contenedor.classList.add('reporte');
    contenedor.classList.add('linea'+linea);
    contenedor.id = id;
    if(botones)
            contenedor.innerHTML = `<div class="contReporte">
                                <h3 class="tipoReporte">${tipo}</h3>
                                <p class="lineaReporte">Linea ${linea}</p>
                                <p class="estacionReporte">${estacion}</p>
                                <p class="descReporte">${descripcion}</p>
                                <p class="fechaReporte">${fecha}</p>
                                <p class="horaReporte">${hora}</p>
                            </div><!-- .contReporte -->
                    
                            <div class="botonesReporte">
                                <button class="btnEditar editarRep" title="Editar reporte">
                                    <span class="material-icons">
                                        edit
                                    </span>
                                    Editar
                                </button>
                                <button class="btnEliminar borrarRep" title="Eliminar reporte">
                                    <span class="material-icons">
                                        delete
                                    </span>
                                    Eliminar
                                </button>
                            </div><!-- .botonesReporte -->

                            <div class="circulosLaterales">
                                <div class="circulo"></div>
                                <div class="circulo"></div>
                                <div class="circulo"></div>
                            </div><!-- .circulosLaterales -->

                            <div class="barra"></div>`;
    else
           contenedor.innerHTML = `<div class="contReporte">
                                <h3 class="tipoReporte">${tipo}</h3>
                                <p class="lineaReporte">Linea ${linea}</p>
                                <p class="estacionReporte">${estacion}</p>
                                <p class="descReporte">${descripcion}</p>
                                <p class="fechaReporte">${fecha}</p>
                                <p class="horaReporte">${hora}</p>
                            </div><!-- .contReporte -->

                            <div class="circulosLaterales">
                                <div class="circulo"></div>
                                <div class="circulo"></div>
                                <div class="circulo"></div>
                            </div><!-- .circulosLaterales -->

                            <div class="barra"></div>`;
    return contenedor;
}

function templateEntrada(id, preg, desc){
    let contenedor = document.createElement('div');
    contenedor.classList.add('entradaForo');
    contenedor.id = 'entrada'+id;

    contenedor.innerHTML = `<div class="contenidoEntrada">
                                <h3>${preg}</h3>
                                <p>${desc}</p>
                            </div>
                            <div class="botonesEntrada">
                                <button class="btnComentariosEntrada" title="Comentarios de la entrada">
                                    <span class="material-icons">
                                        comment
                                    </span>
                                    Comentarios
                                    <span class="material-icons">
                                        keyboard_arrow_down
                                    </span>
                                </button>
                                <button class="btnEditar editEntrada" title="Editar entrada">
                                    <span class="material-icons">
                                        edit
                                    </span>
                                    Editar
                                </button>
                                <button class="btnEliminar borrarEntrada" title="Eliminar entrada">
                                    <span class="material-icons">
                                        delete
                                    </span>
                                    Eliminar
                                </button>
                            </div><!-- .botonesEntrada -->

                            <div class="contComentarios">
                                <div class="contComentarioComent"></div>
                                <div class="contBtnCrearComentario"></div>
                            </div>`;
    return contenedor;
}

function templateComentario(idComentario, comentario, user){
    let ui = new Interface();

    let contenedor = ui.crearElemento('div', '', ['class', 'comentario'], ['id', `coment${idComentario}`]);
    contenedor.innerHTML = `
        <p>${comentario}</p>
        <p>${user} </p>`;

    return contenedor;
}

function templateComentarioUser(idComentario, comentario, entrada){
    let ui = new Interface();
    
    let contenedor = ui.crearElemento('div', '', ['class', 'comentarioUser'], ['id', `coment${idComentario}`]);
    
    contenedor.innerHTML = `
    <div class="contComentario">
        <p>${comentario}</p>
    </div>
    <div class="btnContComentario">
        <button class="btnEliminar btnBorrarComentario" title="Eliminar entrada">
            <span class="material-icons">
                delete
            </span>
            Eliminar
        </button>
        <button class="btnVerEntradaComent entrada${entrada}">
            <span class="material-icons">
                more_horiz
            </span>
            Ver entrada completa
        </button>
    </div>`;
    
    return contenedor;
}