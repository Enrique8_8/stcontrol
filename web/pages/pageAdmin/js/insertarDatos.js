function insertarMiembros(contenedor, data){
    let contenedor2 = document.createElement('div');
    contenedor2.id = 'encabezadosUsers';
    contenedor2.innerHTML = `
    <ul>
        <li>Usuario</li>
        <li>Correo</li>
        <li>Linea del metro</li>
        <li>Eliminar cuenta</li>
    </ul>`;

    contenedor.appendChild(contenedor2);
    for(let user of data){
        let miembro = templateMiembro(user['nombre'], user['correo'], user['linea']);
        contenedor.appendChild(miembro);
    }
}

function insertarReporte(contenedor, data){   
    for(let rep of data){
        let reporte = templateReporte(rep['id_i'], rep['linea'], rep['estacion'], rep['coment'], rep['tipo'], rep['fecha'], rep['hora'], rep['nombre_m']);
        contenedor.appendChild(reporte);
    }
}

function insertarEntradas(contenedor, data){
    console.log("ui")
    for(let ent of data){
        let entrada = templateEntrada(ent['id_e'], ent['nombre_e'], ent['desc'], ent['nombre_m']);
        contenedor.appendChild(entrada);
    }
}