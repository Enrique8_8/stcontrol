function borradoMiembro(){
    let botones = document.querySelectorAll('.btnEliminarMiembro');
    for(let btn of botones){
        btn.addEventListener('click', async e => {
            e.preventDefault();

            let id = btn.parentElement.id;

            let response = await peticion('/STControl/AdminEliminarUser', ["userDelete", id]);

            if(response.status == 200){
                let data = await response.json();
                if(data['eliminado'] == true){
                    btn.parentElement.remove();
                }else{
                    window.location.replace( "../pageLogin/login.jsp?redireccionado=true" );
                }
            }else
                alert("No se pudo eliminar el usuario, por favor reintentalo");

        })
    }
}

function borradoReporte(){
    let botones = document.querySelectorAll('.btnEliminarReporte');

    for(let btn of botones){
        btn.addEventListener('click', async e => {
            e.preventDefault();

            let idPrev = btn.parentElement.parentElement.parentElement.id;
            let id = idPrev.substring(3);

            let response = await peticion('/STControl/AdminEliminarReporte', ['idReporte', id]);

            if(response.status == 200){
                let data = await response.json();
                if(data['eliminado']){
                    document.querySelector(`#${idPrev}`).remove();
                }else
                    window.location.replace( "../pageLogin/login.jsp?redireccionado=true" );
            }else
                alert("No se pudo eliminar el reporte, por favor reintentalo")

        })
    }

}

function borradoEntrada(){
    let botones = document.querySelectorAll('.btnEliminarEntrada');

    for(let btn of botones){
        btn.addEventListener('click', async e => {
            e.preventDefault();

            let idPrev = btn.parentElement.parentElement.id;
            let id = idPrev.substring(3);
            console.log("la id es: " + id)
            let response = await peticion('/STControl/AdminEliminarEntrada', ['idEntrada', id]);

            if(response.status == 200){
                let data = await response.json();
                if(data['eliminado']){
                    document.querySelector(`#${idPrev}`).remove();
                }else
                    window.location.replace( "../pageLogin/login.jsp?redireccionado=true" );
            }else
                alert("No se pudo eliminar el usuario, por favor reintentalo");

        })
    }
}