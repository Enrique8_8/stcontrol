<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Administracion</title>

    <!-- Estilos generales-->
    <t:estilosGenerales></t:estilosGenerales>
    <!--<link
        href="https://fonts.googleapis.com/css?family=Montserrat:400,600|Overpass+Mono:400,600|Poppins:400,500,600&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Monoton&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="../../css/Normalize.css">
    <link rel="stylesheet" href="../../css/stylesHeader.css">
    <link rel="stylesheet" href="../../css/stylesFooter.css">
    <link rel="stylesheet" href="../../interfaz/interface.css">

    <link rel="stylesheet" href="../../css/stylesGenerales.css">-->
    
    <link href="../../css/stylesLineas.css" rel="stylesheet" type="text/css"/>

    <!-- Estilos propios -->
    <link rel="stylesheet" href="css/stylesAdmin.css">

</head>

<body>
    <t:Header username="${sessionScope.username}"></t:Header>
    <div id="contenedorPrincipal">

        <div id="barraSuperior">
            <div id="contextoDescripcion">
                <h2>Administracion <span id="nombreCoso"></span></h2>
            </div>
            <div id="contenedorOpciones">
                <ul>
                    <li id="zonaCrearCuenta">
                        <span class="material-icons">
                            add_circle_outline
                        </span>
                        Crear Cuenta</li>
                    <li id="btnCerrarSesion">
                        <span class="material-icons">
                            power_settings_new
                        </span>
                        Cerrar Sesion
                    </li>
                </ul>
            </div>
        </div>

        <aside id="indice">
            <ul>
                <li id="usuarios">
                    <span class="material-icons">
                        account_circle
                    </span>
                    <span>Usuarios</span>
                </li>
                <hr>
                <li>
                    <div id="registrosRegistrados">
                        <span class="material-icons">
                            list_alt
                        </span>
                        <span>Registros Almacenados</span>
                    </div>
                    <ul>
                        <hr>
                        <li id="reportesR">
                            <span class="material-icons">
                                keyboard_arrow_right
                            </span>
                            <span>Reportes</span>
                        </li>
                        <li id="entradasR">
                            <span class="material-icons">
                                keyboard_arrow_right
                            </span>
                            <span>Entradas</span>
                        </li>
                    </ul>
                </li>

            </ul>
        </aside>

        <main>
            <section id="zonaPrincipal">
                <h2>Bienvenido a la zona de administradores</h2>
                <img src="../../assets/logo.png" alt="Logo de la pagina">
            </section>

            <section id="zonaUsuarios"></section>

            <section id="zonaReportesBorrar"></section>

            <section id="zonaEntradasF">
                <div id="contenedorEntradas"></div><!-- .contenedorEntradas -->
            </section>

            <section id="crearCuenta">
                <h2>Crear cuenta</h2>
                <form action="../../CrearCuentaAdmin" method="post">

                    <div class="campo">
                        <label for="username">Nombre de usuario: </label>
                        <input type="text" name="username" id="username" minlength="1"
                                        maxlength="20" placeholder="Nombre de usuario" pattern="[-a-zA-Z0-9~!$%^&*_=+,ñ. }{\\?]{0,20}" required>
                    </div>
                    <div class="campo">
                        <label for="correo">Correo electronico: </label>
                        <input type="email" name="correo" id="email" required
                                        placeholder="ejemplo@correo.com" minlength="7" maxlength="30" pattern="(?i)^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$"/>
                    </div>
                    <div class="campo">
                        <label for="pass">Contraseña: </label>
                        <input type="password" name="pass" id="pass" minlength="1" maxlength="20" placeholder="Contraseña" required pattern="[-a-zA-Z0-9~!$%^&*_=+,ñ. }{\\?]{0,20}">
                    </div>
                    <div class="campo">
                        <label for="pass2">Confirma contraseña: </label>
                        <input type="password" name="pass2" id="pass2" minlength="1"
                                        maxlength="20" placeholder="Contraseña" required pattern="[-a-zA-Z0-9~!$%^&*_=+,ñ. }{\\?]{0,20}">
                    </div>
                    <div class="campo">
                        <label for="linea">Linea de metro:
                        </label>
                        <select id="linea" name="linea" required>
                            <option value="0" selected disabled>Selecciona una opción</option>
                            <option value="1">Línea 1</option>
                            <option value="2">Línea 2</option>
                            <option value="3">Línea 3</option>
                            <option value="4">Línea 4</option>
                            <option value="5">Línea 5</option>
                            <option value="6">Línea 6</option>
                            <option value="7">Línea 7</option>
                            <option value="8">Línea 8</option>
                            <option value="9">Línea 9</option>
                            <option value="A">Línea A</option>
                            <option value="B">Línea B</option>
                            <option value="12">Línea 12</option>
                        </select>
                    </div>
                    <div class="campo">
                        <label for="tipoC">Tipo de cuenta</label>
                        <select name="tipoC" id="tipoC" required>
                            <option value="0" selected disabled>Selecciona una opción</option>
                            <option value="u">Usuario comun</option>
                            <option value="a">Administrador</option>
                        </select>
                    </div>
                    <div id="btnCrearCuenta">
                        <button type="submit">Crear cuenta</button>
                        <button type="reset">Limpiar datos</button>
                    </div>
                </form>


            </section>

        </main>


    </div>
    
    <t:Footer></t:Footer>
    
    <script src="../../interfaz/Interface.js" type="text/javascript"></script>
    <script src="../../js/peticiones/peticion.js" type="text/javascript"></script>
    
    <script src="js/parametros.js" type="text/javascript"></script>
    <script src="js/events/eventosBorrado.js" type="text/javascript"></script>
    <script src="js/events/cerrarSesion.js" type="text/javascript"></script>
    <script src="js/templates.js" type="text/javascript"></script>
    <script src="js/events/eventosEntrada.js" type="text/javascript"></script>
    <script src="js/insertarDatos.js" type="text/javascript"></script>
    <script src="js/events/eventosCambioT.js" type="text/javascript"></script>
    <script src="js/events/cargaPagina.js" type="text/javascript"></script>
</body>

</html>

<!-- http://127.0.0.1:5500/web/pages/pageAdmin/admin.html -->