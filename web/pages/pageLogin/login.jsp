<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>

    <t:estilosGenerales></t:estilosGenerales>
    
    <link rel="stylesheet" href="stylesLogin.css">
    <link rel="stylesheet" href="../../interfaz/interface.css">
</head>

<body>
    <t:Header username="${sessionScope.username}"></t:Header>
    
    <div id="contenedorPrincipal">
        <main class="centrado">
            <section id="beneficios" class="centrado-vertical">
                <h1 class="titulo">Con tu cuenta de STControl puedes: </h1>
                <ul class="centrado-vertical">
                    <li>Crear reportes sobre incidentes ocurridos en el Metro</li>
                    <li>Realizar un seguimiento a los incidentes que desees</li>
                    <li>Interactuar con la comunidad de usuarios</li>
                    <li>Poder visualizar y modificar tus reportes creados</li>
                </ul>
            </section>

            <section id="login">

                <div id="superiorLogin" class="centrado">

                    <ul>
                        <li id="crear"><a href="#">Crear Cuenta</a></li>
                        <li id="iniciar"><a href="#">Iniciar Sesión</a></li>
                    </ul>

                </div><!-- #superiorLogin -->

                <div class="medio">
                    <img src="../../assets/Guy.png" alt="Usuario Anonimo"/>
                    <h2 class="titulo" id="textoDesc">Crea tu cuenta de STControl</h2>
                </div><!-- .zonaMedia -->

                <div id="alertaError"></div><!-- #alertaError -->

                <div id="formularioRelleno">
                    <div id="formCrearCuenta">
                        <form class="login" name="crearCuenta" id="crearCuenta" method="post" action="../../CrearCuentaC">

                            <div class="text-control">
                                <h3 class="titulo">Ingresa los siguientes datos:</h3>
                            </div>

                            <div class="data-entry">
                                <div class="campoLogin centrado" id="data1">
                                    <label for="username">Nombre de usuario:</label>
                                    <input type="text" name="username" id="username" required minlength="1"
                                        maxlength="20" placeholder="Nombre de usuario" pattern="[-a-zA-Z0-9~!$%^&*_=+,ñ. }{\\?]{0,20}"/>
                                </div>

                                <div class="campoLogin centrado" id="data2">
                                    <label for="email">Correo electronico:</label>
                                    <input type="email" name="email" id="email" required
                                        placeholder="ejemplo@correo.com" minlength="7" maxlength="30" pattern="(?i)^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$"/>
                                </div>

                                <div class="campoLogin centrado" id="data3">
                                    <label for="user-password">Contraseña:</label>
                                    <input type="password" name="user-password" id="user-password" required
                                        minlength="1" maxlength="20" placeholder="Contraseña" pattern="[-a-zA-Z0-9~!$%^&*_=+,ñ. }{\\?]{0,20}"/>
                                </div>

                                <div class="campoLogin centrado" id="data4">
                                    <label for="vpassword">Confirmacion de contraseña:</label>
                                    <input type="password" name="vpassword" id="vpassword" required minlength="1"
                                        maxlength="20" placeholder="Contraseña" pattern="[-a-zA-Z0-9~!$%^&*_=+,ñ. }{\\?]{0,20}">
                                </div>

                                <div class="campoLogin centrado" id="data5">
                                    <label for="linea">Linea de metro que desees seguir:
                                    </label>
                                    <select id="linea" name="linea" required>
                                        <option selected disabled>Selecciona una opción</option>
                                        <option value="Metro 1">Línea 1</option>
                                        <option value="Metro 2">Línea 2</option>
                                        <option value="Metro 3">Lí­nea 3</option>
                                        <option value="Metro 4">Línea 4</option>
                                        <option value="Metro 5">Línea 5</option>
                                        <option value="Metro 6">Línea 6</option>
                                        <option value="Metro 7">Línea 7</option>
                                        <option value="Metro 8">Línea 8</option>
                                        <option value="Metro 9">Línea 9</option>
                                        <option value="Metro A">Línea A</option>
                                        <option value="Metro B">Línea B</option>
                                        <option value="Metro 12">Línea 12</option>
                                    </select>
                                </div>

                                <button type="submit" form="crearCuenta" value="Ingresar"
                                    class="btnSubmit">Ingresar</button>
                            </div>

                        </form>
                    </div><!-- #formCrearCuenta --> 
                </div><!-- .formularioRelleno -->

            </section>

            <script src="js/events/eventsCrearC.js"></script>
            <script src="js/events/eventsInicioS.js"></script>
            <script src="../../js/validaciones/validaciones.js"></script>
            <script src="js/detectadoParametros.js"></script>
            <script src="js/templatesLogin.js"></script>
            <script src="../../interfaz/Interface.js"></script>
            <script src="js/login.js"></script>
            <script src="js/events/cargaInicial.js"></script>
        </main>
    </div>
    
    <t:Footer></t:Footer>
    
</body>

</html>