function eventsInicioS(){
    let form = document.querySelector('#iniciarSesion');

    form.addEventListener('submit', e => {
        e.preventDefault();
        // Aqui se va a validar el formulario de iniciar sesion
        let user = document.querySelector('#username');
        let password = document.querySelector('#user-password');

        if(comprobarValor(user.value, 20)==false || comprobarValor(password.value, 20)==false){
            e.preventDefault();
            
            let ui = new Interface();
            let cont = document.querySelector('#alertaError');
            ui.insertarAlerta(cont, "Por favor, rellena correctamente el formulario");
            form.reset();
            setTimeout(()=>{
                cont.innerHTML = '';
            }, 5000)
        }
    });
}