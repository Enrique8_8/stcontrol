function eventsCrearC(){
    let form = document.querySelector('#crearCuenta');
    form.addEventListener('submit', e => {
        e.preventDefault();
        
        let user = document.querySelector('#username');
        let email = document.querySelector('#email');
        let pass1 = document.querySelector('#user-password');
        let pass2 = document.querySelector('#vpassword');
        let linea = document.querySelector('#linea');

        let selectValido = true;
        let passValida = true;
        let emailValido = false;


        if(/^Metro ([1-9]{1}|[A-B]{1}|[12]{2})$/.test(linea.options[linea.selectedIndex].value)){
            let ultimo = linea.options[linea.selectedIndex].value.substring(6);
            if(ultimo.length === 2 && ultimo == 21)
                selectValido = false;
        }else
            selectValido = false;

        if(comprobarValor(pass1.value, 20))
            if(pass1.value != pass2.value)
                passValida = false;
            else{
                console.log("valor 1 " + pass1.value)
                console.log("valor 2 " + pass2.value)
            }
        else
            passValida = false;

        if(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email.value) && email.value.length <= 30 ){
            emailValido = true;
        }
        
        if(comprobarValor(user.value, 20)==false || selectValido==false || passValida==false || emailValido==false){
            e.preventDefault();

            let ui = new Interface();
            let cont = document.querySelector('#alertaError');
            ui.insertarAlerta(cont, "Por favor, rellena correctamente el formulario");
            form.reset();
            setTimeout(()=>{
                cont.innerHTML = '';
            }, 5000)

        }
    });
}