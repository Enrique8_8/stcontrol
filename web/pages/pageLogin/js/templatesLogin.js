let patternCorreo = "(?i)^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$"
// Template de crear cuenta
var crearCuenta = `
<div id="formCrearCuenta">
<form class="login" name="crearCuenta" id="crearCuenta" method="post" action="../../CrearCuentaC">

    <div class="text-control">
        <h3 class="titulo">Ingresa los siguientes datos:</h3>
    </div>

    <div class="data-entry">
        <div class="campoLogin" id="data1">
            <label for="username">Nombre de usuario:</label>
            <input type="text" name="username" id="username" required minlength="1"
                maxlength="20" placeholder="Nombre de usuario" pattern="[-a-zA-Z0-9~!$%^&*_=+,ñ. }{?]{0,20}"/>
        </div>

        <div class="campoLogin" id="data2">
            <label for="email">Correo electronico:</label>
            <input type="email" name="email" id="email" required
                placeholder="ejemplo@correo.com" minlength="7" maxlength="30" pattern=${patternCorreo}/>
        </div>

        <div class="campoLogin" id="data3">
            <label for="user-password">Contraseña:</label>
            <input type="password" name="user-password" id="user-password" required
                minlength="1" maxlength="20" placeholder="Contraseña" pattern="[-a-zA-Z0-9~!$%^&*_=+,ñ. }{?]{0,20}"/>
        </div>
        
        <div class="campoLogin" id="data4">
            <label for="vpassword">Confirmacion de contraseña:</label>
            <input type="password" name="vpassword" id="vpassword" required minlength="1"
                maxlength="20" placeholder="Contraseña" pattern="[-a-zA-Z0-9~!$%^&*_=+,ñ. }{?]{0,20}">
        </div>

        <div class="campoLogin centrado" id="data5">
            <label for="linea">Selecciona la linea de la que quieras estar mas al tanto:
            </label>
            <select id="linea" name="linea">
                <option value="0" selected disabled>Selecciona una opción</option>
                <option value="Metro 1">Lí­nea 1</option>
                <option value="Metro 2">Lí­nea 2</option>
                <option value="Metro 3">Lí­nea 3</option>
                <option value="Metro 4">Lí­nea 4</option>
                <option value="Metro 5">Lí­nea 5</option>
                <option value="Metro 6">Lí­nea 6</option>
                <option value="Metro 7">Lí­nea 7</option>
                <option value="Metro 8">Lí­nea 8</option>
                <option value="Metro 9">Lí­nea 9</option>
                <option value="Metro A">Lí­nea A</option>
                <option value="Metro B">Lí­nea B</option>
                <option value="Metro 12">Lí­nea 12</option>
            </select>
        </div>

        <button type="submit" form="crearCuenta" value="Ingresar"
            class="btnSubmit">Ingresar</button>
    </div>

</form>
</div><!-- #formCrearCuenta -->

`;

// Template de iniciar sesion
var iniciarSesion = `
<div id="formInicioSesion">
    <form class="login" name="iniciarSesion" id="iniciarSesion" method="post" action="../../InicioSesion">
        <div class="text-control">
            <h3 class="titulo"> Ingresa los siguientes datos: </h3>
        </div>

        <div class="data-entry">
            <div class="campoInicio centrado">
                <label for="username">Nombre de usuario:</label>
                <input type="text" name="username" id="username" required minlength="1" maxlength="20" placeholder="Nombre de usuario" pattern="[-a-zA-Z0-9~!$%^&*_=+,ñ. }{?]{0,20}" required>
            </div>

            <div class="campoInicio centrado">
                <label for="user-password">Contraseña:</label>
                <input type="password" name="user-password" id="user-password" required minlength=1 maxlength="20" placeholder="Contraseña" pattern="[-a-zA-Z0-9~!$%^&*_=+,ñ. }{?]{0,20}" required>
            </div>

            <button type="submit" form="iniciarSesion" value="Ingresar" class="btnSubmit">Ingresar</button>
    </form>
</div>

`;

