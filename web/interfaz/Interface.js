/* 
    Esta clase sera la principal a la hora de manejar elementos del DOM, creandolos dependiendo de 
    las necesidades que se presenten
*/
class Interface{
    
    crearElemento(tipo, text, ...atributos){
        /*
         * Este metodo crea un elemento del DOM, y lo devuelve
         * tipo         Tipo de elemento a crear
         * text         Texto que puede llegar a contener el elemento
         * atributos            Array de arrays, en cada array individual tiene dos valores, el primero sera sobre el nombre del atributo, 
         *                              y el segundo su valor
         */
        let element = document.createElement(tipo);
        atributos.forEach(atributo=>{
            if(atributo[0] == 'class'){
                element.classList.add(atributo[1]);
            }else{
                element.setAttribute(atributo[0], atributo[1]);
            }
        });

        element.textContent = text;
    
        return element;
    }

    insertarAlerta(padre, mensaje){
        /*
         * Este metodo inserta una alerta en el DOM
         * padre              Elemento padre, en el que se va a insertar el hijo
         * mensaje          El contenido que se va a insertar en la alerta
         */
        let atributos = ['id', 'ventanaAlerta'];
        
        let alerta = this.crearElemento('div', mensaje, atributos);
        padre.appendChild(alerta);
    }

    insertarConfirmacion(padre, mensaje){
        let atributos = ['id', 'ventanaNice'];
        
        let alerta = this.crearElemento('div', mensaje, atributos);
        padre.appendChild(alerta);
    }
    
    

}