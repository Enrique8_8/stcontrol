<%@tag description="Header de la pagina" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="username" required="true" %>

<header>
    <a href="../pageIndex/index.jsp">
        <img src="../../assets/logo.png" alt="Logo de STControl" id="logo" />
    </a>
        <!-- Barra de navegacion --> 
        <nav>
            <!-- Elementos de la barra de navegacion -->
            <ul>
                <li>
                    <a href="#">
                        Reportes
                        <i class="material-icons">keyboard_arrow_down</i>
                    </a>
                    <ul>
                        
                        <li><a href="../pageVerReportes/mostrarReportes.jsp">Ver Reportes</a></li>
                        <li><a href="../pageCrearRegistro/crearRegistro.jsp">Crear Reporte</a></li>
                    </ul>
                </li>
                <li><a href="../pageEstadisticas/estadisticasMapa.jsp"> Mapa </a></li>
                <li><a href="../pageAcercaDe/acercaDe.jsp">Acerca de</a></li>
                <li><a href="../pageForo/foro.jsp">Foro</a></li>
            </ul>
            <div id="btn_access">
                <c:choose>
                    <c:when test="${username.equals('')}">
                        <a href="../pageLogin/login.jsp">Acceder</a>
                    </c:when>
                    <c:otherwise>
                        <a href="../../RedireccionamientoCuentaUser">Mi Cuenta</a>
                    </c:otherwise>
                </c:choose>
                
            </div><!-- #btn_access -->
        </nav>
    </header>
