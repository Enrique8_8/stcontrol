<%@tag description="template del mapa" pageEncoding="UTF-8"%>
<%@attribute name="mapa" fragment="true" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>


<div id="contenedorPrincipal">
    <h1 class="encabezado">Mapa del Metro</h1>
    <div id="contenidoMapa">
        <div id="map"></div>

        <div id="filtrosMapa">
            <form id="formFiltros">
                <h2 class="encabezado">Filtros</h2>
                <div class="filtro" id="filtroLineas">
                    <div class="encabezadoFiltro">
                        <h3 class="encabezado">Lineas del metro</h3>
                        <button type="button" class="btnMostrarFiltro" id="btnMostrarLinea">
                            <span class="material-icons" id="expandirLinea">
                                expand_more
                            </span>
                            <span class="material-icons" id="contraerLinea">
                                expand_less
                            </span>
                        </button>
                    </div><!-- #.encabeadoFiltro -->

                    <div id="agregarLineas">
                        <p class="descripcion">Lineas de metro a visializar</p>
                        <div id="OpcionesLineas">
                            <div class="opc opcLinea">
                                <input type="checkbox" name="LineaMetro" id="Metro1" value="1">
                                <label for="Metro1">Metro Linea 1</label>
                            </div>

                            <div class="opc opcLinea">
                                <input type="checkbox" name="LineaMetro" id="Metro2" value="2">
                                <label for="Metro2">Metro Linea 2</label>
                            </div>

                            <div class="opc opcLinea">
                                <input type="checkbox" name="LineaMetro" id="Metro3" value="3">
                                <label for="Metro3">Metro Linea 3</label>
                            </div>

                            <div class="opc opcLinea">
                                <input type="checkbox" name="LineaMetro" id="Metro4" value="4">
                                <label for="Metro4">Metro Linea 4</label>
                            </div>

                            <div class="opc opcLinea">
                                <input type="checkbox" name="LineaMetro" id="Metro5" value="5">
                                <label for="Metro5">Metro Linea 5</label>
                            </div>

                            <div class="opc opcLinea">
                                <input type="checkbox" name="LineaMetro" id="Metro6" value="6">
                                <label for="Metro6">Metro Linea 6</label>
                            </div>

                            <div class="opc opcLinea">
                                <input type="checkbox" name="LineaMetro" id="Metro7" value="7">
                                <label for="Metro7">Metro Linea 7</label>
                            </div>

                            <div class="opc opcLinea">
                                <input type="checkbox" name="LineaMetro" id="Metro8" value="8">
                                <label for="Metro8">Metro Linea 8</label>
                            </div>

                            <div class="opc opcLinea">
                                <input type="checkbox" name="LineaMetro" id="Metro9" value="9">
                                <label for="Metro9">Metro Linea 9</label>
                            </div>

                            <div class="opc opcLinea">
                                <input type="checkbox" name="LineaMetro" id="MetroA" value="A">
                                <label for="MetroA">Metro Linea A</label>
                            </div>

                            <div class="opc opcLinea">
                                <input type="checkbox" name="LineaMetro" id="MetroB" value="B">
                                <label for="MetroB">Metro Linea B</label>
                            </div>

                            <div class="opc opcLinea">
                                <input type="checkbox" name="LineaMetro" id="12" value="Metro12">
                                <label for="Metro12">Metro Linea 12</label>
                            </div>
                        </div>
                    </div>
                </div><!-- .filtro -->

                <div class="filtro" id="filtroEstaciones">
                    <div class="encabezadoFiltro">
                        <h3 class="encabezado">Estaciones del metro</h3>
                        <button type="button" class="btnMostrarFiltro" id="btnMostrarEst">
                            <span class="material-icons" id="expandirEst">
                                expand_more
                            </span>
                            <span class="material-icons" id="contraerEst">
                                expand_less
                            </span>
                        </button>
                    </div><!-- .encabezadoFiltro -->

                    <div id="agregarEstaciones">
                        <p class="descripcion">Estaciones del metro a visualizar</p>
                    </div>
                </div><!-- .filtro -->

                <div class="filtro" id="filtroNumero">
                    <div class="encabezadoFiltro">
                        <h3 class="encabezado">Numero de reportes</h3>
                        <button type="button" class="btnMostrarFiltro" id="btnMostrarNum">
                            <span class="material-icons" id="expandirNum">
                                expand_more
                            </span>
                            <span class="material-icons" id="contraerNum">
                                expand_less
                            </span>
                        </button>



                    </div>

                    <div id="agegarNumero">
                        <p class="descripcion">Segun el numero de reportes registrados</p>

                        <div id="OpcionesNum">
                            <div class="opc opcNum">
                                <input type="checkbox" name="numIncidencias" id="num0" value="0">
                                <label for="num0">Sin Reportes</label>
                            </div><!-- .opc -->

                            <div class="opc opcNum">
                                <input type="checkbox" name="numIncidencias" id="num1" value="1">
                                <label for="num1">1 - 10 Reportes</label>
                            </div><!-- .opc -->

                            <div class="opc opcNum">
                                <input type="checkbox" name="numIncidencias" id="num2" value="2">
                                <label for="num2">11 - 25 Reportes</label>
                            </div><!-- .opc -->

                            <div class="opc opcNum">
                                <input type="checkbox" name="numIncidencias" id="num3" value="3">
                                <label for="num3">26 - 50 Reportes</label>
                            </div><!-- .opc -->

                            <div class="opc opcNum">
                                <input type="checkbox" name="numIncidencias" id="num4" value="4">
                                <label for="num4">51 - 100 Reportes</label>
                            </div><!-- .opc -->

                            <div class="opc opcNum">
                                <input type="checkbox" name="numIncidencias" id="num5" value="5">
                                <label for="num5">101 - 200 Reportes</label>
                            </div><!-- .opc -->

                            <div class="opc opcNum">
                                <input type="checkbox" name="numIncidencias" id="num6" value="6">
                                <label for="num6">+200 Reportes </label>
                            </div><!-- .opc -->

                        </div>

                    </div>
                </div><!-- .filtro -->
                <button type="submit" id="aplicarFiltros">Aplicar Filtros</button>
                <button type="reset" id="btnReset">Limpiar Filtros</button>

            </form>
        </div>

        <div id="acotaciones">
            <h2 class="encabezado">Acotaciones</h2>
            <ul>
                <li>
                    <div id="color1"></div>
                    <p>Sin Reportes</p>
                </li>
                <li>
                    <div id="color2"></div>
                    <p>1 - 10 Reportes</p>
                </li>
                <li>
                    <div id="color3"></div>
                    <p>10 - 25 Reportes</p>
                </li>
                <li>
                    <div id="color4"></div>
                    <p>25 - 50 Reportes</p>
                </li>
                <li>
                    <div id="color5"></div>
                    <p>50 - 100 Reportes</p>
                </li>
                <li>
                    <div id="color6"></div>
                    <p>100 - 200 Reportes</p>
                </li>
                <li>
                    <div id="color7"></div>
                    <p>+200 Reportes</p>
                </li>
            </ul>

        </div>

    </div>


</div>