<%@tag description="Footer template" pageEncoding="UTF-8"%>
<%@attribute name="footer" fragment="true" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<footer>
        <div id="footer_sup">
            <div id="footer_ctc">
                <p>Contacto</p>
                <p>Numero Telefonico: 9090909090</p>
                <p>Correo@correo.com</p>
            </div>

            <div id="footer_ubi">
                <div id="footer_txt">
                    <p>Ubicacion</p>
                    <p>Mar Mediterráneo 227, Popotla, 11400 Ciudad de México, CDMX</p>
                </div>
                <img src="../../assets/Ubicacion.png" alt="Ubicacion de Deep Space">
            </div>
        </div>
        <p>Derechos Reservados Deep Space</p>
</footer>