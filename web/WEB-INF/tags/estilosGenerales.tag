<%@tag description="Carga los estilos que cada pagina debe de incluir" pageEncoding="UTF-8"%>

<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600|Overpass+Mono:400,600|Poppins:400,600&display=swap"
      rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Monoton&display=swap" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="../../css/Normalize.css">
<link rel="stylesheet" type="text/css" href="../../css/stylesHeader.css">
<link rel="stylesheet" type="text/css" href="../../css/stylesFooter.css">
<link rel="stylesheet" type="text/css" href="../../interfaz/interface.css">

<link rel="stylesheet" type="text/css" href="../../css/stylesGenerales.css">
